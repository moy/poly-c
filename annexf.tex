\chapter{Le bêtisier}
%********************

Cette annexe est
une collection des bêtises qu'il faut faire au moins une fois dans sa vie
pour être vacciné.
La caractéristique de beaucoup de ces erreurs est de ne pas provoquer 
de message d'erreur du compilateur, rendant ainsi leur détection difficile.
La différence entre le texte correct et le texte erroné est souvent seulement
d'un seul caractère.
La découverte de telles erreurs ne peut donc se faire que par un examen très
attentif du source du programe.

\section{Erreur avec les opérateurs}
%===================================

\subsection{Erreur sur une comparaison}
%--------------------------------------
\begin{center}
\begin{tabular}{|l|l|} \hline

%   ligne
\begin{minipage}{6 cm}
Ce que voulait le programmeur
\end{minipage} & 
\begin{minipage}{6 cm}
Comparer {\tt a} et {\tt b}
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
Ce qu'il aurait dû écrire
\end{minipage} & 
\begin{minipage}{6 cm}
\begin{verbatim}
if (a == b)
\end{verbatim}
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
Ce qu'il a écrit
\end{minipage} & 
\begin{minipage}{6 cm}
\begin{verbatim}
if (a = b)
\end{verbatim}
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
Ce qu'il a obtenu
\end{minipage} & 
\begin{minipage}{6 cm}
une affectation de {\tt b} à {\tt a}, suivie d'une comparaison à 0
de la valeur affectée.
\end{minipage} \\ \hline

%   fin du tableau
\end{tabular}
\end{center}


\subsubsection*{Comment est ce possible ?}
%........................................
L'affectation est un opérateur et non pas une instruction.

\subsection{Erreur sur l'affectation}
%------------------------------------
C'est le pendant de l'erreur précédente.
\begin{center}
\begin{tabular}{|l|l|} \hline

%   ligne
\begin{minipage}{6 cm}
Ce que voulait le programmeur
\end{minipage} & 
\begin{minipage}{6 cm}
Affecter {\tt b} à {\tt a}
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
Ce qu'il aurait dû écrire
\end{minipage} & 
\begin{minipage}{6 cm}
\begin{verbatim}
a = b;
\end{verbatim}
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
Ce qu'il a écrit
\end{minipage} & 
\begin{minipage}{6 cm}
\begin{verbatim}
a == b;
\end{verbatim}
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
Ce qu'il a obtenu
\end{minipage} & 
\begin{minipage}{6 cm}
La comparaison
de {\tt a} à {\tt b}, suivie de l'inutilisation du résultat.
\end{minipage} \\ \hline

%   fin du tableau
\end{tabular}
\end{center}

\subsubsection*{Comment est ce possible ?}
%........................................
Une dérivation possible pour {\it instruction} est~:\\
{\it instruction}~:\\
   \hspace*{10mm}$\Rightarrow$
   \hspace{3mm}{\it expression\/$_{option}$}
   \hspace{3mm}{\tt ;}\\

Pour que cela ait un sens, il faut que l'{\it expression} réalise un effet
de bord, mais rien ne l'impose dans la définition du langage.

\section{Erreurs avec les macros}
%================================
Le mécanisme des macros est réalisé par le préprocesseur qui réalise un
traitement en amont du compilateur proprement dit.
Le traitement des macros est un pur traitement textuel, sans aucun contexte~;
c'est un nid à erreurs.

\subsection{Un {\tt \#define} n'est pas une déclaration}
%-------------------------------------------------------
\begin{center}
\begin{tabular}{|l|l|} \hline

%   ligne
\begin{minipage}{6 cm}
Ce que le programmeur a écrit
\end{minipage} & 
\begin{minipage}{6 cm}
Ce qu'il aurait dû écrire
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
\begin{verbatim}
#define MAX 10;
\end{verbatim}
\end{minipage} &
\begin{minipage}{6 cm}
\begin{verbatim}
#define MAX 10
\end{verbatim}
\end{minipage} \\ \hline

%   fin du tableau
\end{tabular}
\end{center}

Cette erreur peut provoquer ou non une erreur de compilation à l'utilisation 
de la macro~:
\begin{itemize}
\item[\ptiret]
   L'utilisation \verb|x = MAX;| aura pour expansion \verb|x = 10;;|,
   ce qui est licite~: il y a une instruction nulle derrière \verb|x = 10;|.
\item[\ptiret]
   L'utilisation \verb|int t[MAX];| aura pour expansion \verb|int t[10;];|
   ce qui génèrera un message d'erreur.
\end{itemize}

\subsection{Un {\tt \#define} n'est pas une initialisation}
%-------------------------------------------------------
\begin{center}
\begin{tabular}{|l|l|} \hline

%   ligne
\begin{minipage}{6 cm}
Ce que le programmeur a écrit
\end{minipage} & 
\begin{minipage}{6 cm}
Ce qu'il aurait dû écrire
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
\begin{verbatim}
#define MAX = 10
\end{verbatim}
\end{minipage} &
\begin{minipage}{6 cm}
\begin{verbatim}
#define MAX 10
\end{verbatim}
\end{minipage} \\ \hline

%   fin du tableau
\end{tabular}
\end{center}

Cette erreur sera généralement détectée à la compilation, malheureusement le
message d'erreur sera émis sur l'utilisation de la macro, et non pas
là où réside l'erreur, à savoir la définition de la macro.

\subsection{Erreur sur macro avec paramètres}
%--------------------------------------------
La distinction entre macro avec paramètres et macro sans paramètre se fait
sur la présence d'une parenthèse ouvrante juste
après le nom de la macro, sans aucun blanc entre les deux.
Ceci peut amener des résultats surprenant ; comparer les deux exemples 
suivants~:
\begin{center}
\begin{tabular}{|l|l|l|} \hline
Définition de la macro & paramètres & corps de la macro \\ \hline
\verb|#define add(a,b) (a + b)| & {\tt a} et {\tt b} & \verb|(a + b)| \\ \hline
\verb|#define add (a,b) (a + b)| & aucun & \verb|(a,b) (a + b)| \\ \hline

%   fin du tableau
\end{tabular}
\end{center}

\subsection{Erreur avec les effets de bord}
%------------------------------------------
Le corps d'une macro peut comporter plusieurs occurrences d'un paramètre.
Si à l'utilisation de la macro on réalise un effet de bord sur le
paramètre effectif, cet effet de bord sera réalisé plusieurs fois.
Exemple~:
\begin{verbatim}
#define CARRE(a) ((a) * (a))
\end{verbatim}
l'utilisation de \verb|CARRE(x++)| aura comme expansion \verb|((x++) * (x++))|
et l'opérateur {\tt ++} sera appliqué deux fois.

\section{Erreurs avec l'instruction {\tt if}}
%=============================================
L'instruction {\tt if} ne comporte ni mot-clé introducteur de la partie
{\it then}, ni terminateur (pas de {\tt fi} dans le style des {\it if then else
fi}).
Ceci peut provoquer les erreurs suivantes~:

\begin{center}
\begin{tabular}{|l|l|} \hline

%   ligne
\begin{minipage}{6 cm}
Ce que le programmeur a écrit
\end{minipage} & 
\begin{minipage}{6 cm}
Ce qu'il aurait dû écrire
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
\begin{verbatim}
if ( a > b) ;
   a = b;
\end{verbatim}
\end{minipage} &
\begin{minipage}{6 cm}
\begin{verbatim}
if ( a > b) 
   a = b;
\end{verbatim}
\end{minipage} \\ \hline

%   fin du tableau
\end{tabular}
\end{center}

Le problème vient aussi du fait de l'existence de l'instruction nulle.

\begin{center}
\begin{tabular}{|l|l|} \hline

%   ligne
\begin{minipage}{6 cm}
Ce que le programmeur a écrit
\end{minipage} & 
\begin{minipage}{6 cm}
Ce qu'il aurait dû écrire
\end{minipage} \\ \hline

%   ligne
\begin{minipage}{6 cm}
\begin{verbatim}
if (a > b)
   if ( x > y)  x = y;
else
   ...

\end{verbatim}
\end{minipage} &
\begin{minipage}{6 cm}
\begin{verbatim}
if (a > b)
   { if ( x > y)  x = y; }
else
   ...

\end{verbatim}
\end{minipage} \\ \hline

%   fin du tableau
\end{tabular}
\end{center}

On rappelle qu'un {\tt else} est raccroché au premier {\tt if}.

\section{Erreurs avec les commentaires}
%======================================
\label{erreur-commentaire}
Il y a deux erreurs classiques avec les commentaires~:
\begin{enumerate}
\item le programmeur oublie la séquence fermante {\tt */}. Le compilateur
``mange'' donc tout le texte jusqu'à la séquence fermante du prochain
 commentaire.

\item On veut enlever (en le mettant en commentaire) un gros bloc
d'instructions sans prendre garde au
fait qu'il comporte des commentaires.
Les commentaires ne pouvant être imbriqués, ça n'aura pas l'effet escompté
par le programmeur.
La méthode classique pour enlever (tout en le laissant dans le source)
un ensemble d'instructions est d'utiliser le préprocesseur~:
\begin{verbatim}
#ifdef NOTDEFINED
...
#endif
\end{verbatim}
\end{enumerate}

\section{Erreurs avec les priorités des opérateurs}
%==================================================
Les priorités des opérateurs sont parfois surprenantes.
Les cas les plus gênants sont les suivants~:
\begin{itemize}
\item La priorité des opérateurs bit à bit est inférieure à celle des
opérateurs de comparaison.
\begin{center}
\begin{tabular}{|l|l|l|} \hline
Le programmeur a écrit & il désirait & il a obtenu \\ \hline
\verb|x & 0xff == 0xac| & \verb|(x & 0xff) == 0xac| & \verb|x & (0xff == 0xac)| 
\\ \hline
%   fin du tableau
\end{tabular}
\end{center}

\item La priorité des opérateurs de décalage est inférieure à celle des
opérateurs arithmétiques.
\begin{center}
\begin{tabular}{|l|l|l|} \hline
Le programmeur a écrit & il désirait & il a obtenu \\ \hline
\verb|x << 4 + 0xf| & \verb|(x << 4) + 0xf| & \verb|x << (4 + 0xf)| \\ \hline
%   fin du tableau
\end{tabular}
\end{center}

\item La priorité de l'opérateur d'affectation est inférieure à celle des
opérateurs de comparaison.
Dans la séquence ci-dessous, très souvent utilisée, toutes les parenthèses
sont nécessaire~:
\begin{verbatim}
while ((c = getchar()) != EOF)
   {
   ...
   }
\end{verbatim}
\end{itemize}

\section{Erreur avec l'instruction {\tt switch}}
%================================================
\subsection{Oubli du {\tt break}}
%-------------------------------
L'instruction de sélection a pour syntaxe~:
{\it instruction-sélection}~:\\
   \hspace*{10mm}$\Rightarrow$
      \hspace{3mm}{\tt switch (}
      \hspace{3mm}{\it expression}
      \hspace{3mm}{\tt )}
      \hspace{3mm}{\it instruction}\\
La notion d'alternative de la sélection n'apparaît pas dans la syntaxe~: le
programmeur doit les réaliser par une liste d'instruction étiquettée
par {\tt case} {\it expression-constante} et terminée par {\tt break}.
En cas d'oubli du {\tt break}, une catastrophe s'ensuit.


\subsection{Erreur sur le {\tt default}}
%---------------------------------------
L'alternative à exécuter par défaut est introduite par l'étiquette
{\tt default}.
Si une faute de frappe est commise sur cette étiquette, l'alternative
par défaut ne sera plus reconnue : l'étiquette sera prise pour une
étiquette d'instruction sur laquelle ne sera fait aucun {\tt goto}.
\begin{verbatim}
switch(a)
   {
   case 1 : a = b;
   defult : return(1);       /*   erreur non détectée    */
   }
\end{verbatim}

Une version diabolique de cette erreur est relatée dans le livre de Peter
Van Der Linden : si la lettre l de {\tt default} est remplacée par le 
chiffre 1, avec les fontes utilisées pour imprimer les sources, qui verra la 
différence entre {\tt l} et {\tt 1} ?

\section{Erreur sur les tableaux multidimensionnels}
%====================================================
La référence à un tableau {\tt t} à deux dimensions s'écrit \verb|t[i][j]|
et non pas \verb|t[i,j]| comme dans d'autres langages de programmation.
Malheureusement, si on utilise par erreur la notation \verb|t[i,j]|
selon le contexte d'utilisation, elle pourra être acceptée par le compilateur.
En effet, dans cette expression la virgule est l'opérateur
qui délivre comme résultat l'opérande droit après avoir évalué
l'opérande gauche.
Comme l'évaluation de l'opérande gauche ne réalise ici aucun effet de bord, 
cette évaluation est inutile , donc \verb|t[i,j]| est équivalent à
\verb|t[j]| qui est l'adresse du sous-tableau correspondant à l'index {\tt j}.

\section{Erreur avec la compilation séparée}
%===========================================
Une erreur classique est d'avoir un tableau défini dans une unité de
compilation~:
\begin{verbatim}
int tab[10];
\end{verbatim}
et d'utiliser comme déclaration de référence dans une autre unité de
compilation~:
\begin{verbatim}
extern int * tab;
\end{verbatim}
Rappelons que \verb|int tab[]| et \verb|int *t| ne sont équivalents que dans
le seul cas de paramètre formel de fonction.
Dans le cas qui nous occupe ici, la déclaration de référence correcte est~:
\begin{verbatim}
extern int tab[];
\end{verbatim}

