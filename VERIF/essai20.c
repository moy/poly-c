/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai de scanf avec les formats %o %x %u                    >**/
/** @(#) title: Ils peuvent etre optionnelement signes!!                    >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
main()
{
int ret;
unsigned int nb1, nb2, nb3;

   ret = scanf("%o %x %u", &nb1, &nb2, &nb3);
   printf("ret = %u, nb1 = %u nb2 = %u nb3 = %u\n",ret, nb1, nb2, nb3);
   printf("ret = %d, nb1 = %d nb2 = %d nb3 = %d\n",ret, nb1, nb2, nb3);

}

