/******************************************************************************/
/**                                                                          **/
/** @(#) title: Pour verifier qu'on peut modifier une constante             >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/**   Ce se compile, mais ca emet un warning                                 **/
/**                                                                          **/
/******************************************************************************/
main()
{
const i = 12;

int *p = &i;
*p = 13;
printf("i = %d\n",i);
}

