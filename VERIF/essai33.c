/******************************************************************************/
/**                                                                          **/
/** @(#) title: Reference a une struct avant sa definition                  >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

     /*
     Ceci met en evidence le fait que la simple reference a une struct
     est une declaration avec un "incomplete type" tel qu'explique en
     6.5.2.3
     */

struct s2 t[10];
struct s1 
   {
   struct s2 * p;
   };

struct s2
   {
   struct s1 *p;
   };

struct s1 str1;
struct s2 str2;
main()
{
str1.p = &str2;
str2.p = &str1;
}
