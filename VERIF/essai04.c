/******************************************************************************/
/**                                                                          **/
/** @(#) title:  Verif d'exemple pour le chap4                              >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/


enum {BLEU=1, BLANC, ROUGE};

void print_color(color)
int color;
{
switch(color)
   {
   case BLEU : printf("bleu"); break;
   case BLANC : printf("blanc"); break;
   case ROUGE : printf("rouge"); break;
   default : printf("erreur interne du logiciel numero xx\n");
   }
}
main()
{
print_color(BLANC);
printf("\n");
}
