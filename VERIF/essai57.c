/******************************************************************************/
/**                                                                          **/
/** @(#) title: Passage de fonction en parametre                            >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*   dans le prototype du parametre on met le nom des parametres   */
int call_fonc1(int f(int a, int b), int p1, int p2)
{
return (f(p1,p2));
}

/*   dans le prototype du parametre on omet le nom des parametres   */
int call_fonc2(int f(int , int), int p1, int p2)
{
return (f(p1,p2));
}

int add(int a, int b)
{
return(a + b);
}

int sub(int a, int b)
{
return(a - b);
}

main()
{
printf("6 + 4 = %d\n",call_fonc1(add,6,4));
printf("6 - 4 = %d\n",call_fonc2(sub,6,4));
}

