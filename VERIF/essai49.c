/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai avec de VRAIS tableau de pointeurs vers des tableaux  >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
int ligne1[] = {10, 11, 12};
int ligne2[] = {20, 21, 22};
int ligne3[] = {30, 31, 32};
int ligne4[] = {40, 41, 42};

int LIGNE1[] = {100};
int LIGNE2[] = {110,111};
int LIGNE3[] = {120,121, 122};
int LIGNE4[] = {130,131,132,133};

int (*t[4])[3] = { (int (*)[3]) ligne1, (int (*)[3]) ligne2,
		   (int (*)[3]) ligne3, (int (*)[3]) ligne4};

int (*T[4])[] = { (int (*)[]) LIGNE1, (int (*)[]) LIGNE2, (int (*)[]) LIGNE3,
		  (int (*)[]) LIGNE4 };

int taille[4] = {1,2,3,4};

main()
{
int i, j;

for (i = 0; i < 4; i++)
   {
   for(j = 0; j < 3; j++) printf("%d ",(*t[i])[j]);
   printf("\n");
   }

for (i = 0; i < 4; i++)
   {
   for (j = 0; j < taille[i]; j++)
      printf("%d ", (*T[i])[j]);
   printf("\n");
   }

}
