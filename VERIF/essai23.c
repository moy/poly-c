/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verification si getc evalue plusieurs fois ses arguments    >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*
 *  en Sun OS 4.1.3, getc evalue 2 fois l'argument
 *  en Solaris 2.4, getc evalue 2 fois l'argument
 */

#include <stdio.h>
main()
{
int i, c;
FILE * TAB_FILE[10] = {stdin,stdin,stdin};

printf("Tapez un caractere quelconque\n");
i = 0;
c = getc(TAB_FILE[i++]);
if (i != 1)
   printf("getc evalue plusieurs fois son argument (i = %d)\n",i);
else printf("get evalue une seule fois son argument\n");

}

