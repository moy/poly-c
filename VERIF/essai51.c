#include <stdio.h>
#include <stdarg.h>

int add(int nbopd, ...)
{
int i, s = 0;
va_list(ap);

va_start(ap,nbopd);
for( i = 1; i <= nbopd; i++)
   s = s + va_arg(ap,int);
va_end(ap);
return(s);
}

int main()
{
printf("resu = %d\n",add(3,10,11,12));
}

