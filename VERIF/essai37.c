/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verification d'affirmatio au chapitre 6                     >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/


enum type {ENTIER, REEL};

struct arith
   {
   enum type typ_val;
   union
      {
      int i;
      float f;
      } u;
   };

main()
{
struct arith a1,a2;

a1.typ_val = ENTIER;
a1.u.i = 1;

a2.typ_val = REEL;
a2.u.f = 2.0;
}
