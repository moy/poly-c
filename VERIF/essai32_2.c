/*   On donne un prototype ansi a la procedue qui ne l'est pas   */
/*   et pas de prototype a celle qui l'est                       */
/*   ---------------------------------------------------------   */
void k_r(float f, short int si, char c);

main()
{
float f = 1.0;
short int si= 1;
char c = 'a';
k_r(f,si,c);
ansi(f,si,c);
}
