/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verification D'affirmation auchapitre 6                     >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/


struct sr
   {
   unsigned int trace : 2;
   unsigned int priv : 2;
   unsigned int : 1;            /*   inutilise   */
   unsigned int masque : 3;
   unsigned int : 3;            /*   inutilise   */
   unsigned int extend : 1;
   unsigned int negative : 1;
   unsigned int zero : 1;
   unsigned int overflow : 1;
   unsigned int carry : 1;
   };

main()
{
struct sr SR;

SR.carry = 1;
if (SR.carry)
   printf("carry\n");
}


