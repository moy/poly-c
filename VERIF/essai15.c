/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai de scanf avec le format %i : on peut lire des         >**/
/** @(#) title: nombres ecrits sous la forme 123, 0234, 0x34ab              >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
main()
{
int ret, i, nb1, nb2, nb3, nb4;

   ret = scanf("%i %i %i %i", &nb1, &nb2, &nb3, &nb4);
   printf("ret = %d, nb1 = %o nb2 = %d nb3 = %x, nb4 = %x\n",ret, nb1, nb2, nb3, nb4);

}

