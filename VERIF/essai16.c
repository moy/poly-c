/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai de scanf avec le format %u                            >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
main()
{
int ret, i, nb1, nb2, nb3, nb4;

   ret = scanf("%u %u %u", &nb1, &nb2, &nb3);
   printf("ret = %d, nb1 = %d nb2 = %d nb3 = %d\n",ret, nb1, nb2, nb3);

}

