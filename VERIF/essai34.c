/******************************************************************************/
/**                                                                          **/
/** @(#) title: Incomplete type sur les tableaux                            >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

int t[];      /*   t est defini mais de maniere incomplete   */
int t[10];    /*   on complete la definition                 */
main()
{
printf("size of t = %d\n",sizeof t);
}
