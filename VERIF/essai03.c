/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verif d'un exemple du chapitre 4                            >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/


char mess[] = "Hello world!!";
char *p;

main()
{
for (p = &mess[0]; *p != '\0'; p++)
   {
   /*   ici p repere l'element courant de mess   */
   putchar(*p);
   }

for (p = &mess[0]; *p ; p++)
   {
   /*   ici p repere l'element courant de mess   */
   putchar(*p);
   }

p = &mess[0];
while (*p)
   {
   /*   ici utilisation de *p++   */
   putchar(*p++);
   }

p = &mess[0];
while (*p != '\0')
   {
   /*   ici utilisation de *p++   */
   putchar(*p++);
   }
}
