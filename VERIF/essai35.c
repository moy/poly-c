/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essais avec les types compatibles                           >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
struct reel {float x; float y;};
struct reel2 {float x; float y;};
struct {int i; int j;} s1,s2;
struct {int i; int j;} s3;
main()
{
struct reel r1,r2;
struct reel2 r3;

int (*p1)(int,int);
int (*p2)(int,int);
int (*p3)();
int (*p4)(float,int);
int (*p5)(double,int);

#if 0
s1 = s3;   /*   ne mache pas   */
#endif
s1 = s2;   /*   ca marche   */

r1 = r2;   /*   affectation de 2 structures du meme type   */

#if 0
r3 = r1;
#endif

/*      ca ne marche pas   avec le message :                                  */
/*   Message de SUNWspro compiler :                                           */
/*      "essai35.c", line 18: assignment type mismatch:                       */
/*	struct reel2 {float x, float y} "=" struct reel {float x, float y}    */
/*   Message de gcc :                                                         */
/*      essai35.c:21: incompatible types in assignment                        */

p1 = p2;
p1 = p3;   /*   ca marche   */
#if 0
p4 = p3;   /*   ne marche pas   */
#endif

p5 = p3;   /*   ca marche   */
}
