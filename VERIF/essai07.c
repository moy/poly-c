/******************************************************************************/
/**                                                                          **/
/** @(#) title: Programming challenge de 'Deep C secrets' page 249          >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/**   pas tres interessant.                                                  **/
/**                                                                          **/
/******************************************************************************/
void f1(char ca[])
{
printf("&ca    = %x\n",&ca);
printf("&ca[0] = %x\n",&ca[0]);
printf("&ca[1] = %x\n",&ca[1]);
}

void f2(char * pa)
{
printf("&pa    = %x\n",&pa);
printf("&pa[0] = %x\n",&pa[0]);
printf("&pa[1] = %x\n",&pa[1]);
printf("++pa   = %x\n",++pa);
}

char chaine[] = "Hello world!\n";

main()
{
printf("adresse de chaine = %x\n",chaine);
f1(chaine);
f2(chaine);
}

