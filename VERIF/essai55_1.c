/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai avec la compilation separee                           >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/**   Dans les deux modules, m est initialise statiquement : ca resulte      **/
/**   en une erreur a l'edition de liens.                                    **/
/**      ld: fatal: symbol `m' is multiply defined:                          **/
/**      (file essai55_1.o and file essai55_2.o);                            **/
/**                                                                          **/
/**   ATTENTION : si il y a des erreurs 'toto' multiply defined, ld ne sort  **/
/**      pas les erreurs 'undefined' donc je suis oblige de traiter ce cas   **/
/**      dans essai56                                                        **/
/**                                                                          **/
/******************************************************************************/
int m = 50;
extern int n;

main()
{
n = 60;
f();
exit(0);
}

