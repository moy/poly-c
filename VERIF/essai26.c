/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai sur les trigraphes                                    >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
main()
{
char diese = '??=';
char crochet_ouvrant = '??(';
char crochet_fermant = '??)';

/*   subtilite : pour avoir un anti-slash il faut en mettre deux !!   */
char anti_slash = '??/??/';

/*   le ' de la sequence ??' n'est pas pris comme ' de fin   */
char caret = '??'';
char accolade_ouvrante = '??<';
char accolade_fermante = '??>';
char barre_verticale = '??!';
char tilde = '??-';

printf ("%c must be a #\n",diese);
printf ("%c must be a [\n",crochet_ouvrant);
printf ("%c must be a ]\n",crochet_fermant);
printf ("%c must be a \\\n",anti_slash);
printf ("%c must be a ^\n",caret);
printf ("%c must be a {\n",accolade_ouvrante);
printf ("%c must be a }\n",accolade_fermante);
printf ("%c must be a |\n",barre_verticale);
printf ("%c must be a ~\n",tilde);

/*   maintenant dans une chaine   */
printf ("??= ??( ??) ??/??/ ??' ??< ??> ??! ??- \n");
}
