#include <limits.h>
main()
{
float f;
int i;

printf("size of float = %d\n",sizeof(float));

for (i = -20; i <= 20; i++)
   {
   f = i;
   printf("%2d en flottant = 0x%x\n",i,f);
   }

f = 0.5;
printf(".5 en flottant = 0x%x\n",f);
f = 0.25;
printf(".25 en flottant = 0x%x\n",f);
f = 100;
printf("%2d en flottant = 0x%x\n",100,f);
f = 1000;
printf("%2d en flottant = 0x%x\n",1000,f);
f = 10000;
printf("%2d en flottant = 0x%x\n",10000,f);
f = FLT_MAX;
printf("FLT_MAX en flottant = 0x%x\n",f);

f = FLT_MIN;
printf("FLT_MIN en flottant = 0x%x\n",f);
}
