int i = 0x01020304;

main()
{
char *p;

p = (char *) &i;
if (*p++ == 1 && *p++ == 2 && *p++ == 3 && *p++ ==4)
   printf("big endian\n");

p = (char *) &i;
if (*p++ == 4 && *p++ == 3 && *p++ == 2 && *p++ ==1)
   printf("little endian\n");
}
