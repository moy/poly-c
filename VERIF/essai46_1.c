/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai avec la compilation separee                           >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/**   On met en evidence les configurations autorisees                       **/
/**   Se reporter a essai55 et essai56 pour voir ce qui est refuse           **/
/**                                                                          **/
/**   int i             int i;                  accepte                      **/
/**   int j = 20;       int j;                  accepte                      **/
/**   int k;            extern int k;           accepte      recommande      **/
/**   int l = 40;       extern int l;           accepte      recommande      **/
/**   int m = 50;       int m = 60;             REFUSE                       **/
/**   extern int n;	extern int n;		REFUSE                       **/
/**                                                                          **/
/******************************************************************************/
int i;
int j = 20;
int k;
int l = 40;

main()
{
i = 10;
k = 30;
printf("dans main avant appel de f : i   j   k   l\n");
printf("                            %d  %d  %d  %d\n",i,j,k,l);
f();
printf("dans main apres appel de f : i   j   k   l\n");
printf("                            %d  %d  %d  %d\n",i,j,k,l);
exit(0);
}

