/******************************************************************************/
/**                                                                          **/
/** @(#) title: Pour verifier les affimations de chap5                      >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
int tab[5] = {11, 12, 13, 14, 15};

void imp_tab(const int t[], int nb_elem)   /*   definition de imp_tab       */
{
int i;

for (i = 0; i < nb_elem; i++) printf("%d ",t[i]);
}

main()
{
imp_tab(tab,5);
}

