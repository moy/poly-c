/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verification de l'equivalence de 'white space' dans le      >**/
/** @(#) title: format de scanf avec le white space dans le flot d'entree   >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*
 *   De maniere plus precise, on veut verifier que n'importe quel caractere
 *   de 'white space' dans le format va matcher n'importe quel caractere
 *   'white space' dans l'input.
 *
 *   dans le flot d'entree on a 6 sequences de 6 couples de nombres separes
 *   a chaque fois par les 6 caracteres de 'white space' :
 *   blanc, tabulation, return, new line, form feed et vertical tabulation
 *
 *   ATTENTION
 *   Ce programme marche en Solaris 2.4 pas en 4.1.3
 */

main()
{
int ret,i,nb1,nb2;

/*   Une boucle de 6 scanf pour les 6 types de white space   */
/*   separateur dans scanf : un blanc                        */
/*   -----------------------------------------------------   */
printf("essai 1\n");
printf("-------\n");
for (i = 1; i <= 6; i++)
   {
   ret = scanf("%d %d",&nb1,&nb2);
   printf("ret = %d, nb1 = %d nb2 = %d\n",ret,nb1,nb2);
   }

/*   Une boucle de 6 scanf pour les 6 types de white space   */
/*   separateur dans scanf : une tabulation                  */
/*   -----------------------------------------------------   */
printf("essai 2\n");
printf("-------\n");
for (i = 1; i <= 6; i++)
   {
   ret = scanf("%d	%d",&nb1,&nb2);
   printf("ret = %d, nb1 = %d nb2 = %d\n",ret,nb1,nb2);
   }

/*   Une boucle de 6 scanf pour les 6 types de white space   */
/*   separateur dans scanf : un return                       */
/*   -----------------------------------------------------   */
printf("\nessai 3\n");
printf("-------\n");
for (i = 1; i <= 6; i++)
   {
   ret = scanf("%d\r%d",&nb1,&nb2);
   printf("ret = %d, nb1 = %d nb2 = %d\n",ret,nb1,nb2);
   }

/*   Une boucle de 6 scanf pour les 6 types de white space   */
/*   separateur dans scanf : un new line                     */
/*   -----------------------------------------------------   */
printf("\nessai 4\n");
printf("-------\n");
for (i = 1; i <= 6; i++)
   {
   ret = scanf("%d\n%d",&nb1,&nb2);
   printf("ret = %d, nb1 = %d nb2 = %d\n",ret,nb1,nb2);
   }

/*   Une boucle de 6 scanf pour les 6 types de white space   */
/*   separateur dans scanf : un vertical tab                 */
/*   -----------------------------------------------------   */
printf("\nessai 5\n");
printf("-------\n");
for (i = 1; i <= 6; i++)
   {
   ret = scanf("%d\v%d",&nb1,&nb2);
   printf("ret = %d, nb1 = %d nb2 = %d\n",ret,nb1,nb2);
   }

/*   Une boucle de 6 scanf pour les 6 types de white space   */
/*   separateur dans scanf : un form feed                    */
/*   -----------------------------------------------------   */
printf("\nessai 6\n");
printf("-------\n");
for (i = 1; i <= 6; i++)
   {
   ret = scanf("%d\f%d",&nb1,&nb2);
   printf("ret = %d, nb1 = %d nb2 = %d\n",ret,nb1,nb2);
   }

}

