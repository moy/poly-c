/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verifier qu'il n'y a pas consommation dans l'input du       >**/
/** @(#) title: 'white space' pour les formats c et [                       >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*
 *   ATTENTION
 *   %20c bouffe 20 caracteres quelconques y compris les new line
 *
 */
main()
{
int ret;
int nb1;
#define LGSTR 1024
char str1[LGSTR];
int i;

/*   dans le cas de %c le null termninal n'est pas ajoute a la fin,   */
/*   on initialise donc le tableau avec des nulls                     */
/*   --------------------------------------------------------------   */
for (i = 0; i < LGSTR; i++)
   str1[i] = '\0';

/*   ATTENTION bien sur a ne pas mettre de blanc entre %d et %20 !!   */
ret = scanf("%d%20c", &nb1, str1);
printf("ret = %d, nb1 = %d str1 = %s\n",ret, nb1, str1);

ret = scanf("%d%[ abcdefghijklmnopqrstuvwxyz]", &nb1, str1);
printf("ret = %d, nb1 = %d str1 = %s\n",ret, nb1, str1);

}

