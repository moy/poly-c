/******************************************************************************/
/**                                                                          **/
/** @(#) title: Il y a des long long int en Solaris, pas en 4.1.x           >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*
 Ce programme se compile sous Solaris 2.5 et donne
 "t.c", line 4: illegal type combination en Sun OS 4.1.3
 */

main()
{
printf("%d\n",sizeof(long int));
printf("%d\n",sizeof(long long int));
}
