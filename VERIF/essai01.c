/******************************************************************************/
/**                                                                          **/
/** @(#) title:  Verifier ce qui est dit sur les licences du compilo        >**/
/** @(#) title:  en ce qui conmcerne les procedures                         >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

/*
 *   licence 1 : en l'absence de type retourne par la fonction on prend int
 */
sum(int i,int j)
{
return(i+j);
}

/*
 *   licence 2 : en l'absence du type des parametres, on prend int
 */
int diff (i,j)
{
return(i-j);
}

/*
 *   licence 3 : metter le vide a la place du mot cle void pour les parametres
 */
main()
{
int r;

r = sum(12,34);
printf("r = %d\n",r);
r = diff(23,10);
printf("r = %d\n",r);
}

