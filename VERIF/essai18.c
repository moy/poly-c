/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verification des proprietes de l'ecriture a la fin          >**/
/** @(#) title: Deux processus font des ecriture a la fin sur le meme fic.  >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*
 *   Les ecritures s'ecrasent mutuellement
 *   Pour voir ce qu'il faut faire Cf essai19.c
 */
#include <stdio.h>

main()
{
FILE *fp;

/*   pour detruire le contenu du fichier si il existe   */
fp = fopen("essai18.data","w");
fclose(fp);

if (fork() == 0)
   {
   /*   le fils   */
   /*   -------   */
   FILE *fp;
   int i;

   printf("fils\n");
   fp = fopen("essai18.data","a");
   for (i = 1; i<=10; i++)
      {
      fprintf(fp,"[%8d] : ligne %2d\n",getpid(), i);
      fflush(fp);
      printf("%d\n",getpid());
      sleep(1);
      }
   }
else
   {
   /*   le pere   */
   /*   -------   */
   FILE *fp;
   int i;

   printf("pere\n");
   fp = fopen("essai18.data","a");
   for (i = 1; i<=10; i++)
      {
      fprintf(fp,"[%8d] : ligne %2d\n",getpid(), i);
      printf("%d\n",getpid());
      fflush(fp);
      sleep(1);
      }
   }
}


