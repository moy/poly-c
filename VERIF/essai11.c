/******************************************************************************/
/**                                                                          **/
/** @(#) title:  Pour verifier le comportement de scanf                     >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

/*
 *   Avant de trouver la chaine correspondant a un 'conversion specifier'
 *   scanf bouffe les blancs dans le flot d'entree. On peut donc et on peut
 *   ne pas mettre des blancs entre les differents 'conversion specifier'
 */

main()
{
int i,j,k,l;

scanf("%d%d%d%d",&i,&j,&k,&l);
printf("i = %d j = %d k= %d l = %d\n",i,j,k,l);

/*   comme separateurs, respectivement un blanc,    */
/*   deux blancs, un blanc et une tabulation        */
/*   -------------------------------------------    */
scanf("%d %d  %d  	%d",&i,&j,&k,&l);
printf("i = %d j = %d k= %d l = %d\n",i,j,k,l);

}
