/******************************************************************************/
/**                                                                          **/
/** @(#) title: Melange des declarations avec set sans prototype            >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

/*   Ce programme fonctionne avec essai32_2.c                                 */
/*   Il a pour but de verifier ce qui est affirme a la page 209 du livre      */
/*   Deep C Secrets                                                           */
/*   Impossible a compiler en SunOS 4.1.3 mais ok sous Solaris et met         */
/*   bien en evidence le bug                                                  */
/******************************************************************************/
k_r(f,si,c)
float f;
short int si;
char c;
{
printf("float = %f, short int = %d , char = %c\n",f,si,c);
}

ansi(float f, short int si, char c)
{
printf("float = %f, short int = %d , char = %c\n",f,si,c);
}
