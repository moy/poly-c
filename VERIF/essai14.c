/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verification de l'equivalence de 'white space' dans le      >**/
/** @(#) title: format de scanf avec le white space dans le flot d'entree   >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*
 *   De maniere plus precise, on veut verifier que n'importe quel nombre
 *   de caracteres de 'white space' dans le format va matcher n'importe quel
 *   nombre de caracteres de 'white space' dans l'input.
 *
 *   Il faut voir le fichier essai14.data pour comprendre les tests faits
 *
 *   ATTENTION
 *   Ce programme marche en Solaris 2.4 pas en 4.1.3
 */

main()
{
int ret,i,nb1,nb2;

for (i = 1; i <= 6; i++)
   {
   ret = scanf("%d 	\r\n\v\f%d",&nb1,&nb2);
   printf("ret = %d, nb1 = %d nb2 = %d\n",ret,nb1,nb2);
   }

}

