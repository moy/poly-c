/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verification de la artie "bestiaire de types "              >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

/*   les tableaux   */

/*    -----------   */
int t1[2][3] = {
   {0, 1, 2},
   { 10, 11, 12}
};
int t2[4][5] = {
   { 0,1,2,3,4},
   { 10,11,12,13,14},
   { 20,21,22,23,24},
   { 30,31,32,33,34},
};
int i, j, k;
int *t3[3] = {&i, &j, &k};

struct complex { float x,y; };
struct complex c1, c2;
struct complex t4[3] = {
   {1.5, 2.4},
   {1,   3.2},
   {0.7, 3}
};

struct complex * t5[] = {&c1, &c2};
int f1(int a) { return(a); } 
int f2(int a) { return(a); } 
int f3(int a) { return(a); } 

int (*t[])(int) = {f1, f2, f3};

/*   pointeurs   */
/*   ---------   */
int *p1;
int *p2 = &i;

int **p3;
int **p4 = &p1;
struct complex * p5;
struct complex * p6 = &c1;

int (*p7)(int);
int (*p8)(int) = f1;
int (*p9)(int, float);
void (*p10)(int);

/*   les fonctions   */
/*   -------------   */
void f4(void)
{
}

void f5(int a)
{
}

int f6(void)
{
}

int f7(int a, float b)
{
}

int f8(int a, ...)
{
}

/*   fonctions non dans un contexte de definition   */
/*   --------------------------------------------   */
extern int f09(int (*pf)(int,int));
extern int f10(int (*pf)(int a ,int b));

/*   les qualificatifs   */
/*   -----------------   */
const int t10[5];
const struct const_coord {
   int x; int y;
};
struct const_coord s1;

const struct {
   int x; int y;
   } s2;

struct coord {
   int x; int y;
};

const struct coord s3;

/*   le main   */
/*   -------   */
main()
{
/*   t10[1] = 0;    ERREUR   */
s1.x = 1;                             /*   PAS D'ERREUR !!!!!!!    */
/*   s2.x = 1;   ERREUR   */
/*   s3.x = 1;   ERREUR   */
}
