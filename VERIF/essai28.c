/******************************************************************************/
/**                                                                          **/
/** @(#) title: verif de morceau de code du chapitre "structures et unions" >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

enum {AVANT, EGAL, APRES};

struct date { int jour, mois,annee;};

char * tres [] = {"AVANT", "EGAL", "APRES"};

int cmp_date( struct date d1, struct date d2)
{
if (d1.annee > d2.annee)
   return(APRES);
if (d1.annee < d2.annee)
   return(AVANT);

if (d1.mois > d2.mois)
   return(APRES);
if (d1.mois < d2.mois)
   return(AVANT);

if (d1.jour > d2.jour)
   return(APRES);
if (d1.jour < d2.jour)
   return(AVANT);

return(EGAL);
}

main()
{
int res;
struct date d1 = {1, 2, 3};
struct date d2 = {1, 2, 3};
struct date d3 = {2, 2, 3};
struct date d4 = {1, 3, 3};
struct date d5 = {1, 2, 4};

res = cmp_date(d1,d2); printf("d1 est %s d2\n",tres[res]);
res = cmp_date(d1,d3); printf("d1 est %s d3\n",tres[res]);
res = cmp_date(d3,d1); printf("d3 est %s d1\n",tres[res]);
res = cmp_date(d1,d4); printf("d1 est %s d4\n",tres[res]);
res = cmp_date(d4,d1); printf("d4 est %s d1\n",tres[res]);
res = cmp_date(d1,d5); printf("d1 est %s d5\n",tres[res]);
res = cmp_date(d5,d1); printf("d5 est %s d1\n",tres[res]);
}
