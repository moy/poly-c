/******************************************************************************/
/**                                                                          **/
/** @(#) title: Probleme de cast pour passage de parametre                  >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*
   Compiler ce programme avec -DBUG pour mettre en evidence le bug
 */

#ifdef BUG
extern double sin();
#else
extern double sin(double x);
#endif

main()
{
printf("%f\n",sin(1));
printf("%f\n",sin((double)1));
}
