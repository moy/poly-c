/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verif de exemple du chapitre 4                              >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
#define NBELEM 3
int taille[NBELEM] = {1, 2, 3};
int ligne1[] = {10};
int ligne2[] = {20,21};
int ligne3[] = {30,31,32};

int * tab[] = {ligne1, ligne2, ligne3};

main()
{
int i, j, *p;

for (i = 0; i < NBELEM; i++)
   {
   for (p = tab[i]; p < tab[i] + taille[i]; p++)
      printf("%d ",*p);
   printf("\n");
   }

for (i = 0; i < NBELEM; i++)
   {
   for (p = tab[i], j = 0; j < taille[i]; j++)
      printf("%d ",p[j]);
   printf("\n");
   }

for (i = 0; i < NBELEM; i++)
   {
   for (j = 0; j < taille[i]; j++)
      printf("%d ", tab[i][j]);
   printf("\n");
   }

}

