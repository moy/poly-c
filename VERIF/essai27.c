/******************************************************************************/
/**                                                                          **/
/** @(#) title: Initialisation de variables automatic                       >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

int i = 2;
int j = 3;
/*   int t[2] = { i,j};   ca ca ferait une erreur de compilation   */
main()
{
/*   ces deux initialisations faisaient une erreur en C K&R   */
int t[2] = { 1,2};
struct s_t { int a; int b;} s = { 3,4};

printf("tableau t --> ");
for (i = 0; i < 2; i++)
   printf("%d ",t[i]);
printf("\n");

printf("structure s --> %d %d\n",s.a,s.b);
}
