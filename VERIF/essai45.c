/******************************************************************************/
/**                                                                          **/
/** @(#) title: Mise en evidence de probleme de comparaison entre           >**/
/** @(#) title: unsigned int et signed int                                  >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
main()
{
unsigned int i = 0;

if (i < -1 ) 
   printf("Bizarre, bizarre ...\n");
else printf ("Tout semble normal\n");

if (sizeof(int) < -1)
   printf("Bizarre, bizarre ...\n");
else printf ("Tout semble normal\n");

if ((int) sizeof(int) < -1)
   printf("Bizarre, bizarre ...\n");
else printf ("Tout semble normal\n");

}
