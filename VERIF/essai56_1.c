/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai avec la compilation separee                           >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/**   Dans les deux modules, n est declare extern : ca resulte               **/
/**   en une erreur a l'edition de liens.                                    **/
/**                                                                          **/
/**      Undefined                       first referenced                    **/
/**       symbol                             in file                         **/
/**       n                                   essai56_1.o                    **/
/**                                                                          **/
/******************************************************************************/
extern int n;

main()
{
n = 60;
f();
exit(0);
}

