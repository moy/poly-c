/******************************************************************************/
/**                                                                          **/
/** @(#) title: Pour verifier que les elements des tableaux a 2 dimensions  >**/
/** @(#) title: ou les tablau de pointeurs peuvent etre accedes par t[i][j] >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
main()
{
static int t1[2] = {1, 2};
static int t2[3] = {3, 4, 5};
static int t3[4] = {6, 7, 8, 9};

static int *t[3] = {t1,t2,t3}; /* meme remarque que pour le tableau precedent*/
static int l[3] = {2, 3, 4};    /*  indique les longueurs des tableaux   */

static int T[2][3] = {{1, 2, 3} , {4, 5, 6}};
int i,j;

/*   on imprime la taille du tableau t   */
/*   ---------------------------------   */
printf("taille de t = %d\n",sizeof(t));

/*   on imprime le tableau t en accedant aux elements par t[i][j]   */
/*   ------------------------------------------------------------   */
printf("\nTableau t : \n");
for (i = 0; i < 3; i++)
   {
   for (j = 0; j < l[i]; j++)
      printf("%d  ",t[i][j]);
   printf("\n");
   }

/*   meme chose, en accedant aux elements de t par la notation *(t[i] + j)   */
/*   ---------------------------------------------------------------------   */
printf("\nTableau t : \n");
for (i = 0; i < 3; i++)
   {
   for (j = 0; j < l[i]; j++)
      printf("%d  ",*(t[i] + j));
   printf("\n");
   }

/*   on imprime la taille du tableau T   */
/*   ---------------------------------   */
printf("\ntaille de T = %d\n",sizeof(T));

/*   on imprime le tableau T en accedant aux elements par T[i][j]   */
/*   ------------------------------------------------------------   */
printf("\nTableau T:\n");
for (i = 0; i < 2; i++)
   {
   for ( j = 0 ; j < 3; j++)
      printf("%d  ",T[i][j]);
   printf("\n");
   }

/*   meme chose, en accedant aux elements de T par la notation *(t[i] + j)   */
/*   ---------------------------------------------------------------------   */
printf("\nTableau T:\n");
for (i = 0; i < 2; i++)
   {
   for ( j = 0 ; j < 3; j++)
      printf("%d  ",*(T[i] + j));
   printf("\n");
   }

}
