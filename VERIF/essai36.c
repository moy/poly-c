/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai sur les espaces de nom                                >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*   premier toto : un indentificateur ordinaire   */
typedef int toto;

/*   deuxieme toto : tag de structure   */
struct toto { float x,y;};

/*   troisieme toto : champ de structure   */
struct s1 {int a,b;};

/*   quatrieme toto : champ d'une autre structure   */
struct s2 {int a,b;};

/*   L'exemple de "deep C secrets" et qui est faux !! */
typedef struct baz {int baz;} baz;
#if 0
baz baz;
#endif

main()
{
/*   cinquieme toto : etiquette de branchement   */
toto : 
printf("Hello world\n");
}


