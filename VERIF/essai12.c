/******************************************************************************/
/**                                                                          **/
/** @(#) title: Essai de scanf avec %10c                                    >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/

/*
 *   On voit avec ca que scanf %<nombre>c lit jusqu'a obtenir <nombre>
 *   caracteres, ca lit meme un retour chariot
 */
#define LGBUF 100
char buf[LGBUF];
int i;

main()
{
int i;

/*
 *    puisque le %c de scanf ne rajoute pas de null on initialise le
 *    tableau a null
 */
for (i = 0 ; i < LGBUF; i++) buf[i] = '\0';
scanf("%10c",buf);
printf("resu de scanf = |%s|\n");
}
