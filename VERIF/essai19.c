/******************************************************************************/
/**                                                                          **/
/** @(#) title: Verification des proprietes de l'ecriture a la fin          >**/
/** @(#) title: sur 2 processus qui partagent le meme FILE *                >**/
/** @(#) keywords:                                                          >**/
/**                                                                          **/
/******************************************************************************/
/*
 *   Les ecritures se font a la fin et ne s'ecrasent pas mutuellement
 *   Mais voir aussi essai18.c
 */
#include <stdio.h>

main()
{
FILE *fp;

/*   pour detruire le contenu du fichier si il existe   */
fp = fopen("essai19.data","w");
fclose(fp);

fp = fopen("essai19.data","a");

if (fork() == 0)
   {
   /*   le fils   */
   /*   -------   */
   int i;

   for (i = 1; i<=10; i++)
      {
      fprintf(fp,"[%8d] : ligne %2d\n",getpid(), i);
      fflush(fp);
      printf("%d\n",getpid());
      sleep(1);
      }
   }
else
   {
   /*   le pere   */
   /*   -------   */
   int i;

   for (i = 1; i<=10; i++)
      {
      fprintf(fp,"[%8d] : ligne %2d\n",getpid(), i);
      printf("%d\n",getpid());
      fflush(fp);
      sleep(1);
      }
   }
}


