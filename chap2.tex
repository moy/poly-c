% \setcounter{chapter}{1}
% \setcounter{page}{31}

\chapter{Les tableaux}
%*********************
Dans ce chapitre nous allons voir
comment déclarer un tableau, comment l'initialiser,
comment faire référence à un des ses éléments.
Du point de vue algorithmique, quand on utilise des tableaux, on a besoin
d'instructions itératives, nous passerons donc en revue l'ensemble
des instructions itératives du langage.
Nous terminerons par un certain nombre d'opérateurs très utiles pour
mettre en {\oe}uvre ces instructions.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Les tableaux}
%=====================
\subsection{Déclaration de tableaux dont les éléments ont un type de base}
%-------------------------------------------------------------------------
\index{declaration@{déclaration}!de tableau}
\index{tableau!declaration@{déclaration}}
\index{tableau!taille de}

Pour déclarer un tableau dont les éléments ont un type de base~:

\begin{itemize}
\item[\ptiret]
   partir de la déclaration de variable ayant un type de base~;
\item[\ptiret]
   ajouter entre crochets le nombre d'éléments du tableau après le nom.
\end{itemize}
Exemple~:
%
\begin{verbatim}
int t[10];                   /*   t tableau de 10 int         */
long int t1[10], t2[20];     /*   t1 tableau de 10 long int, 
                                  t2 tableau de 20 long int   */
\end{verbatim}
%
En pratique, il est recommandé de toujours donner un nom à la constante qui
indique le nombre d'éléments d'un tableau. Exemple~:

\begin{verbatim}
#define N 100
int t[N];
\end{verbatim}
%
Les points importants sont les suivants~:
\begin{itemize}
\item[\ptiret]
les index des éléments d'un tableau vont de 0 à N - 1 ;
\item[\ptiret]
la taille d'un tableau doit être connue statiquement par le compilateur.
\end{itemize}
Impossible donc d'écrire~:
\begin{verbatim}
int t[n];
\end{verbatim}
où \verb+n+ serait une variable.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Initialisation d'un tableau}
%---------------------------------------
\index{initialisation!de tableau}
Il est possible d'initialiser un tableau avec une liste d'expressions
constantes séparées par des virgules, et entourée des signes \verb+{+ et
\verb+}+. Exemple~:

\begin{verbatim}
#define N 5
int t[N] = {1, 2, 3, 4, 5};
\end{verbatim}
%
On peut donner moins d'expressions constantes que le tableau ne
comporte d'éléments.
Dans ce cas, les premiers éléments du tableau
seront initialisés avec les valeurs indiquées, les autres seront
initialisés à zéro. Exemple~:

\begin{verbatim}
#define N 10
int t[N] = {1, 2};
\end{verbatim}
%
Les éléments d'indice 0 et 1 seront initialisés respectivement avec les
valeurs 1 et 2, les autres éléments seront initialisés à zéro.

Il n'existe malheureusement pas de facteur de répétition, permettant
d'exprimer <<~initialiser n éléments avec la même valeur v~>>.
Il faut soit mettre n fois la valeur v dans l'initialisateur, soit initialiser
le tableau par des instructions.

\subsubsection*{Cas particulier des tableaux de caractères}
%..........................................................
\index{tableau!de caracteres@{de caractères}}

\begin{itemize}
\item[$\bullet$]
Un tableau de caractères peut être initialisé par une liste de constantes
caractères.  Exemple~:

\begin{verbatim}
char ch[3] = {'a', 'b', 'c'};
\end{verbatim}
C'est évidemment une méthode très lourde.

\index{chaine litterale@{chaîne littérale}}
\item[$\bullet$] Un tableau de caractères peut être initialisé par une chaîne littérale.
Exemple~:

\begin{verbatim}
char ch[8] = "exemple";
\end{verbatim}

On se rappelle que le compilateur complète toute chaîne littérale avec un
caractère {\it null}, il faut donc que le tableau ait au moins un élément de
plus que le nombre de caractères de la chaîne littérale.

\item[$\bullet$]
Il est admissible que la taille déclarée pour le tableau soit supérieure
à la taille de la chaîne littérale. Exemple~:

\begin{verbatim}
char ch[100] = "exemple";
\end{verbatim}

dans ce cas, seuls les 8 premiers caractères de {\tt ch} seront initialisés.

\item[$\bullet$]
Il est également possible de ne pas indiquer la taille du tableau et dans
ce cas, le compilateur a le bon goût de compter le nombre de caractères de la
chaîne littérale et de donner la taille adéquate au tableau (sans oublier
le {\it null}).
\index{caractere@{caractère}!null@{\it null\/}}
Exemple~:

\begin{verbatim}
char ch[] = "ch aura 22 caractères";
\end{verbatim}

\item[$\bullet$] Il est également possible de donner au tableau une taille égale au nombre
de caractères de la chaîne. Dans ce cas, le compilateur comprend qu'il ne faut
pas rajouter le {\it null} de la fin de chaîne. Exemple~:
\begin{verbatim}
char ville[8] = "bordeaux";
\end{verbatim}

\end{itemize}

\subsection{Référence à un élément d'un tableau}
%-----------------------------------------------
\index{operateur@{opérateur}!indexation}
\index{tableau!element@{élément de}}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

Dans sa forme la plus simple, une référence à un élément de tableau
a la syntaxe suivante~:\\
\\
{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$\hspace{2mm} {\it nom-de-tableau} {\tt [} {\it expression\/$_1$} {\tt ]}\\

\item[$\bullet$] Sémantique~:
{\it expression\/$_1$} doit délivrer une valeur entière, et
l'{\it expression} délivre l'élément d'indice {\it expression\/$_1$}
du tableau.
Une telle {\it expression} est une {\it lvalue}, on peut donc la
rencontrer aussi bien en partie gauche qu'en partie droite d'affectation.
\index{lvalue}

\end{itemize}

Par exemple, dans le contexte de la déclaration~:
\begin{verbatim}
#define N 10
int t[N];
\end{verbatim}
on peut écrire~:
\begin{verbatim}
x = t[i];   /* référence à l'élément d'indice i du tableau t */
t[i+j] = k; /* affectation de l'élément d'indice i+j du tableau t */
\end{verbatim}

\subsection{Chaînes et tableaux de caractères}
%=============================================
\index{chaine de caracteres@{chaîne de caractères}}
\index{tableau!de caracteres@{de caractères}}
Le langage C fait la distinction entre tableau de caractères et chaîne de
caractères~: une chaîne de caractères est un tableau de caractères dont la
fin est indiquée par un caractère {\it null}.
C'est une convention très pratique exploitée par la bibliothèque standard.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Les instructions itératives}
%====================================

\subsection{Instruction {\tt for}}
%---------------------------------
\index{instruction!for@{\tt for}}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it instruction}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{2mm} {\tt for} \hspace{2mm}{\tt (} \hspace{2mm}{\it expression\/$_{1 \hspace{1mm}option}$} \hspace{2mm}{\tt ;} \hspace{2mm}{\it expression\/$_{2 \hspace{1mm}option}$} \hspace{2mm}{\tt ;} \hspace{2mm}{\it expression\/$_{3 \hspace{1mm}option}$} \hspace{2mm}{\tt )}\\
\hspace*{18mm}\hspace{2mm}{\it instruction}

\item[$\bullet$] Sémantique~:
l'exécution réalisée correspond à l'organigramme suivant~:
\end{itemize}

\begin{center}
\setlength{\unitlength}{1mm}
\begin{picture}(100,100)
\put(35,12){\framebox(30,8){expression3}}
\put(35,32){\framebox(30,8){instruction}}
%   on ne peut pas mettre du texte dans l'ovale
\put(50,56){\oval(40,8)}   %   le put est au centre
\put(35,55){expression2 != 0 ?} 
\put(70,56){\vector(1,0){10}}
\put(82,56){fin du for}
\put(73,57){non}
\put(52,49){oui}
\put(35,72){\framebox(30,8){expression1}}
\put(50,12){\line(0,-12){12}}
\put(50,32){\vector(0,-12){12}}
\put(50,52){\vector(0,-12){12}}
\put(50,72){\vector(0,-12){12}}
\put(50,92){\vector(0,-12){12}}
\put(10,0){\line(40,0){40}}
\put(10,0){\line(0,66){66}}
\put(10,66){\vector(1,0){39}}
\end{picture}
\end{center}

Lorsque l'on omet {\it expression$_1$} et/ou {\it expression$_2$}
et/ou {\it expression$_3$}, la
sémantique est celle de l'organigramme précédent, auquel on a enlevé la
ou les parties correspondantes.

\subsubsection*{Remarques}
%.........................
On voit que la vocation de {\it expression\/$_1$} et {\it expression\/$_3$} 
est de réaliser des effets de bord, puisque leur valeur est inutilisée.
\index{effet de bord}
Leur fonction logique est
d'être respectivement les parties initialisation et itération de la boucle.
L' {\it expression\/$_2$} est utilisée pour le test de bouclage.
L'{\it instruction} est le travail de la boucle.

\subsubsection{Exemple de boucle {\tt for}}
%..........................................

Initialisation d'un tableau à zéro~:

\begin{verbatim}
#define N 10
int t[N];
for (i = 0; i < N; i = i + 1) t[i] = 0;
\end{verbatim}

\subsection{Instruction {\tt while}}
%-----------------------------------
\index{instruction!while@{\tt while}}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it instruction}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt while} \hspace{3mm}{\tt (} \hspace{3mm}{\it expression} \hspace{3mm}{\tt )} \hspace{3mm}{\it instruction}

\item[$\bullet$] Sémantique~:

l'exécution réalisée correspond à l'organigramme suivant~:
\end{itemize}

\begin{center}
\setlength{\unitlength}{1mm}
\begin{picture}(100,60)
\put(35,12){\framebox(30,8){instruction}}
%   on ne peut pas mettre du texte dans l'ovale
\put(50,36){\oval(40,8)}   %   le put est au centre
\put(35,35){expression != 0 ?} 
\put(70,36){\vector(1,0){10}}
\put(82,36){fin du while}
\put(73,37){non}
\put(52,29){oui}
\put(50,32){\vector(0,-1){12}}
\put(50,12){\line(0,-1){12}}
\put(0,0){\line(1,0){50}}
\put(0,0){\line(0,1){36}}
\put(0,36){\vector(1,0){29}}
\put(50,50){\vector(0,-1){10}}
\end{picture}
\end{center}

\subsection{Instruction {\tt do}}
%--------------------------------
\index{instruction!do@{\tt do}}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it instruction}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt do} \hspace{3mm}{\it instruction} \hspace{3mm}{\tt while} \hspace{3mm}{\tt (} \hspace{3mm}{\it expression} \hspace{3mm}{\tt )} \hspace{3mm}{\tt ;}

\item[$\bullet$] Sémantique~:

l'exécution réalisée correspond à l'organigramme suivant~:
\end{itemize}

\begin{center}
\setlength{\unitlength}{1mm}
\begin{picture}(100,60)
\put(35,32){\framebox(30,8){instruction}}
%   on ne peut pas mettre du texte dans l'ovale
\put(50,16){\oval(40,8)}   %   le put est au centre
\put(35,15){expression != 0 ?} 
\put(70,16){\vector(1,0){10}}
\put(82,16){fin du do}
\put(73,17){non}
\put(52,9){oui}
\put(50,50){\vector(0,-1){10}}
\put(50,32){\vector(0,-1){12}}
\put(50,12){\line(0,-1){12}}
\put(0,0){\line(1,0){50}}
\put(0,0){\line(0,1){36}}
\put(0,36){\vector(1,0){34}}
\end{picture}
\end{center}

\subsection{Instruction {\tt break}}
%-----------------------------------
\index{instruction!break@{\tt break}}
\label{instruction-break}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it instruction}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt break} \hspace{3mm}{\tt ;}

\item[$\bullet$] Sémantique~:

Provoque l'arrêt de la première instruction {\tt for}, {\tt while},
{\tt do} englobante.
\end{itemize}

\subsubsection*{Exemple}
%.......................
L'instruction {\tt for} ci-dessous est stoppée au premier {\tt i}
tel que {\tt t[i]} est nul~:

\begin{latexonly}
%   newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}

\begin{verbatim}
for (i = 0; i < N; i = i + 1)
   if (t[i] == 0) break;
\end{verbatim}

\subsection{Instruction {\tt continue}}
%--------------------------------------
\index{instruction!continue@{\tt continue}}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it instruction}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt continue} \hspace{3mm}{\tt ;}

\item[$\bullet$] Sémantique~:

Dans une instruction {\tt for}, {\tt while} ou {\tt do}, l'instruction
{\tt continue} provoque l'arrêt de l'itération courante, et le passage
au début de l'itération suivante.

\end{itemize}

\subsubsection*{Exemple}
%.......................
Supposons que l'on parcoure un tableau {\tt t} pour réaliser un certain
traitement sur tous les éléments, sauf ceux qui sont négatifs~:

\begin{verbatim}
for (i = 0; i < N; i = i + 1)
   {
   /*   on passe au i suivant dans le for   */
   if (t[i] < 0 ) continue;   
   ... /*   traitement de l'élément courant     */
   }
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Les opérateurs}
%=======================

\subsection {Opérateur pré et postincrément}
%-------------------------------------------
\index{operateur@{opérateur}!++@{\tt ++}}

Le langage C offre un opérateur d'incrémentation qui peut être utilisé soit
de manière préfixé, soit de manière postfixé.
Cet opérateur se note {\tt ++} et s'applique à une {\it lvalue}.
\index{lvalue}
Sa syntaxe d'utilisation est donc au choix, soit
{\tt ++}~{\it lvalue}  (utilisation en préfixé), soit
{\it lvalue}~{\tt ++} (utilisation en postfixé).
Tout comme l'opérateur d'affectation, l'opérateur d'incrémentation réalise
à la fois un effet de bord et délivre une valeur~:\\
{\tt ++} {\it lvalue} incrémente lvalue de 1 et délivre cette nouvelle
valeur.\\
{\it lvalue} {\tt ++} incrémente lvalue de 1 et délivre {\bf la valeur
initiale} de {\it lvalue}.
\index{effet de bord}
%
\subsubsection*{Exemples}
%.......................

Soient {\tt i} un {\tt int} et {\tt t} un tableau de {\tt int}~:

\begin{verbatim}
i = 0;
t[i++] = 0;  /*  met à zéro l'élément d'indice 0  */
t[i++] = 0;  /*  met à zéro l'élément d'indice 1  */

i = 1;
t[++i] = 0;  /*  met à zéro l'élément d'indice 2  */
t[++i] = 0;  /*  met à zéro l'élément d'indice 3  */
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Opérateur pré et postdécrément}
%------------------------------------------
\index{operateur@{opérateur}!--@{\tt --}}

Il existe également un opérateur de décrémentation qui partage avec
l'opérateur incrément les caractéristiques suivantes~:
\begin{itemize}
\item[\ptiret] il peut s'utiliser en préfixe ou en postfixé ;
\item[\ptiret] il s'applique à une {\it lvalue,} ;
\item[\ptiret] il fait un effet de bord et délivre une valeur.
\end{itemize}
\index{lvalue}
\index{effet de bord}

Cet opérateur se note {\tt --} et décrémente la {\it lvalue} de 1~:\\
{\tt --} {\it lvalue} décrémente lvalue de 1 et délivre
cette nouvelle valeur.\\
{\it lvalue} {\tt --} décrémente lvalue de 1 et délivre
{\bf la valeur initiale} de lvalue.

\subsubsection*{Exemples}
%.......................

Soient {\tt i} un {\tt int} et {\tt t} un tableau de {\tt int}~:

\begin{verbatim}
i = 9;
t[i--] = 0;  /*  met à zéro l'élément d'indice 9  */
t[i--] = 0;  /*  met à zéro l'élément d'indice 8  */

i = 8;
t[--i] = 0;  /*  met à zéro l'élément d'indice 7  */
t[--i] = 0;  /*  met à zéro l'élément d'indice 6  */
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Quelques utilisations typiques de ces opérateurs}
%------------------------------------------------------------
\subsubsection*{Utilisation dans les instructions {\it expression}}
%..................................................................
\index{instruction!expression}

On a vu qu'une des formes d'instruction possibles en C est~:\\
{\it expression} \hspace{3mm}{\tt ;}\\
et que cela n'a de sens que si l'{\it expression} réalise un effet de bord.
Les opérateurs {\tt ++} et {\tt --} réalisant précisément un effet de bord,
permettent
donc d'écrire des instructions se réduisant à une expression utilisant
un de ces opérateurs.
Une incrémentation ou une décrémentation de variable se fait classiquement en
C de la manière suivante~:

\begin{verbatim}
i++;    /*   incrémentation de i   */
j--;    /*   décrémentation de j   */
\end{verbatim}

\subsubsection*{Utilisation dans les instructions itératives}
%............................................................
Une boucle {\tt for} de parcours de tableau s'écrit typiquement de la
manière suivante~:
\begin{verbatim}
for (i = 0; i < N; i++)
   {
   ...
   }
\end{verbatim}

\begin{latexonly}
%   newpage egalement dans la version papier
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {\newpage}
\end{latexonly}

\subsection {Opérateur {\it et logique}}
%---------------------------------------
\index{operateur@{opérateur}!\&\&@{\tt \&\&}}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$} \hspace{3mm}\verb+&&+ \hspace{3mm}{\it expression\/$_2$}

\item[$\bullet$] Sémantique~:

expression$_1$ est évaluée et~:
\begin{enumerate}
\item si sa valeur est nulle, l'expression \verb+&&+ rend la valeur 0 ;
\item si sa valeur est non nulle, expression$_2$ est évaluée, et l'expression
\verb+&&+ rend
la valeur 0 si expression\/$_2$ est nulle, et 1 sinon.
\end{enumerate}
\end{itemize}

On voit donc que l'opérateur \verb+&&+ réalise le {\it et logique} de {\it expression\/$_1$} et
{\it expression\/$_2$} (en prenant pour faux la valeur 0, et pour vrai toute valeur
différente de 0).

\subsubsection*{Remarque sur la sémantique}
%..........................................
%
%   ATTENTION
%   Apres la modif ANSI comme quoi il est valide de prendre l'adresse de
%   l'element (fictif) apres le dernier element d'un tableau, j'avais failli
%   enlever ce qu'il y a dessous. Mais non, ca reste valide. Car si on peut
%   Car si il est valide de prendre l'adresse d'un tel element fictif, il
%   n'est pas valide de dereferencer cette adresse. Ici, t[N] fait bien
%   un acces a cet element fictif
%   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

On a la certitude que {\it expression\/$_2$} ne sera pas évaluée si
{\it expression\/$_1$} rend la valeur faux.
Ceci présente un intérêt dans certains cas
de parcours de tableau ou de liste de blocs chaînés.
Par exemple dans le cas d'un parcours de tableau à la recherche d'un élément
ayant une valeur particulière, supposons que l'on utilise comme test de fin de
boucle l'expression
\begin{verbatim}
i < n && t[i] != 234
\end{verbatim}
(on boucle tant que ce test est vrai),
où \verb+i < n+ est le test permettant de ne pas déborder du tableau,
et \verb+t[i] != 234+
est le test de l'élément recherché.
S'il n'existe dans le tableau aucun élément égal à 234,
il va arriver un moment où on va
évaluer \verb+i < n && t[i] != 234+ avec \verb+i = n+.
La sémantique de l'opérateur \verb+&&+ assurant que 
l'expression \verb+t[i] != 234+ ne sera pas évaluée,
on ne court donc pas le risque d'avoir une erreur matérielle
(\verb+t[n]+ peut référencer une adresse mémoire invalide).

\subsubsection*{Exemples d'utilisation}
%......................................

\begin{verbatim}
int a,b;
if (a > 32 && b < 64) ...
if ( a && b > 1)  ... 
b = (a > 32 && b < 64);
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Opérateur {\it ou logique}}
%--------------------------------------
%   | est un caractere special de makeindex, on l'escape avec \textbar
\index{operateur@{opérateur}!\textbar\textbar@{\tt \textbar\textbar} (ou logique)}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$} \hspace{3mm}\verb+||+ \hspace{3mm}{\it expression\/$_2$}

\item[$\bullet$] Sémantique~:

{\it expression\/$_1$} est évaluée et~:
\begin{itemize}
\item si sa valeur est non nulle, l'expression {\tt ||} délivre la valeur 1 ;
\item sinon, {\it expression\/$_2$} est évaluée, si sa valeur est nulle,
l'expression \verb+||+
délivre la valeur 0 sinon elle délivre la valeur 1.
\end{itemize}
\end{itemize}

On voit donc que l'opérateur \verb+||+ réalise le {\it ou logique} de ses
opérandes,
toujours avec les mêmes conventions pour les valeurs vrai et faux, à savoir
0 pour faux, et n'importe quelle valeur non nulle pour vrai.
Dans ce cas également, on a la certitude que le second opérande ne sera pas
évalué si le premier délivre la valeur vrai.

\subsubsection*{Exemples}
%........................

\begin{verbatim}
int a,b;
if (a > 32 || b < 64) ...
if ( a || b > 1)  ... 
b = (a > 32 || b < 64);
\end{verbatim}

\subsection{Opérateur {\it non logique}}
%---------------------------------------
\index{operateur@{opérateur}!"!@{\tt "!}}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}\verb+!+ \hspace{3mm}{\it expression}

\item[$\bullet$] Sémantique~:

{\it expression} est évaluée, si sa valeur est nulle, l'opérateur \verb+!+ délivre
la valeur 1, sinon il délivre la valeur 0.

Cet opérateur réalise le {\it non logique} de son opérande.
\end{itemize}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Exercice}
%=================

Déclarer un tableau \verb+nb_jour+ qui doit être initialisé de façon à
ce que \verb+nb_jour[i]+ soit égal au nombre de jours du i$^{eme}$ mois de
l'année pour i allant de 1 à 12 (\verb+nb_jour[0]+ sera inutilisé).

Écrire une procédure d'initialisation de \verb+nb_jour+ qui utilisera
l'algorithme suivant~:
\begin{itemize}
\item[\ptiret] si {\tt i} vaut 2 le nombre de jours est 28 ;
\item[\ptiret] sinon si {\tt i} pair et \verb+i <= 7+ ou {\tt i} impair et \verb+i > 7+
le nombre de jours est 30 ;
\item[\ptiret] sinon le nombre de jours est 31.
\end{itemize}

Écrire une procédure d'impression des 12 valeurs utiles de \verb+nb_jour+.
La procédure {\tt main} se contentera d'appeler les procédures
d'initialisation et d'impression de \verb+nb_jour+.

\newpage
\include{exer3}
