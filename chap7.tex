% \setcounter{chapter}{6}
% \setcounter{page}{101}

\cleardoublepage
\chapter{Les expressions}
%************************
Ce chapitre débute par l'étude des conversions, problème qui avait été 
à peine effleuré quand nous avions parlé des opérateurs.
Il se poursuit par la présentation des opérateurs non encore vus
et se termine par l'étude de la sémantique des expressions.

\section{Les conversions de types}
%=================================
\index{conversion!de types}
\index{surcharge!d'operateur@{d'opérateur}}

\subsection{Utilité des conversions}
%-----------------------------------
Dans un programme, dans un contexte où l'on attend une valeur
d'un certain type, il faut normalement fournir une valeur de ce type.
Par exemple, si la partie gauche d'une affectation est de type flottant,
la valeur fournie en partie droite doit également être de type flottant.
Il est cependant agréable de ne pas être trop strict sur cette règle.
Si le type attendu et le type de la valeur fournie sont trop différents,
(on attend un flottant et on fournit une structure),
il est normal que le compilateur considère qu'il s'agit d'une erreur du
programmeur.
Si par contre, le type attendu et le type de la valeur fournie sont assez
<<~proches~>>, c'est une facilité agréable que le compilateur fasse lui-même
la conversion.
On peut admettre par exemple, que dans un contexte où on attend un nombre
flottant, on puisse fournir un nombre entier.

%Chaque langage définit ses propres règles de conversions.
%Si un langage est strict sur la règle << la valeur fournie doit être du type
%attendu >>, on dit qu'il s'agit d'un langage {\it fortement typé}.
%Si un langage est laxiste sur cette règle, on dit qu'il est
%{\it faiblement typé}.
%Le langage C fait partie de la seconde catégorie.

Autre situation où les conversions sont utiles~: les expressions.
Les machines physiques sur lesquelles s'exécutent les programmes
comportent des instructions différentes pour réaliser de l'arithmétique
sur les entiers et sur les flottants.
Cette situation se retrouve dans les langages de programmation de bas
niveau (les assembleurs) où le programmeur doit utiliser des 
opérateurs différents pour réaliser la même opération (au sens mathématique
du terme) selon qu'elle porte sur des entiers ou des flottants.
%
Les langages de programmation de haut niveau par contre, surchargent les
symboles des opérateurs arithmétiques de manière à ce que le même symbole
puisse réaliser une opération indifféremment
entre entiers ou entre flottants~: le symbole {\tt +} permet
de réaliser l'addition de deux entiers ou deux flottants.
Ceci est déjà une facilité agréable, mais il est possible d'aller plus loin.
Le langage peut autoriser le programmeur à donner aux opérateurs des
opérandes de types différents, charge au compilateur de faire une conversion
de type sur l'un ou l'autre des opérandes pour les amener à un type commun.

Enfin, il se peut que le langage offre au programmeur la possibilité
de demander explicitement une conversion de type~: si le langage
{\sc pascal} n'offre pas une telle possibilité, le langage C par contre
dispose d'un opérateur de conversion de type.

\subsection{Ce qu'il y a dans une conversion}
%--------------------------------------------
\index{complement a 2@{complément à 2}}
Pour comprendre ce qui se passe dans une conversion il faut bien distinguer
type, valeur et représentation. La représentation d'une valeur est la chaîne
de bits qui compose cette valeur dans la mémoire de la machine.
La représentation des entiers est une suite de bits en notation binaire simple
pour les positifs, généralement en complément à 2 pour les négatifs.
La représentation des flottants est plus compliquée, c'est généralement un
triplet de chaînes de bits~: (signe, mantisse, exposant).

Une conversion a pour but de changer le type d'une valeur, sans changer
cette valeur si c'est possible~;
elle pourra éventuellement s'accompagner d'un changement de représentation.

Exemple de conversion avec changement de représentation~: la conversion
d'entier vers flottant ou vice versa.
Exemple de conversion sans changement de représentation~: la conversion
d'entier non signé vers entier signé ou vice versa, sur une machine où les
entiers signés sont représentés en complément à 2.

%Quand on converti un entier en flottant, il y a nécessairement changement
%de représentation, et éventuellement changement de valeur en cas d'erreur
%d'arrondi. Par exemple, un entier sur 32 bits a une précision de 10 chiffres
%décimaux, alors qu'un flottant sur 32 bits n'offre qu'une précision de 6
%chiffres décimaux. Par contre, un flottant sur 64 bits offre une précision
%de 15 chiffers décimaux.

%Si le langage offre différentes précisions d'entiers (c'est le cas du langage
%C), convertir un entier court en entier long se fait sans changement de valeur
%simplement par extension du bit de signe.
%Changer un entier long en entier court ne pourra se faire que par troncature
%de bits, le résultat pouvant être aberrant~: ca sera considéré
%comme un cas de débordement de capacité.

%Si le langage offre des entiers signés et non signés, convertir un entier
%signé en un entier non signé (ou vice versa), se fera
%sans changement de représentation, le résultat pouvant être aberrant~: ca sera
%la aussi, considéré comme un cas de débordement de capacité.

\subsection{L'ensemble des conversions possibles}
%------------------------------------------------
\subsubsection{Conversions vers un type entier}
%..............................................
\index{type!entier}
\index{type!flottant}
\index{pointeur!conversion d'un}
\begin{itemize}
\item[$\bullet$] {\bf depuis un type entier}
   La règle est de préserver, si c'est possible, la valeur mathématique de 
   l'objet. Si ce n'est pas possible~: 
   \begin{itemize}
   \item[\ptiret]
      si le type destination est un type signé, on considère qu'il y
      a dépassement de capacité et la valeur du résultat n'est pas définie.
   \item[\ptiret]
      si le type destination est un type non signé, la valeur du résultat
      doit être égale (modulo {\it n}) à la valeur originale, où {\it n} est le
      nombre de bits utilisés pour représenter les valeur du type destination.
   \end{itemize}
   Dans ce qui suit, on se place précisément dans la cas où la machine
   représente les nombres signés en complément à 2 (c'est le cas de
   pratiquement toutes les machines).
   Une conversion d'un entier signé vers un entier non signé, ou vice versa,
   se fait sans changement de représentation.
   Une conversion d'un entier vers un entier plus court se fait par
   troncature des bits les plus significatifs.
   Une conversion d'un entier vers un entier plus long se fait par extension du
   bit de signe si le type originel est signé, par extension de zéros si le
   type originel est non signé.

\item[$\bullet$] {\bf depuis un type flottant}
  La règle est de préserver, si c'est possible, la valeur mathématique de
  l'objet, sachant qu'il peut y avoir une erreur d'arrondi.

\item[$\bullet$] {\bf depuis un pointeur}
   Un pointeur peut être converti en un type entier. Pour cela il est considéré
   comme un type entier non signé de la même taille que les pointeurs.
   Il est ensuite converti dans le type destination selon les règles de 
   conversions d'entiers vers entiers.
\end{itemize}

\subsubsection{Conversions vers un type flottant}
%............................................
Seuls les types entiers et flottants peuvent être convertis en un type flottant.
Là aussi, la règle est de préserver la valeur si possible, sinon c'est un cas
d'overflow ou d'underflow.

\subsubsection{Conversion vers un type pointeur}
%............................................
\index{pointeur!conversion vers}
\index{NULL@{\tt NULL}}
Les différentes possibilités sont les suivantes~:
\begin{itemize}
\item[\ptiret] Un type pointeur vers $T1$ peut être converti en un type pointeur 
   vers $T2$ quels que soient $T1$ et $T2$.
\item[\ptiret] La valeur entière 0 peut être convertie en un type pointeur vers $T$ 
   quel que soit $T$, et c'est la valeur dite de {\it pointeur invalide}.
\item[\ptiret] Une valeur entière non nulle peut être convertie en un type 
   pointeur vers $T$ quel que soit $T$, mais cela est explicitement non 
   portable.
\end{itemize}
%
Nous avons vu précédemment au paragraphe \ref{conversion-tableau}~:
\begin{itemize}
\item[\ptiret] Toute expression de type tableau de {\sc x} est convertie en type
pointeur vers {\sc x}.
\end{itemize}
%
Il y a une règle similaire concernant les fonctions~:
\begin{itemize}
\item[\ptiret] Toute expression de type fonction retournant {\sc x} est convertie
en type pointeur vers fonction retournant {\sc x}.
\end{itemize}

\subsubsection{Conversion vers le type void}
%........................................
\index{void@{\tt void}}
N'importe quelle valeur peut être convertie vers le type void.
Cela n'a de sens que si la valeur résultat n'est pas utilisée.

\subsection{Les situations de conversions}
%-----------------------------------------
\index{promotion des entiers}
Dans le langage C, les situations où se produisent les conversions sont les
suivantes~:
\begin{enumerate}
\item une valeur d'un certain type est utilisée dans un contexte qui 
en demande un autre.
   \begin{itemize}
   \item[\ptiret] passage de paramètre~: le paramètre effectif n'a pas le
   type du paramètre formel~;
   \item[\ptiret] affectation~: la valeur à affecter n'a pas le même type
   que la variable~;
   \item[\ptiret] valeur rendue par une fonction~: l'opérande de {\it return}
   n'a pas le type indiqué dans la déclaration de la fonction.
   \end{itemize}

\item opérateur de conversion~: le programmeur demande explicitement
   une conversion.

\item un opérateur a des opérandes de types différents.
\end{enumerate}

Dans les cas 1 et 2, type de départ et type d'arrivée de la conversion
sont donnés.
Dans le cas 3, par contre, c'est le compilateur qui choisit le type d'arrivée
de la conversion. Il le fait selon des règles soigneusement définies.
Il y en a deux dans le langage C qui portent les noms de <<~promotion des
entiers~>> et <<~conversions arithmétiques habituelles~>>.

\subsection{La promotion des entiers}
%------------------------------------
Ce que l'on appelle dans le langage C {\it promotion des entiers} est une
règle de conversion des opérandes dans les expressions.
La promotion des entiers a pour but d'amener les <<~petits entiers~>>
à la taille des {\tt int}.

\subsubsection{Domaine d'application}
%....................................
La promotion des entiers est appliquée à l'opérande des opérateurs
unaires {\tt +}, {\tt -} et \verb+~+, ainsi qu'aux deux opérandes des opérateurs
de décalage \verb|>>| et \verb|<<|.
La promotion des entiers est également utilisée dans la définition des
{\it conversions arithmétiques habituelles}\/.

\subsubsection{La règle}
%........................
\index{unsigned@{\tt unsigned}}
\index{short@{\tt short}}
Une valeur de type {\tt char}, un {\tt short int} ou un champ de bits,
ou d'une version signée ou non signée des précédents,
peut être utilisée dans un contexte où
{\tt un int} ou un {\tt unsigned int} est demandé.
Cette valeur est convertie en {\tt un int} ou un {\tt unsigned int} 
d'une manière (hélas) dépendante de l'implémentation~:
\begin{itemize}
   \item [\ptiret] si un {\tt int} peut représenter toutes les valeurs du
   type de départ, la valeur est convertie en {\tt int}~;
   \item[\ptiret] sinon, elle est convertie en {\tt unsigned int}.
\end{itemize}

\subsection{Les conversions arithmétiques habituelles}
%-----------------------------------------------------
\index{conversion!arithmetiques habituelles@{arithmétiques habituelles}}

\subsubsection{Domaine d'application}
%....................................
Les conversions arithmétiques habituelles sont réalisés sur les opérandes
de tous les opérateurs arithmétiques binaires sauf les opérateurs de décalage
\verb|>>| et \verb|<<| ainsi que sur les second et troisième opérandes de 
l'opérateur \verb|?:|.

\subsubsection{La règle}
%........................
\index{float@{\tt float}}
\index{double@{\tt double}}
\index{unsigned@{\tt unsigned}}
\index{long@{\tt long}}
\begin{enumerate}
   \item Si un opérande est de type {\tt long double}, l'autre opérande est 
   converti en {\tt long double}.
   \item Sinon si un opérande est de type {\tt double}, l'autre opérande est 
   converti en {\tt double}.
   \item Sinon si un opérande est de type {\tt float}, l'autre opérande est 
   converti en {\tt float}.
   \item Sinon la promotion des entiers est réalisée sur les deux opérandes.
   Ensuite~:
   \begin{itemize}
      \item[a.] Si un opérande est de type {\tt unsigned long int}, l'autre
         opérande est converti en {\tt unsigned long int}.
      \item[b.] Sinon, si un opérande est de type {\tt long int} et l'autre
         de type {\tt unsigned int}, alors~:
         \begin{itemize}
	    \item[\ptiret] si un {\tt long int} peut représenter toutes les valeurs
		  d'un {\tt unsigned int}, l'opérande de type
		  {\tt unsigned int} est converti en {\tt long int}.
	    \item[\ptiret] sinon, les deux opérandes sont convertis en 
		  {\tt unsigned long int}.
          \end{itemize}
      \item[c.] Sinon, si un opérande est de type {\tt long int}, l'autre
	 opérande est converti en {\tt long int}.
      \item[d.] Sinon, si un opérande est de type {\tt unsigned int}, l'autre
         opérande est converti en {\tt unsigned int}.
      \item[e.] Sinon, les deux opérandes sont de même type, et il n'y a pas
	 de conversion à réaliser.
   \end{itemize}
\end{enumerate}

\subsubsection{Discussion}
%.........................
Les points 1, 2, 3 sont faciles à comprendre~: si les deux opérandes sont
flottants, celui de moindre précision est converti dans le type de
l'autre. Si un seul des opérandes est de type flottant, l'autre est 
converti dans ce type.

On aborde le point 4 si les deux opérandes sont des variétés d'entiers
courts, normaux ou longs, signés ou non signés.
On applique alors
la promotion des entiers, de manière à se débarrasser des entiers courts.
À la suite de cela, il n'y plus comme types possibles que {\tt int}, 
{\tt unsigned int}, {\tt long int} et {\tt unsigned long int}.

Si l'on excepte les cas où les deux types sont identiques, 
le reste des règles peut se résumer dans le tableau suivant~:\\
\begin{center}
\begin{tabular}{|c|c|c|}\hline
opérande &  opérande & résultat \\ \hline 
{\tt unsigned long int} & {\it quelconque} & {\tt unsigned long int} \\ \hline
{\tt long int}          & {\tt unsigned int}  &
   \begin{minipage}{4cm}
   \begin{center}
   {\tt long int} \\
   \hrule
   {\tt unsigned long int}
   \end{center}
   \end{minipage}                                           \\ \hline
{\tt long int}          & {\tt int}  &  {\tt long int}      \\ \hline
{\tt unsigned int}      & {\tt int}  &  {\tt unsigned int}  \\ \hline
\end{tabular}
\end{center}

%-------------------------------------------------------------------------------
%   ABANDONNE MAIS ON NE SAIT JAMAIS ...
%
%\subsubsection{Ce que désire le programmeur}
%%.........................................
%Le programmeur désire qu'un conversion de type se fasse sans changement de
%la valeur. Si une fonction attend en paramètre un flottant, et qu'il lui
%passe un entier, le programmeur s'attend à ce que l'entier soit converti 
%de la représentation entière à la représentation flottante, sans changement
%de la valeur (avec à la rigueur une erreur d'arrondi).
%Bien entendu, changer de type sans changer la valeur n'est pas toujours
%possible. La conversion d'un {\tt int} ayant une valeur négative en
%en {\tt unsigned int}, est impossible sans modifier la valeur.
%
%
%
%\item un opérateur a des opérandes de types différents.
%Le langage C possède une grande variété de types de base~,
%en particulier quatre types d'entiers
%({\tt char}, {\tt short int}, {\tt int}, {\tt long int}) et trois types de
%flottants ({\tt float}, {\tt double} et {\tt long double}).
%Il accepte d'autre part que les opérandes des expressions soient de types
%différents.
%Si i est un {\tt short int}, j un {\tt int} et k un {\tt long int}
%il est valide d'écrire {\tt i*j + k}.
%
%Les machines physiques par contre, ne disposent pas d'instructions ayant
%des opérandes de types différents.
%Une machine qui peut additionner deux entiers sur 8 bits, deux entiers sur 16
%bits ou deux entiers sur 32 bits, ne pourra pas additionner un entier 8 bits
%et un entier 16 bits.
%
%Ce problème se résoud de la manère suivante~:
%lorsque le compilateur rencontre un opérateur ayant des opérandes de types
%différents, il les convertit pour les amener à un type commun.
%Dans ce type de conversion, le type de départ est donné, mais le type d'arrivée
%est choisi par le compilateur.
%Il y a deux règles de choix du type d'arrivée qui portent les 
%noms\footnote{C'est du jargon C}
%de {\it promotion des entiers} et {\it conversions arithmétiques habituelles}
%\end{itemize}

%-------------------------------------------------------------------------------

\subsection{Les surprises des conversions}
%-----------------------------------------
\index{unsigned@{\tt unsigned}}
\index{operateur@{opérateur}!sizeof@{\tt sizeof}}
D'une manière générale, les conversions sont un mécanisme qui fonctionne à la
satisfaction du programmeur.
Il y a cependant une situation où cela peut donner des résultats surprenants~:
quand on réalise une comparaison entre entiers signés et entiers non signés.
Par exemple, le programme suivant~:
\begin{verbatim}
int main(void)
{
unsigned int i = 0;

if (i < -1 )
   printf("Bizarre, bizarre ...\n");
   else printf ("Tout semble normal\n");
}
\end{verbatim}
%
imprimera le message {\tt Bizarre, bizarre ...}, pouvant laisser croire que
pour le langage C, $0$ est inférieur à $-1$.

L'explication est la suivante~: l'opérateur $<$ a un opérande de type
{\tt unsigned int} (la variable {\tt i}), et un autre opérande de type
{\tt int} (la constante -1).
D'après le tableau des conversions donné ci-dessus, on voit que dans un tel
cas, les opérandes sont convertis en {\tt unsigned int}.
Le compilateur génère donc une comparaison non signée entre 0 et 4294967295
(puisque -1 = 0xffffffff = 4294967295), d'où le résultat.

Pour que tout rentre dans l'ordre, il suffit d'utiliser l'opérateur de
conversion pour prévenir le compilateur de ce qu'on veut faire~:
\begin{verbatim}
int main(void)
{
unsigned int i = 0;

if ((int) i < -1 )    /*   comparaison entre deux int   */
   printf("Bizarre, bizarre ...\n");
   else printf ("Tout semble normal\n");
}
\end{verbatim}

Là où tout se complique c'est qu'on peut utiliser des entiers non signés
sans le savoir~!
Considérons le programme suivant~:
\begin{latexonly}
%   newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}
\begin{verbatim}
int main(void)
{
if (sizeof(int) < -1)
   printf("Bizarre, bizarre ...\n");
else printf ("Tout semble normal\n");
}
\end{verbatim}
%
le lecteur a sans doute deviné qu'il va imprimer le message {\tt Bizarre,
bizarre ...}, et cependant les entiers n'ont pas une longueur négative~!
L'explication est la suivante~: 
l'opérateur {\tt sizeof} rend une valeur dont le type est non signé.
Voici ce que dit exactement la norme~: << {\it La valeur du résultat} [ de 
{\tt sizeof} ] {\it dépend de l'implémentation, et son type (un type entier
non signé) est }{\tt size\_t }{qui est définit dans le fichier d'include}
{\tt stddef.h} >>.
Dans notre exemple, le compilateur a généré une comparaison non signée entre
4 ({\tt sizeof(int)}) et $4\:294\:967\:295$, d'où le résultat.

\subsubsection{Recommandations}
%..............................
\begin{enumerate}
\item Ne jamais mélanger des entiers signés et non signés dans des
  comparaisons~: utiliser l'opérateur de conversion pour amener l'opérateur de
  comparaison à avoir des opérandes de même type.
  \item Bien noter que l'opérateur {\tt sizeof} rend une valeur de type
   entier non signé.
\end{enumerate}

\section{Les opérateurs}
%=======================
Nous avons étudié les opérateurs arithmétiques usuels dans le chapitre sur
les bases du langage, les opérateurs incrément et décrément dans le
chapitre sur les tableaux, les opérateurs d'adresse dans le chapitre sur
les pointeurs et les opérateurs de sélection dans le chapitre sur les
structures.

Le langage C comporte quelques autres opérateurs que nous allons étudier
ci-après.

\subsection{Opérateur {\it non bit à bit}}
%-----------------------------------------
\index{operateur@{opérateur}!\~@{\verb+~+}(tilde)}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}\verb+~+ \hspace{3mm}{\it expression}

\item[$\bullet$] Sémantique~:

{\it expression} est évaluée et doit délivrer une valeur de type entier, 
l'opération {\it non bit à bit} est réalisée sur cette valeur, et le
résultat obtenu est la valeur de l'expression \verb+~+.

\end{itemize}

\subsection{Opérateur {\it et bit à bit}}
%----------------------------------------
\index{operateur@{opérateur}!\&@{\tt \&}!et bit à bit}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$} \hspace{3mm}\verb+&+ \hspace{3mm}{\it expression\/$_2$}

\item[$\bullet$] Sémantique~:

Les deux expressions sont évaluées et doivent délivrer des valeurs de type
entier, le {\it et bit à bit} est  réalisé, et la valeur
obtenue est la valeur de l'expression \verb+&+.

\end{itemize}

\subsection{Opérateur {\it ou bit à bit} : \textbar}
%----------------------------------------
\index{operateur@{opérateur}!textbar@{\tt \textbar} (ou bit a bit)}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$} \hspace{3mm}{\tt |} \hspace{3mm}{\it expression\/$_2$}

\item[$\bullet$] Sémantique~:

Les deux expressions sont évaluées et doivent délivrer des valeurs de type
entier, le {\it ou bit à bit} est  réalisé, et la valeur
obtenue est la valeur de l'expression {\tt |}.

\end{itemize}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Opérateur {\it ou exclusif bit à bit}}
%-------------------------------------------------
\index{operateur@{opérateur}!\^@{\verb+^+}}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$} 
\hspace{3mm}\verb+^+ \hspace{3mm}{\it expression\/$_2$}

\item[$\bullet$] Sémantique~:

Les deux expressions sont évaluées et doivent délivrer des valeurs de type
entier, le {\it ou exclusif bit à bit} est  réalisé, et la valeur
obtenue est la valeur de l'expression \verb+^+.

\end{itemize}

\subsection{Opérateur {\it décalage à gauche}}
%---------------------------------------------
\index{operateur@{opérateur}!<{}<@{\tt <{}<}}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$} \hspace{3mm}\verb|<<| \hspace{3mm}{\it expression\/$_2$}

\item[$\bullet$] Sémantique~:

Les deux expressions sont évaluées et doivent délivrer des valeurs de type
entier, la valeur de {\it expression\/$_1$} est décalée à gauche de 
{\it expression$_2$} bits en remplissant les bits libres avec des zéros.
Le résultat obtenu est la valeur de l'expression \verb|<<|.

\end{itemize}

\subsection{Opérateur {\it décalage à droite}}
%---------------------------------------------
\index{unsigned@{\tt unsigned}}
\index{operateur@{opérateur}!>{}>@{\tt >{}>}}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$}
   \hspace{3mm}{\verb|>>|} \hspace{3mm}{\it expression\/$_2$}

\item[$\bullet$] Sémantique~:

Les deux expressions sont évaluées et doivent délivrer des valeurs de type
entier, la valeur de {\it expression\/$_1$} est décalée à droite de 
{\it expression$_2$} bits.
Si {\it expression\/$_1$} délivre une valeur {\tt unsigned}, le décalage
est un décalage logique~: les bits libérés sont remplis avec des zéros.
Sinon, le décalage peut être logique ou arithmétique (les bits libérés
sont remplis avec le bit de signe), cela dépend de l'implémentation.

\end{itemize}
\begin{latexonly}
%   newpage egalement dans la version papier
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {\newpage}
\end{latexonly}

\subsection{Opérateur conditionnel}
%----------------------------------
\index{operateur@{opérateur}!?:@{\tt ?:}}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$} \hspace{3mm}{\tt ?} \hspace{3mm}{\it expression\/$_2$}
\hspace{3mm}{\tt~:} \hspace{3mm}{\it expression\/$_3$}

\item[$\bullet$] Sémantique~:

{\it expression\/$_1$} est évaluée et doit délivrer une valeur de type
entier.
Si cette valeur est~:

\begin{itemize}
\item[\ptiret] non nulle, {\it expression\/$_2$} est évaluée et le résultat est la
valeur de l'expression conditionnelle.
\item[\ptiret] nulle, {\it expression\/$_3$} est évaluée et le résultat est la
valeur de l'expression conditionnelle.
\end{itemize}

\end{itemize}

\subsubsection*{Exemples}
%........................
Cet opérateur permet de remplacer une instruction {\tt if}~:
\begin{verbatim}
max = a > b ? a : b;
\end{verbatim}
On peut utiliser cet opérateur en cascade, mais la lisibilité en souffre~:
\begin{verbatim}
printf("i est %s",
       i < 0 ? "negatif\n" 
             : i > 0 ? "positif\n"
                     : "nul\n");
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Opérateur {\it virgule}}
%-----------------------------------
\index{operateur@{opérateur}!,:@{\tt ,}(virgule)}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it expression\/$_1$} \hspace{3mm}{\tt ,} \hspace{3mm}{\it expression\/$_2$}

\item[$\bullet$] Sémantique~:

{\it expression\/$_1$} est évaluée et sa valeur ignorée.
{\it expression\/$_2$} est évaluée et sa valeur est la valeur de
l'expression {\it expression\/$_1$} \hspace{3mm}{\tt ,}
\hspace{3mm}{\it expression\/$_2$}.

\end{itemize}

\subsubsection*{Remarque}
%........................
Étant donné que la valeur de {\it expression\/$_1$} est ignorée, pour qu'une
telle construction ait un sens, il faut que {\it expression\/$_1$} fasse un
effet de bord.
On peut écrire par exemple~:
%
\begin{verbatim}
i = (j = 2 , 1);
\end{verbatim}
%
ce qui est une manière particulièrement horrible d'écrire~:
%
\begin{verbatim}
i = 1;
j = 2;
\end{verbatim}

Une utilisation agréable par contre de l'opérateur virgule est dans les
expressions d'une boucle for.
Si on désire écrire une boucle {\tt for} qui utilise deux index, il est
utile d'écrire par exemple~:

\begin{verbatim}
for (i = 1, j = 1; i <= LIMITE; i++, j = j + 2)
   {
   ...
   }
\end{verbatim}
%
ceci permet de rendre manifeste que {\tt i = 1} et {\tt j = 1} sont la
partie initialisation et {\tt i++} et {\tt j = j + 2} sont la partie itération
de la boucle.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Opérateurs d'affectation composée}
%---------------------------------------------
\index{operateur@{opérateur}!affectation composée}
Chacun des opérateurs \verb:+ - * / % >> << & ^ |: peut s'associer à 
l'opérateur d'affectation pour former respectivement les opérateurs
\verb:+=: \verb:-=: \verb:*=: \verb:/=: \verb:%=: \verb:>>=:
\verb:<<=: \verb:&=: \verb:^=: \verb:|=:.

Nous donnerons la syntaxe et la sémantique de ces opérateurs dans le cas
de l'opérateur {\tt +=}, celles des autres s'en déduit immédiatement.

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it lvalue} \hspace{3mm}{\tt +=} \hspace{3mm}{\it expression}

\item[$\bullet$] Sémantique~:

{\it lvalue} \hspace{3mm}{\tt =} \hspace{3mm}{\it lvalue} \hspace{3mm}{\tt +}\hspace{3mm}{\it expression}

\end{itemize}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Opérateur {\it conversion}}
%--------------------------------------
\index{operateur@{opérateur}!conversion}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it expression}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt (} \hspace{3mm}{\it type} \hspace{3mm}{\tt )} \hspace{3mm}{\it expression}

\item[$\bullet$] Sémantique~:
{\it expression} est évaluée et convertie dans le type indiqué par
{\it type}.

\end{itemize}

\subsubsection*{Note}
%....................
Dans le jargon C, l'opérateur de conversion de type s'appelle un {\it cast}.
Dans le vocabulaire des langages de programmation en général, une conversion
de type s'appelle en anglais une {\it coertion}, que l'on peut traduire par
contrainte ou coercition. Le mot anglais {\it cast} signifie plâtre (pour 
maintenir un membre brisé), il donne donc bien une idée de contrainte,
mais c'est quand même un choix bizarre.

\subsubsection*{Exemples d'utilisation}
%......................................
\index{pointeur!generique@générique}
\index{little endian@{\it little endian}}
\index{big endian@{\it big endian}}
\index{extern@{\tt extern}}
\index{operateur@{opérateur}!sizeof@{\tt sizeof}}
\index{void@{\tt void}}
%   ----   fin des index ---
%
L'opérateur de conversion est devenu moins utile avec la normalisation
du langage C.
Dans {\sc k\&r} C, il était utilisé essentiellement pour deux raisons~:
\begin{enumerate}
\item à cause de l'absence de pointeur générique {\tt void *}.
En effet, les procédures d'allocation de mémoire comme {\tt malloc} étaient
définies par~:
\begin{verbatim}
extern char * malloc();
\end{verbatim}
ce qui nécessitait d'utiliser l'opérateur de conversion de type à chaque
utilisation~:
\begin{verbatim}
p1 = (struct s1 *) malloc(sizeof(struct s1));
p2 = (struct s2 *) malloc(sizeof(struct s2));
\end{verbatim}

\item à cause de l'absence de prototype de fonction qui rendait
impossible la déclaration du type des paramètres des
fonctions externes. 
Si une procédure {\tt p} attendait un paramètre de type {\tt float}, et si on
désirait lui passer la valeur possédée par la variable {\tt i} de type
{\tt int}, il ne fallait pas écrire {\tt p(i)} mais {\tt p((float)~i)}.

\end{enumerate}

Il reste cependant un certain nombre de situations où l'opérateur de conversion
est nécessaire.
En voici un exemple.
Il s'agit d'un
programme qui a pour but de déterminer si l'architecture de la machine
est de type {\it little endian} ou {\it big endian}.
Il faut regarder l'ordre des octets dans un entier,
d'où la nécessité de l'opérateur de conversion.
Ce programme suppose que les {\it int} sont implémentés sur 4 octets.

\begin{latexonly}
%   newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}

\begin{verbatim}
int i = 0x01020304;
char *p;

p = (char *) &i;    /*   int * transformé en char *   */
if (*p++ == 1 && *p++ == 2 && *p++ == 3 && *p++ == 4 )
   printf("big endian\n");
else
   {
   p = (char *) &i;
   if (*p++ == 4 && *p++ == 3 && *p++ == 2 && *p++ == 1 )
      printf("little endian\n");
   else printf("architecture exotique !!\n");
   }
\end{verbatim}
Exécuté sur une machine Sun à processeur {\sc sparc}, ce programme répondra
{\it big endian}, exécuté sur un {\sc pc} à processeur Intel, il répondra 
{\it little endian}.

\section{Sémantique des expressions}
%===================================
\subsection{Opérateurs d'adressage}
%----------------------------------
\index{operateur@{opérateur}!d'adressage}
Dans le langage C, les constructions suivantes~:

\begin{center}
\begin{tabular}{c|c}
   \verb+()+ &pour l'appel de procédure\\
   \verb+[]+ &pour l'indexation\\
   \verb+*+ &pour l'indirection\\
   \verb+.+ &pour la sélection de champ\\
   \verb+->+ &pour l'indirection et sélection\\
   \verb+&+ &pour délivrer l'adresse d'un objet
\end{tabular}
\end{center}
%
sont des opérateurs à part entière.
Cela signifie que ces opérateurs, que l'on peut appeler opérateurs
d'adressage, ont une priorité et sont en
concurrence avec les autres opérateurs pour déterminer la sémantique
d'une expression.
Par exemple, la sémantique de l'expression \verb|*p++| ne peut se déterminer
que si l'on connaît les priorités relatives des opérateurs \verb+*+ et
\verb|++|.

\subsection{Priorité et associativité des opérateurs}
%----------------------------------------------------
\index{priorite@priorité!d'operateur@{opérateur}}
\index{associativite@associativité!d'operateur@{opérateur}}
Pour déterminer la sémantique d'une expression il faut non seulement 
connaître la priorité
des opérateurs mais également leur associativité.
En effet, seule la connaissance de l'associativité de l'opérateur \verb+==+
permet de savoir si \verb+ a == b == c+ signifie \verb+(a == b) == c+
ou si elle signifie \verb+a == (b == c)+.

Un opérateur a une associativité à droite quand~:
\newline
{\tt a} \hspace{3mm}{\it op} \hspace{3mm}{\tt b} \hspace{3mm}{\it op} \hspace{3mm}{\tt c}
signifie \hspace{3mm}{\tt a} \hspace{3mm}{\it op} \hspace{3mm}{\tt ( b} \hspace{3mm}{\it op} \hspace{3mm}{\tt c)}.

Un opérateur a une associativité à gauche quand~:
\newline
{\tt a} \hspace{3mm}{\it op} \hspace{3mm}{\tt b} \hspace{3mm}{\it op} \hspace{3mm}{\tt c\hspace{3mm}}
signifie \hspace{3mm}{\tt (a} \hspace{3mm}{\it op} \hspace{3mm}{\tt b)} \hspace{3mm}{\it op} \hspace{3mm}{\tt c}.

Nous donnons ci-dessous le tableau exhaustif des opérateurs avec leurs
priorités et leurs associativité.

%\begin{minipage}[t]{14cm}
\begin{center}
\begin{tabular}{c|l|c}
priorité &Opérateur &Associativité\\ \hline
16 &\verb+()+ \hspace{3mm}\verb+[]+
	      \hspace{3mm}\verb+->+
	      \hspace{3mm}\verb+.+ 
              \verb|++| \footnote{postfixé}
	      \hspace{3mm}\verb+--+ \footnote{postfixé} & G\\
15 &\verb+!+ \hspace{3mm}\verb+~+ 
	     \hspace{3mm}\verb|++| \footnote{préfixé}
	     \hspace{3mm}\verb+--+ \footnote{préfixé} 
	     \hspace{3mm}\verb+-+ \footnote{unaire}
	     \hspace{3mm}\verb|+| \footnote{unaire}
	     \hspace{3mm}\verb+*+ \footnote{indirection}
	     \hspace{3mm}\verb+&+ \footnote{adresse de}
	     \hspace{3mm}{\tt sizeof} &D\\
14 & {\it conversion} & D\\
13 &\verb+*+ \footnote{multiplication} \hspace{3mm}\verb+/+ \hspace{3mm}\verb+%+ & G\\
12 &\verb|+| \hspace{3mm}\verb+-+ & G\\
11 &\verb+<<+ \hspace{3mm}\verb+>>+ & G\\
10 &\verb+<+ \hspace{3mm}\verb+<=+ \hspace{3mm}\verb+>+ \hspace{3mm}\verb+>=+ & G\\
9 &\verb+==+ \hspace{3mm}\verb+!=+ & G\\
8 &\verb+&+ \footnote{et bit bit} & G\\
7 &\verb+^+ & G\\
6 &\verb+|+ & G\\
5 &\verb+&&+ & G\\
4 &\verb+||+ & G\\
3 &\verb+?:+ & D\\
2 &\verb+=+ \hspace{2mm}\verb|+=| \hspace{2mm}\verb+-=+ \hspace{2mm}\verb+*=+ \hspace{2mm}\verb+/=+ \hspace{2mm}\verb+%=+ \hspace{2mm}\verb+>>=+ \hspace{2mm}\verb+<<=+ \hspace{2mm}\verb+&=+ \hspace{2mm}\verb+^=+ \hspace{2mm}\verb+|=+ & D\\
1 &\verb+,+ &G

\end{tabular}
\end{center}
%\end{minipage}

\subsubsection*{Discussion}
%.........................
Les choix faits pour les priorités des opérateurs sont assez mauvais,
les concepteurs du langage eux-mêmes en conviennent.
\footnote{The C programming langage, page 3~: C, like any other language, has
its blemishes. Some of the operators have the wrong precedence;}
Les choix les plus irritants sont les suivants~:
\begin{itemize}
\item[\ptiret]
La précédence des opérateurs bits à bits est plus petite que celle
des opérateurs de comparaison. Donc {\tt a\&b == c} ne signifie
pas {\tt (a\&b) == c}, mais {\tt a \& (b==c)}.

\item[\ptiret]
La précédence des opérateurs de décalage est plus petite que celle
des opérateurs de {\tt +} et {\tt -}. Donc \verb|a << 4 + b| signifie
\verb|a << (4 + b)|.
\end{itemize}

\subsubsection*{Recommandation}
%..............................
Il est considéré comme un bon style de programmation en C, de systématiquement
parenthéser les expressions dès qu'elles comportent d'autres opérateurs
que les opérateurs de l'arithmétique usuelle.

\subsection{Ordre d'évaluation des opérandes}
%--------------------------------------------
\index{ordre!d'evaluation@{d'évaluation}}
À part quelques exceptions,
l'ordre d'évaluation des opérandes d'un opérateur n'est pas
spécifié par le langage.
Ceci a pour conséquence que le programmeur doit faire extrêmement attention
aux effets de bords dans les expressions.
Par exemple, l'instruction~:
\begin{verbatim}
t[i] = f();
\end{verbatim}
où la fonction {\tt f} modifie la valeur de {\tt i} a un comportement
indéterminé~: il est impossible de savoir si la valeur prise
pour indexer {\tt t} sera celle de {\tt i} avant ou après l'appel à {\tt f}.

\section{Récréation}
%===================
\index{shell@{\it shell}}
Voici en illustration de l'opérateur \verb|~|, la contribution de
Jack Applin a la compétition du code C le plus obscur ({\sc ioccc}) de 1986.
Ce programme a la propriété extraordinaire d'être un source valide à la fois
pour le shell \verb|/bin/sh|, le langage C et {\sc fortran}~!
Voici le source~:
\begin{verbatim}
cat =13 /*/ >/dev/null 2>&1; echo "Hello, world!"; exit
*
*  This program works under cc, f77, and /bin/sh.
*
*/; main() {
      write(
cat-~-cat
     /*,'(
*/
     ,"Hello, world!"
     ,
cat); putchar(~-~-~-cat); } /*
     ,)')
      end
*/
\end{verbatim}

\subsubsection*{La version shell}
%................................
La commande \verb|cat =13| est une commande incorrecte (à cause du blanc entre
\verb|cat| et le signe {\tt =}), mais comme l'{\it erreur standard} est
redirigée sur \verb|/dev/null|, le message d'erreur n'apparaît pas.
Ensuite, l'echo imprime <<~Hello world~>>, puis le shell fait exit.
Le reste du source lui est donc indifférent.
Ensuite, les deux versions C et fortran sont mélangées grâce à un emploi 
judicieux des commentaires (en fortran, toute ligne commençant par \verb|*| ou
\verb|c| est un commentaire).

\subsubsection*{La version fortran}
%...................................
Une fois débarrassé des commentaires fortran, le source devient~:
\begin{verbatim}
      write( *,'("Hello, world!")')
      end
\end{verbatim}
ce qui imprime <<~Hello world~>>.
\begin{latexonly}
%   newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}

\subsubsection*{La version C}
%............................
Après nettoyage des commentaires C, le source original devient~:
\begin{verbatim}
cat =13 ;
main()
{
write( cat-~-cat ,"Hello, world!" , cat);
putchar(~-~-~-cat); 
}
\end{verbatim}

La déclaration {\tt cat =13 ;} est valide en C {\sc k\&r} mais obsolète 
en {\sc ansi} C~: elle est équivalente à {\tt int cat =13 ;}
Cette forme est cependant encore généralement acceptée (avec un warning)
par les compilateurs.
La suite ne fonctionne correctement que sur une machine satisfaisant aux
deux contraintes suivantes~:
\begin{enumerate}
\item être une machine {\sc unix} pour disposer de l'appel noyau {\tt write}~;
\item avoir les entiers négatifs représentés en complément à 2.
Dans ce cas en effet, \verb|~-x| vaut \verb|x - 1|.
\end{enumerate}
Donc \verb|cat-~-cat| vaut 1 qui, en premier paramètre de {\tt write}
désigne la {\it sortie standard}, et \verb|~-~-~-13| vaut 10 (le code de
{\it newline}).
Le troisième paramètre passé à {\tt write} doit être la longueur de la chaîne à 
imprimer, ici 13 qui est bien la longueur de \verb|Hello, world!|.
Au final, ce programme imprime <<~Hello world~>>.
