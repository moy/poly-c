#define LIGNE_BLANCHE printf("\n");
main()
{
/*   DEBUT   */
/*   le cas le plus simple en positif et negatif   */
printf("|%d|\n",1234);
printf("|%d|\n",-1234);

/*   Utilisation de l'indicateur +   */
printf("|%+d|\n",1234);
printf("|%+d|\n",-1234);

/*   Utilisation de l'indicateur <space>   */
printf("|% d|\n",1234);
printf("|% d|\n",-1234);

/*   cas simple en hexadecimal   */
printf("|%x|\n",0x56ab);
printf("|%X|\n",0x56ab);

/*   influence de # sur l'hexadecimal   */
printf("|%#x|\n",0x56ab);
printf("|%#X|\n",0x56ab);

/*   cas simple en octal   */
printf("|%o|\n",1234);

/*   influence de # sur l'octal   */
printf("|%#o|\n",1234);
LIGNE_BLANCHE

/*   taille et precision   */
printf("|%10d|\n",1234);
printf("|%10.6d|\n",1234);
printf("|%.6d|\n",1234);

printf("|%*.6d|\n",10,1234);
printf("|%*.*d|\n",10,6,1234);
LIGNE_BLANCHE

/*   format f   */
printf("|%f|\n",1.234567890123456789e5);
printf("|%.4f|\n",1.234567890123456789e5);
printf("|%.20f|\n",1.234567890123456789e5);
printf("|%20.4f|\n",1.234567890123456789e5);
LIGNE_BLANCHE

/*   format e   */
printf("|%e|\n",1.234567890123456789e5);
printf("|%.4e|\n",1.234567890123456789e5);
printf("|%.20e|\n",1.234567890123456789e5);
printf("|%20.4e|\n",1.234567890123456789e5);
LIGNE_BLANCHE

/*   format g   */
printf("|%.4g|\n",1.234567890123456789e-5);
printf("|%.4g|\n",1.234567890123456789e5);
printf("|%.4g|\n",1.234567890123456789e-3);
printf("|%.8g|\n",1.234567890123456789e5);
LIGNE_BLANCHE

/*   FIN   */
}
