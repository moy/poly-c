\appendix
\chapter{Les jeux de caractères}
%*******************************
Un jeu de caractère est un ensemble de signes typographiques et un
codage~: à chaque signe est associé un nombre entier qui est son code.

\section{Les normes}
%===================
L'histoire des jeux de caractères pour les besoins de l'informatique
débute avec le codage {\sc ebcdic} (sur 8 bits) utilisé par {\sc ibm} et le
codage {\sc ascii} (sur 7 bits) utilisé par le reste du monde.
Le code {\sc ebcdic} n'est pas une norme, mais le code {\sc ascii} est une
norme de l'{\sc ansi}.

En 1963, l'{\sc iso} crée une norme internationale à partir de la norme {\sc
ascii} et, internationalisation oblige, elle réserve 10 caractères pour des
variantes nationales. La France (dans une norme {\sc nf}) en utilisera 5
pour y caser
des lettres accentuées (à ç é è ù) laissant de côté l'accent circonflexe
si cher aux académiciens français, le tréma et les ligatures.
Cette norme est donc inutilisable pour écrire réellement en français, mais il
faut dire à la décharge de l'{\sc iso}, qu'en utilisant un code à 7 bits,
cela était impossible.

En 1988, l'{\sc iso} édite une nouvelle norme de jeu de caractères à 8 bits
cette fois pour prendre en compte les particularités des langues européennes.
Mais la diversité est telle qu'il n'est pas possible de mettre tous les
signes typographiques dans un seul jeu de 256 caractères. Les langues ont
donc été regroupées en familles, et la norme {\sc iso-}{\small 8859} comprend
plusieurs jeux de caractères. Celui qui nous intéresse est celui qui est
prévu pour les langues d'europe occidentale et qui est référencé
{\sc iso-}{\small 8859-1} ou {\sc iso-latin-}{\small 1}.
Malheureusement, les français ne sont pas de très bons lobbyistes dans les
instances internationales, et aussi incroyable que cela puisse
paraître, la ligature {\oe} du français a été oubliée~!

\subsubsection*{Pour en savoir plus}
%...................................
Il existe un excellent article de Jacques André et Michel Goosens sur les
problèmes de normalisation de codage de caractères, librement accessible
via l'{\sc url}~:\\
{\tt http://cahiers.gutenberg.eu.org/}.
Il s'agit de la revue <<~les cahiers de GUTenberg~>> , et l'article en question est
dans le cahier 20.
Pour le problème de la ligature {\oe}, voir le cahier 25.

\section{Le code {\tt ascii}}
%============================
Le jeu de caractères est formé d'un ensemble de caractères de commandes et
de caractères graphiques. 
L'ensemble des caractères de commande est formé de six familles~:
\begin{itemize}
\item[$\bullet$] commandes de format                  %   PREMIERE FAMILLE
\begin{center}
\begin{tabular}{|l|c|} \hline
commande & nom \\ \hline
{\it carriage return} & CR \\
{\it line feed} & LF \\
{\it backspace} & BS \\
{\it horizontal tabulation} & HT \\
{\it vertical tabulation} & VT \\
{\it space} & SP \\
{\it form feed} & FF \\ \hline
\end{tabular}
\end{center}
Le nom {\it carriage return} arrive tout droit de l'époque des machines à
écrire, où la position d'écriture était fixe et où le papier était
porté sur un chariot ({\it carriage}) mobile.
Le caractère {\it carriage return} est la commande permettant de mettre la
position d'écriture en début de ligne, sans changer de ligne.
Le caractère {\it line feed} met la position d'écriture sur la ligne suivante,
sans aller en début de ligne.
Pour obtenir l'effet de <<~passage à la ligne~>> , il faut donc un caractère
{\it carriage return} suivi d'un caractère {\it line feed} (ou l'inverse).
Dans le système {\sc unix}, le caractère choisi par convention comme signifiant
<<~passage à la ligne~>>  est le caractère {\it line feed}, et c'est à la charge
des pilotes de périphériques de remplacer ce caractère logique par la suite 
de caractères nécessaires pour obtenir un passage à la ligne suivante.
Prenons le cas d'un pilote de terminal écran clavier~:
\begin{itemize}
\item[\ptiret] en entrée~: la convention habituelle est de faire un passage à
la ligne an appuyant sur la touche {\it carriage return}.
Le pilote de terminal~:
   \begin{enumerate}
   \item
      envoie au programme qui réalise la lecture un {\it line feed}.
   \item
      envoie à l'écran (en tant qu'écho de ce {\it carriage return}) la
      séquence {\it line feed} suivi de {\it carriage return}.
   \end{enumerate}
\item[\ptiret]
   en sortie~: le pilote de terminal transforme les {\it line feed} en
   {\it line feed} suivi de {\it carriage return} 
\end{itemize}

Par abus de langage, dans le monde C et/ou {\sc unix}, on utilise souvent
le terme de {\it newline} pour désigner en réalité {\it line feed}.
Mais qu'il soit bien clair que la norme {\sc ansi} ne comporte pas de caractère
appelé {\it newline}.

\item[$\bullet$]  commandes d'extension du code         %   AUTRE FAMILLE
\begin{center}
\begin{tabular}{|l|c|} \hline
commande & nom \\ \hline
{\it shift out} & SO \\
{\it shift in} & SI \\
{\it escape} & ESC \\ \hline
\end{tabular}
\end{center}

Le caractère {\it escape} a été largement utilisé par les concepteurs de
terminaux écran-clavier et d'imprimantes pour augmenter le nombre
de commandes.
La technique consiste à définir des {\it séquences d'échappement}
formées du caractère {\it escape} suivi d'un certains nombre de caractères
ordinaires qui perdent leur signification habituelle.
Voici quelques {\it séquences d'échappement} du terminal écran-clavier 
{\sc vt}{\small 100}~:
\begin{center}
\begin{tabular}{|l|l|} \hline
séquence & sémantique \\ \hline
{\it escape} {\tt [2A} & monter le curseur de 2 lignes \\
{\it escape} {\tt [4B} & descendre le curseur de 4 lignes \\
{\it escape} {\tt [3C} & décaler le curseur de 3 positions vers la droite \\
{\it escape} {\tt [1D} & décaler le curseur de 1 position vers la gauche \\ \hline
\end{tabular}
\end{center}

\item[$\bullet$] commande de séparation                  %   AUTRE FAMILLE
\begin{center}
\begin{tabular}{|l|c|} \hline
commande & nom \\ \hline
{\it file separator} & FS \\
{\it group separator} & GS \\
{\it record separator} & RS \\
{\it unit separator} & US \\
{\it end of medium} & EM \\ \hline
\end{tabular}
\end{center}

Ces caractères ont pour but de séparer les différentes unités d'information
sur bandes magnétiques.
Ils sont obsolètes de nos jours, les programmes d'archivage ({\tt tar}, {\tt
cpio}) utilisant leur propre format sans faire appel à ces caractères.

\item[$\bullet$] commandes pour la communication synchrone    %   AUTRE FAMILLE
\begin{center}
\begin{tabular}{|l|c|} \hline
commande & nom \\ \hline
{\it start of header} & SOH \\
{\it start of text} & STX \\
{\it end of text} & ETX \\
{\it end of transmission} & EOT \\
{\it end of transmitted block} & ETB \\
{\it enquiry} & ENQ \\
{\it positive acknowledge} & ACK \\
{\it negative acknowledge} & NAK \\
{\it synchronisation} & SYN \\
{\it data link escape} & DLE \\
{\it null} & NUL \\ \hline
\end{tabular}
\end{center}

Les 10 premières commandes ont été crées pour construire des trames de
communication entre machines reliées par des lignes synchrones.
Elles sont complètement obsolètes de nos jours, où les communications se 
font grâce à des réseaux dont les trames n'utilisent pas ces caractères.

La dernière commande {\it null} était utile à l'époque des téléimprimeurs
dont le temps de retour du chariot était plus grand que le temps d'impression
d'un caractère quelconque.
Après avoir envoyé un {\it carriage return}, il fallait envoyer plusieurs
{\it null} (en fonction de la vitesse de la ligne) pour être sûr que le
chariot était bien revenu en début de ligne~!

\item[$\bullet$] commandes de périphérique                  %   AUTRE FAMILLE
\begin{center}
\begin{tabular}{|l|c|} \hline
commande & nom \\ \hline
{\it device control 1} & DC1 \\
{\it device control 2} & DC2 \\
{\it device control 3} & DC3 \\
{\it device control 4} & DC4 \\ \hline
\end{tabular}
\end{center}

Ces caractères ont été prévus pour donner des ordres spécifiques à certains
périphériques.
A l'époque des téléimprimeurs, ceux-ci possédaient un lecteur-perforateur
de ruban papier.
Les codes {\it device control} étaient utilisés pour commander ce
lecteur-perforateur.

De nos jours {\it device control 3} et {\it device control 1} sont utilisés
sous les noms respectifs de {\sc xon} et {\sc xoff} pour réaliser du
contrôle de flux.
Les caractères {\it device control 3} et {\it device control 1} sont affectés
aux touches {\it Control-q} et {\it Control-s} du clavier.
Lorsqu'un pilote de terminal écran-clavier gère le contrôle de flux,
l'utilisateur peut taper {\it Control-s} pour faire stopper une sortie trop
rapide (pour se donner le temps de la lire sur l'écran), et la faire
continuer en tapant {\it Control-q}.

\item[$\bullet$] commandes diverses                         %   AUTRE FAMILLE
\begin{center}
\begin{tabular}{|l|c|} \hline
commande & nom \\ \hline
{\it cancel} & CAN \\
{\it substitute} & SUB \\
{\it delete} & DEL \\
{\it bell} & BEL \\ \hline
\end{tabular}
\end{center}

Il y a deux caractères qui sont utilisés couramment pour réaliser la fonction
d'effacement du caractère (erroné) précédent~: {\it back space} et
{\it delete}.
En fonction du caractère qui est le plus facile à taper sur son clavier,
l'utilisateur désirera choisir l'un ou l'autre.
Le caractère {\it back space} peut sur tout clavier s'obtenir par {\it
Control-h}, alors qu'il n'y a pas de {\it Control-quelque-chose} correspondant 
au caractère {\it delete}.
Selon les claviers, il peut y avoir une touche marquée {\it back space}, et/ou
une touche marquée {\it delete}, ou une touche marquée $\leftarrow$ qui
génère {\it back space} ou {\it delete}, et qui peut, ou ne peut pas,
être configurée par le {\it set-up} du terminal pour générer au choix
{\it back space} ou {\it delete}~!

Un utilisateur {\sc unix} utilise la commande {\tt stty} pour indiquer au
système d'exploitation le caractère qu'il désire pour réaliser la fonction
d'effacement de caractère.


\end{itemize}   %   FIN DES FAMILLES 
\vfill

\subsection{Les codes {\tt ascii } en octal}
%-------------------------------------------
\begin{center}
\newcommand{\SP}{\hspace*{2mm}}
\setlength{\extrarowheight}{4pt}
\setlength{\tabcolsep}{4pt}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|} \hline
code & \SP 0 \SP & \SP 1 \SP & \SP 2 \SP & \SP 3 \SP & \SP 4 \SP & \SP 5 \SP &
\SP 6 \SP & \SP 7 \SP \\ \hline
000 & {\sc nul} & {\sc soh} & {\sc stx} & {\sc etx} &
       {\sc eot} & {\sc enq} & {\sc ack} & {\sc bel} \\ \hline
010 & {\sc bs} & {\sc ht} & {\sc lf} & {\sc vt} & 
           {\sc np} & {\sc cr} & {\sc so} & {\sc si} \\ \hline
020 & {\sc dle} & {\sc dc1} & {\sc dc2} & {\sc dc3} &
       {\sc dc4} & {\sc nak} & {\sc syn} & {\sc etb} \\ \hline
030 & {\sc can} & {\sc em} & {\sc sub} & {\sc esc} &
           {\sc fs} & {\sc gs} & {\sc rs} & {\sc us} \\ \hline
040 & {\sc sp} & \verb+!+ & \verb+"+ & \verb+#+ &
      \verb+$+ & \verb+%+ & \verb+&+ & \verb+'+ \\ \hline
050 & \verb+(+ & \verb+)+ & \verb+*+ & \verb=+= &
      \verb+,+ & \verb+-+ & \verb+.+ & \verb+/+ \\ \hline
060 & \verb+0+ & \verb+1+ & \verb+2+ & \verb+3+ &
      \verb+4+ & \verb+5+ & \verb+6+ & \verb+7+ \\ \hline
070 & \verb+8+ & \verb+9+ & \verb+:+ & \verb+;+ &
      \verb+<+ & \verb+=+ & \verb+>+ & \verb+?+ \\ \hline
100 & \verb+@+ & \verb+A+ & \verb+B+ & \verb+C+ &
      \verb+D+ & \verb+E+ & \verb+F+ & \verb+G+ \\ \hline
110 & \verb+H+ & \verb+I+ & \verb+J+ & \verb+K+ &
      \verb+L+ & \verb+M+ & \verb+N+ & \verb+O+ \\ \hline
120 & \verb+P+ & \verb+Q+ & \verb+R+ & \verb+S+ &
      \verb+T+ & \verb+U+ & \verb+V+ & \verb+W+ \\ \hline
130 & \verb+X+ & \verb+Y+ & \verb+Z+ & \verb+[+ &
      \verb+\+ & \verb+]+ & \verb+^+ & \verb+_+ \\ \hline
140 & \verb+`+ & \verb+a+ & \verb+b+ & \verb+c+ &
      \verb+d+ & \verb+e+ & \verb+f+ & \verb+g+ \\ \hline
150 & \verb+h+ & \verb+i+ & \verb+j+ & \verb+k+ &
      \verb+l+ & \verb+m+ & \verb+n+ & \verb+o+ \\ \hline
160 & \verb+p+ & \verb+q+ & \verb+r+ & \verb+s+ &
      \verb+t+ & \verb+u+ & \verb+v+ & \verb+w+ \\ \hline
170 & \verb+x+ & \verb+y+ & \verb+z+ & \verb+{+ &
      \verb+|+ & \verb+}+ & \verb+~+ & {\sc del} \\ \hline
\end{tabular}
\end{center}

\subsection{Les codes {\tt ascii} en hexadécimal}
%------------------------------------------------
\begin{center}
\setlength{\extrarowheight}{4pt}
\setlength{\tabcolsep}{4pt}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|} \hline
code & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & A & B & C & D & E & F \\ \hline
0x00 & {\sc nul} & {\sc soh} & {\sc stx} & {\sc etx} &
       {\sc eot} & {\sc enq} & {\sc ack} & {\sc bel} &
       {\sc bs} & {\sc ht} & {\sc lf} & {\sc vt} &
       {\sc np} & {\sc cr} & {\sc so} & {\sc si} \\ \hline
0x10 & {\sc dle} & {\sc dc1} & {\sc dc2} & {\sc dc3} &
       {\sc dc4} & {\sc nak} & {\sc syn} & {\sc etb} &
       {\sc can} & {\sc em} & {\sc sub} & {\sc esc} &
       {\sc fs} & {\sc gs} & {\sc rs} & {\sc us} \\ \hline
0x20 & {\sc sp} & \verb+!+ & \verb+"+ & \verb+#+ &
       \verb+$+ & \verb+%+ & \verb+ & + & \verb+'+ &
       \verb+(+ & \verb+)+ & \verb+*+ & \verb=+= &
       \verb+,+ & \verb+-+ & \verb+.+ & \verb+/+ \\ \hline
0x30 & \verb+0+ & \verb+1+ & \verb+2+ & \verb+3+ &
       \verb+4+ & \verb+5+ & \verb+6+ & \verb+7+ &
       \verb+8+ & \verb+9+ & \verb+:+ & \verb+;+ &
       \verb+<+ & \verb+=+ & \verb+>+ & \verb+?+ \\ \hline
0x40 & \verb+@+ & \verb+A+ & \verb+B+ & \verb+C+ &
       \verb+D+ & \verb+E+ & \verb+F+ & \verb+G+ &
       \verb+H+ & \verb+I+ & \verb+J+ & \verb+K+ &
       \verb+L+ & \verb+M+ & \verb+N+ & \verb+O+ \\ \hline
0x50 & \verb+P+ & \verb+Q+ & \verb+R+ & \verb+S+ &
       \verb+T+ & \verb+U+ & \verb+V+ & \verb+W+ &
       \verb+X+ & \verb+Y+ & \verb+Z+ & \verb+[+ &
       \verb+\+ & \verb+]+ & \verb+^+ & \verb+_+ \\ \hline
0x60 & \verb+`+ & \verb+a+ & \verb+b+ & \verb+c+ &
       \verb+d+ & \verb+e+ & \verb+f+ & \verb+g+ &
       \verb+h+ & \verb+i+ & \verb+j+ & \verb+k+ &
       \verb+l+ & \verb+m+ & \verb+n+ & \verb+o+ \\ \hline
0x70 & \verb+p+ & \verb+q+ & \verb+r+ & \verb+s+ &
       \verb+t+ & \verb+u+ & \verb+v+ & \verb+w+ &
       \verb+x+ & \verb+y+ & \verb+z+ & \verb+{+ &
       \verb+|+ & \verb+}+ & \verb+~+ & {\sc del} \\ \hline
\end{tabular}
\end{center}

\subsection{Les codes {\tt ascii} en décimal}
%--------------------------------------------
\vspace*{1cm}
\begin{center}
\setlength{\extrarowheight}{4pt}
\setlength{\tabcolsep}{4pt}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|} \hline
code & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\ \hline
0 & {\sc nul} & {\sc soh} & {\sc stx} & {\sc etx} & {\sc eot} &
       {\sc enq} & {\sc ack} & {\sc bel} & {\sc bs} & {\sc ht} \\ \hline 
10 & {\sc lf} & {\sc vt} & {\sc np} & {\sc cr} & {\sc so} &
       {\sc si} & {\sc dle} & {\sc dc1} & {\sc dc2} & {\sc dc3} \\ \hline
20 & {\sc dc4} & {\sc nak} & {\sc syn} & {\sc etb} & {\sc can} &
       {\sc em} & {\sc sub} & {\sc esc} & {\sc fs} & {\sc gs} \\ \hline
30 & {\sc rs} & {\sc us} & {\sc sp} & \verb+!+ & \verb+"+ & 
       \verb+#+ & \verb+$+ & \verb+%+ & \verb+ & + & \verb+'+ \\ \hline
40 & \verb+(+ & \verb+)+ & \verb+*+ & \verb=+= & \verb+,+ &
       \verb+-+ & \verb+.+ & \verb+/+ & \verb+0+ & \verb+1+ \\ \hline
50 & \verb+2+ & \verb+3+ & \verb+4+ & \verb+5+ & \verb+6+ &
       \verb+7+ & \verb+8+ & \verb+9+ & \verb+:+ & \verb+;+ \\ \hline
60 & \verb+<+ & \verb+=+ & \verb+>+ & \verb+?+ & \verb+@+ &
       \verb+A+ & \verb+B+ & \verb+C+ & \verb+D+ & \verb+E+ \\ \hline
70 & \verb+F+ & \verb+G+ & \verb+H+ & \verb+I+ & \verb+J+ &
       \verb+K+ & \verb+L+ & \verb+M+ & \verb+N+ & \verb+O+ \\ \hline
80 & \verb+P+ & \verb+Q+ & \verb+R+ & \verb+S+ & \verb+T+ &
       \verb+U+ & \verb+V+ & \verb+W+ & \verb+X+ & \verb+Y+ \\ \hline
90 & \verb+Z+ & \verb+[+ & \verb+\+ & \verb+]+ & \verb+^+ & 
       \verb+_+ & \verb+`+ & \verb+a+ & \verb+b+ & \verb+c+ \\ \hline
100 & \verb+d+ & \verb+e+ & \verb+f+ & \verb+g+ & \verb+h+ &
       \verb+i+ & \verb+j+ & \verb+k+ & \verb+l+ & \verb+m+ \\ \hline
110 & \verb+n+ & \verb+o+ & \verb+p+ & \verb+q+ & \verb+r+ &
       \verb+s+ & \verb+t+ & \verb+u+ & \verb+v+ & \verb+w+ \\ \hline
120 & \verb+x+ & \verb+y+ & \verb+z+ & \verb+{+ & \verb+|+ &
       \verb+}+ & \verb+~+ & {\sc del} & & \\ \hline
\end{tabular}
\end{center}
\vfill
