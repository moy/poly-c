%-------------------------------------------------------------------------------
%   verso de la page de titre
%-------------------------------------------------------------------------------
\newpage
\thispagestyle{empty}   %   pour que le numero de page ne soit pas imprime
\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\vspace*{3cm}}
   {\vspace*{10cm}}
\end{latexonly}
%
Copyright 1997, 1998, 2003 Bernard Cassagne\\
Copyright 2012 Matthieu Moy\\
Ce texte est copyrighté et n'est pas dans le domaine public.
Sa reproduction est cependant autorisée à condition de respecter les
conditions suivantes~:
\begin{itemize}
\item Si ce document est reproduit pour les besoins personnels du reproducteur,
toute forme de reproduction (totale ou partielle) est autorisée.
\item Si ce document est reproduit dans le but d'être distribué à de tierces
personnes~:
   \begin{itemize}
   \item il devra être reproduit dans son intégralité sans aucune
   modification. Cette notice de copyright devra donc être présente.

   \item il ne devra pas être vendu.
   Cependant, dans le seul cas d'un enseignement gratuit, une participation
   aux frais de reproduction pourra être demandée, mais elle ne pourra être
   supérieure au prix du papier et de l'encre composant le document.
   \end{itemize}

\end{itemize}
Toute reproduction sortant du cadre précisé ci-dessus est interdite sans
accord préalable de l'auteur.

Une page permettant d'obtenir ce document sous divers formats est accessible
par l'{\sc url}~:\\
\url{http://www-verimag.imag.fr/~moy/cours/poly-c/}
\vspace*{1cm}
\\
Version de ce document~: 2.3 de juin 2012

%   On ne met pas les explications pour le pdf dans le html (of course!)
\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {
   %----------------------------------------------------------------------------
   %   page d'explication pour la forme pdf
   %----------------------------------------------------------------------------
   \newpage
   \vspace*{5cm}
   Quelques indications pour naviguer facilement dans ce document~:
   \begin{itemize}
   \item les ``Bookmarks'' de Acrobat Reader constituent une table des
      matières résumée permettant un accés direct à chaque chapitre ;
   \item une table des matières complète est accessible via un bookmark,
      mais également par un lien cliquable en bas de chaque page ;
   \item l'index est lui aussi accessible par un lien cliquable en bas de
      chaque page.
   \item chaque titre de chapitre ou section de la table des matières 
      est un lien cliquable ;
   \item dans l'index, chaque numéro de page est un lien cliquable.
   \end{itemize}
   Bonne navigation !
   }
   {}
\end{latexonly}

%   Debut des numeros de page en romain
%   -----------------------------------
\pagestyle{plain}
\pagenumbering{roman}

%-------------------------------------------------------------------------------
%   La table des matieres
%-------------------------------------------------------------------------------
% \nofrenchguillemets
%   Je ne sais pas pourquoi, mais dans la version pdf, la table des
%   matieres ne debute pas automatiquement sur une nouvelle page.
\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}
\tableofcontents
% \frenchguillemets


\chapter*{Avant-propos}
%**********************

%   Debut des numeros de page en normal
%   -----------------------------------
\pagenumbering{arabic}

\section*{Au sujet de l'auteur}
%==============================
Bernard Cassagne, l'auteur original de ce document, est ingénieur
retraité du {\sc cnrs} et travaillait dans un laboratoire de
recherche de l'université de Grenoble~: le laboratoire LIG,
anciennement {\sc clips}.
Le document est maintenant maintenu par Matthieu Moy,
enseignant-chercheur à l'Ensimag et au laboratoire Verimag.
Toute notification d'erreur ou toute proposition d'amélioration de ce 
document sera la bienvenue à l'e-mail {\tt Matthieu.Moy@imag.fr}.

\section*{Au sujet de ce manuel}
%===============================
La littérature technique nous a habitué à deux styles d'écriture de manuels~:
le style <<~manuel de référence~>> et le style <<~guide de l'utilisateur~>>
Les manuels de référence se donnent comme buts d'être exhaustifs
et rigoureux.
Les guides de l'utilisateur se donnent comme but d'être didactiques.
Cette partition vient du fait qu'il est quasiment impossible sur des sujets
complexes comme les langages de programmation d'être à la fois rigoureux et
didactique.
Pour s'en persuader il suffit de lire le texte d'une norme internationale.

Ce manuel se place dans la catégorie <<~guide de l'utilisateur~>>~: son but est
de permettre à une personne sachant programmer,
d'acquérir les éléments fondamentaux du langage C.
Ce manuel présente donc chaque notion selon une gradation des difficultés
et ne cherche pas à être exhaustif.
Il comporte de nombreux exemples, ainsi que des exercices dont la solution
se trouve dans le corps du texte, mais commence toujours sur une page
différente.
Le lecteur peut donc au choix, ne lire les solutions qu'après avoir
programmé sa solution personnelle, ou bien lire directement la solution
comme si elle faisait partie du manuel.

\section*{Les notions supposées connues du lecteur}
%==================================================
Les notions supposées connues du lecteur sont les concepts généraux concernant
les langages de programmation.
En fin du texte se trouve un glossaire qui regroupe des concepts généraux
et certains concepts propres du langage C.
En cas de rencontre d'un mot inconnu, le lecteur est invité à s'y reporter.

\section*{Un mot sur les problèmes de traduction}
%================================================
Le document de référence concernant le langage C est la norme {\sc ansi}
définissant le langage.
C'est un document écrit en anglais technique, ce qui pose des problèmes 
de traduction~: comment traduire les néologismes inventés pour les besoins
du langage~?
De manière à ne pas dérouter les lecteurs français, je me suis imposé de
respecter les choix de traduction d'un grand éditeur
\footnote{C'est {\sc masson} qui a édité en français \symbol{'133}1\symbol{'135}
et \symbol{'133}2\symbol{'135} (Cf. Bibliographie)}
de livres techniques,
même quand je n'ai pas trouvé ces choix très heureux.
J'ai donc utilisé {\it déclarateur}, {\it initialisateur} et
{\it spécificateur} bien que me semble-t-il, ils râpent assez fort le
palais quand on les prononce.

\section*{Conventions syntaxiques}
%=================================
Les règles de grammaires qui sont données dans le corps de ce manuel sont
simplifiées (dans un but didactique) par rapport à la grammaire officielle
du langage.
Cependant, le lecteur trouvera à l'annexe D la grammaire sous une forme
exhaustive et conforme à la norme {\sc ansi}.
La typographie des règles suit les conventions suivantes~:
\begin{enumerate}
\item les éléments terminaux du langage seront écrits dans une fonte
à largeur constante, comme ceci~: {\tt while}.

\item les éléments non terminaux du langage seront écrits en italique, comme
ceci~:\\
{\it instruction}.

\item les règles de grammaires seront écrites de la manière suivante~:
\begin{itemize}
\item[$\bullet$] les parties gauches de règles seront seules sur leur ligne,
cadrées à gauche et suivies du signe deux points (:).
\item[$\bullet$] les différentes parties droites possibles seront introduites par le
signe $\Rightarrow$ et indentées sur la droite.

Exemple~:\\
\\
{\it instruction} :\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt if} \hspace{3mm}{\tt (} \hspace{3mm}{\it expression} \hspace{3mm}{\tt )} \hspace{3mm}{\it instruction\/$_1$}\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt if} \hspace{3mm}{\tt (} \hspace{3mm}{\it expression} \hspace{3mm}{\tt )} \hspace{3mm}{\it instruction\/$_1$} \hspace{3mm}{\tt else} \hspace{3mm}{\it instruction\/$_2$}\\
\\
Ceci signifie qu'il y a deux manières possibles de dériver le non-terminal
{\it instruction}.
La première règle indique qu'on peut le dériver en~:\\
{\tt if} \hspace{3mm}{\tt (} \hspace{3mm}{\it expression} \hspace{3mm}{\tt )} \hspace{3mm}{\it instruction\/$_1$}\\
la deuxième règle indique qu'on peut aussi le dériver en~:\\
{\tt if} \hspace{3mm}{\tt (} \hspace{3mm}{\it expression} \hspace{3mm}{\tt )} \hspace{3mm}{\it instruction\/$_1$} \hspace{3mm}{\tt else} \hspace{3mm}{\it instruction\/$_2$}

\item[$\bullet$] une partie droite de règle pourra être écrite sur plusieurs lignes.
Ceci permettra de réfléter une manière possible de mettre en page le
fragment de programme C correspondant, de façon à obtenir une bonne
lisibilité.

Sans en changer la signification, l'exemple précédent aurait pu être
écrit~:\\
\\
{\it instruction} :\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt if} \hspace{3mm}{\tt (} \hspace{3mm}{\it expression} \hspace{3mm}{\tt )}\\
\hspace*{23mm}{\it instruction\/$_1$}\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt if} \hspace{3mm}{\tt (} \hspace{3mm}{\it expression} \hspace{3mm}{\tt )}\\
\hspace*{23mm}{\it instruction\/$_1$}\\
\hspace*{18mm}{\tt else} \hspace{3mm}{\it instruction\/$_2$}\\
\\

\item[$\bullet$] les éléments optionnels d'une règle seront indiqués en mettant le
mot <<~option~>> (en italique et dans une fonte plus petite) à droite de
l'élément concerné.

Par exemple, la règle~:\\
\\
{\it déclarateur-init} :\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\it déclarateur}  \hspace{3mm}{\it initialisateur\/$_{option}$}\\
\\
indique que {\it déclarateur-init} peut se dériver soit en~:\\
\\
{\it déclarateur}  \hspace{3mm}{\it initialisateur}\\
\\
soit en~:\\
\\
{\it déclarateur}

\end{itemize}
\end{enumerate}

\section*{Remerciements}
%=======================
Beaucoup de personnes m'ont aidé à améliorer le manuscrit original en me
signalant de nombreuses erreurs et en me proposant des améliorations.
Qu'elles soient toutes remerciées de leurs lectures attentives et
amicalement critiques, tout particulièrement Michel Burlet, Damien Genthial,
Fabienne Lagnier, Xavier Nicollin et Serge Rouveyrol.
%-------------------------------------------------------------------------------
%
%   Tous les see de l'index
%   -----------------------
%
%   On ne peut pas les mettre dans un module a part, car LaTeX ne genere
%   pas de fichier .idx si il n'y a pas de texte genere avec
%
%   Autre emmerdement : derriere un see, on ne peut pas mettre de |
%
\index{operateur@{opérateur}!modulo|see{opérateur \%}}
\index{operateur@{opérateur}!incrementation@{incrémentation}|see{opérateur {\tt ++}}}
\index{operateur@{opérateur}!decrementation@{décrémentation}|see{opérateur {\tt --}}}
\index{operateur@{opérateur}!indirection|see{opérateur {\tt *}}}

%   Attention, il faut escaper & avec \
\index{operateur@{opérateur}!et logique|see{opérateur {\tt \&\&}}}
\index{operateur@{opérateur}!adresse de|see{opérateur {\tt \&}}}

%   | a une signification pour makeindex, on l'escape avec \textbar
\index{operateur@{opérateur}!ou logique|see{opérateur {\tt \textbar\textbar}}}

\index{affectation|see{opérateur {\tt =}}}
\index{operateur@{opérateur}!affectation|see{opérateur {\tt =}}}
\index{tableau!initialisation|see{initialisation de tableau}}
\index{parametre@{paramètre}!passage de tableau en|see{tableau, passage en
paramètre}}
\index{parametre@{paramètre}!passage de structure en|see{structure, passage en
paramètre}}
\index{structure!declaration@{déclaration}|see {déclaration, de structure}}
\index{structure!initialisation|see {initialisation, de structure}}
\index{structure!operateurs sur@{opérateurs sur}|see {opérateurs, sur les structures}}
\index{structure!affectation|see {affectation, de structure}}
\index{recursivite@{récursivité}!de structure|see {structure, récursive}}
\index{recursivite@{récursivité}!de fonction|see {fonction, récursive}}
\index{cast|see {opérateur, conversion}}
\index{pointeur!invalide|see{{\tt NULL}}}
\index{fonction!prototype de |see {prototype, de fonction}}
\index{sizeof |see {operateur, {\tt sizeof}}}
\index{constante!chaine de caracteres@{chaîne de caractères}|see {chaîne littérale}}
\index{procedure@{procédure}|see {fonction}}
\index{entier|see {type, entier}}
\index{flottant|see {type, flottant}}
%
%   les mots cles qui sont des instructions
%
\index{break@{\tt break}|see {instruction, {\tt break}}}
\index{continue@{\tt continue}|see {instruction, {\tt continue}}}
\index{do@{\tt do}|see {instruction, {\tt do}}}
\index{for@{\tt for}|see {instruction, {\tt for}}}
\index{goto@{\tt goto}|see {instruction, {\tt goto}}}
\index{if@{\tt if}|see {instruction, {\tt if}}}
\index{return@{\tt return}|see {instruction, {\tt return}}}
\index{switch@{\tt switch}|see {instruction, {\tt switch}}}
\index{while@{\tt while}|see {instruction, {\tt while}}}
