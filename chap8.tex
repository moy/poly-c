\cleardoublepage
\chapter{Le préprocesseur}
%*************************
Les services rendus par le préprocesseur sont~: l'inclusion de fichier source,
le traitement de macros et la compilation conditionnelle.
L'inclusion de fichier source a déjà été vue dans le chapitre
\ref{inclusion-de-source}, nous n'y reviendrons pas.

\section{Traitement de macros}
%=============================
\index{macro}
\index{macro!expansion de}

Il existe deux types de macros~: les macros sans paramètre
et les macros avec paramètres.

\subsection{Les macros sans paramètres}
%--------------------------------------
\index{macro!sans parametre@{sans paramètre}}
Les macros sans paramètre ont été introduites au paragraphe \ref{define}.
Rappelons que lorsque le préprocesseur lit une ligne du type~:\\
\\
\verb+#define+ \hspace{3mm}{\it nom}
\hspace{3mm}{\it reste-de-la-ligne}\\
\\
il remplace dans toute la suite du source, toute nouvelle occurrence de
{\it nom} par {\it reste-de-la-ligne}.
Il n'y a aucune contrainte quand à ce qui peut se trouver dans
{\it reste-de-la-ligne}, mais l'utilité principale des macros sans paramètre
est de donner un nom parlant à une constante.
Les avantages à toujours donner un nom aux constantes 
sont les suivants~:

\begin{enumerate}
\item un nom bien choisi permet d'expliciter la sémantique de la constante.
Exemple~: \verb|#define NB_COLONNES 100|.
\item la constante chiffrée se trouve à un seul endroit, ce qui
facilite la modification du programme quand on veut changer la valeur de la
constante (cas de la taille d'un tableau, par exemple).
\item on peut expliciter les relations entre constantes. Exemple~:\\
\verb|#define NB_LIGNES 24|\\
\verb|#define NB_COLONNES 80|\\
\verb|#define TAILLE_TAB     NB_LIGNES * NB_COLONNES|
\end{enumerate}

\subsubsection{Exemple de mauvaise utilisation}
%..............................................
\index{shell@{\it shell}}
Du fait de l'absence de contrainte sur {\it reste-de-la-ligne}, on peut
faire des choses très déraisonnables avec les macros.
Un exemple célèbre est le source du shell écrit par Steve Bourne pour le
système {\sc unix}. Bourne avait utilisé les facilités de macros pour
programmer dans un dialecte de Algol 68.
Voici un extrait de ses définitions~:
\begin{verbatim}
#define IF      if(
#define THEN    ){
#define ELSE    } else {
#define ELIF    } else if (
#define FI      ;}

#define BEGIN   {
#define END     }
#define SWITCH  switch(
#define IN      ){
#define ENDSW   }
#define FOR     for(
#define WHILE   while(
#define DO      ){
#define OD      ;}
#define REP     do{
#define PER     }while(
#undef DONE
#define DONE    );
#define LOOP    for(;;){
#define POOL    }
\end{verbatim}
Et voici un exemple de code~:
\begin{verbatim}
assign(n,v)
        NAMPTR          n;
        STRING          v;
{
        IF n->namflg&N_RDONLY
        THEN    failed(n->namid,wtfailed);
        ELSE    replace(&n->namval,v);
        FI
}
\end{verbatim}
Ce n'est ni du C ni de l'Algol, il y a un consensus dans la communauté C
pour estimer que ce genre de choses est à proscrire.

\subsubsection{Définition de macro à l'invocation du compilateur}
%................................................................
\index{compilateur!mise en oeuvre du}
Certains compilateurs permettent de définir des macros sans
paramètres à l'invocation du compilateur.
Il est alors possible d'écrire un programme utilisant une macro qui n'est 
nulle part définie dans le source.
La définition se fera à l'invocation du compilateur.
Ceci est très pratique pour que certaines constantes critiques d'un
programme aient une valeur qui soit attribuée à l'extérieur du programme,
par une phase de configuration par exemple.

Ci-dessous, un exemple pour le système {\sc unix}~:
la compilation du fichier {\tt fic.c} en
définissant la macro sans paramètre de nom {\tt NB\_LIGNES} et de valeur 24~:
\begin{verbatim}
cc -c -DNB_LIGNES=24 fic.c
\end{verbatim}

\subsection{Macros prédéfinies}
%..............................
\index{macro!predefinie@{prédéfinie}}
\index{{\tt \_\_LINE\_\_}}
\index{{\tt \_\_FILE\_\_}}
\index{{\tt \_\_DATE\_\_}}
\index{{\tt \_\_TIME\_\_}}
\index{{\tt \_\_STDC\_\_}}

Il y a un certain nombre de macros prédéfinies par le préprocesseur~:
\begin{center}
\begin{tabular}{|l|l|c|} \hline
nom  & valeur de la macro & forme  \\
     &                    & syntaxique\\ \hline
\verb|__LINE__| & numéro de la ligne courante du programme source & entier\\
\verb|__FILE__| & nom du fichier source en cours de compilation& chaîne \\
\verb|__DATE__| & la date de la compilation & chaîne\\
\verb|__TIME__| & l'heure de la compilation & chaîne\\
\verb|__STDC__| & 1 si le compilateur est {\sc iso}, 0 sinon& entier \\ \hline
\end{tabular}
\end{center}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Les macros avec paramètres}
%--------------------------------------
\index{macro!avec parametres@{avec paramètres}}
\index{macro!corps de}
\index{macro!expansion de}
Une macro avec paramètres se définit de la manière suivante~:
\\
\verb+#define+ \hspace{3mm}{\it nom}
\hspace{3mm}{\tt (}
\hspace{3mm}{\it liste-de-paramètres-formels}
\hspace{3mm}{\tt )}
\hspace{3mm}{\it reste-de-la-ligne}\\
\\
La {\it liste-de-paramètres-formels} est une liste d'identificateurs séparés
par des virgules.
Le  {\it reste-de-la-ligne} est appelé <<~corps de la macro~>>.
Toute occurrence ultérieure de {\it nom} sera un appel de la macro et devra
avoir la forme~:\\
{\it nom}\hspace{3mm}{\tt (}
\hspace{3mm}{\it liste-de-paramètres-effectifs}
\hspace{3mm}{\tt )}
\\
Dans la {\it liste-de-paramètres-effectifs}, les paramètres sont séparés par
des virgules et chaque paramètre est une suite quelconque d'unités lexicales.
Le préprocesseur remplace l'ensemble nom de la macro et liste de
paramètres effectifs parenthésés, par {\it reste-de-la-ligne} dans lequel
chaque paramètre formel est remplacé par le paramètre effectif correspondant.
Cette opération de remplacement de texte porte le nom d'{\it expansion}
de la macro.

L'utilité principale des macros avec paramètres est de bénéficier de la clarté
d'expression des fonctions sans en souffrir la lourdeur~: le code est inséré
en ligne, donc on économise le code d'entrée et de retour de fonction.
Exemple~:
\\
\begin{verbatim}
#define min(a, b)       ((a) < (b) ? (a) : (b))
#define max(a, b)       ((a) < (b) ? (b) : (a))

f()
{
int i,j,k;

i = min(j,k); /*  équivalent à : i =  j  <  k  ?  j  :  k ;              */
i = max(j,k); /*  équivalent à : i =  j  <  k  ?  k  :  j ;              */
}
\end{verbatim}

\subsubsection{Attention}
%........................
La distinction entre macro avec et sans paramètre se fait sur le caractère
qui suit immédiatement le {\it nom} de la macro~: si ce caractère est une
parenthèse ouvrante c'est une macro avec paramètres, sinon c'est une macro
sans paramètre.
En particulier, si après le {\it nom} de la macro il y a un blanc
avant la parenthèse ouvrante, ça sera une macro sans paramètre.
Exemple~:
\begin{verbatim}
#define CARRE (a) a * a
\end{verbatim}
Une utilisation de \verb|CARRE(2)| aura comme expansion \verb|(a) a * a(2)| !
Attention donc à l'erreur difficile à voir~: la présence d'un blanc entre
le nom d'une macro avec paramètres et la parenthèse ouvrante.

\subsubsection{Exemple}
%......................
\index{linux@{\sc linux}}
Cet exemple est tiré du source de {\sc Linux}.
Il s'agit d'un fragment de gestion de la mémoire virtuelle,
une structure page a été définie~:
\begin{verbatim}
struct page {
   struct inode *inode;
   unsigned long offset;
   struct page *next_hash;
   atomic_t count;
   unsigned flags; /* atomic flags, 
                      some possibly updated asynchronously */
   ...    /*   d'autres champs   */
   };
\end{verbatim}
Dans cette structure, le champs flags est un ensemble de bits définis
ci-après~:
\begin{verbatim}
/* Page flag bit values */
#define PG_locked                0
#define PG_error                 1
#define PG_referenced            2
#define PG_uptodate              3
#define PG_free_after            4
#define PG_decr_after            5
\end{verbatim}
Puis le programmeur a défini des macros pour tester commodément ces bits
à l'aide de la fonction {\tt test\_bit} définie par ailleurs~:
\begin{verbatim}
/* Make it prettier to test the above... */
#define PageLocked(page) \
        (test_bit(PG_locked, &(page)->flags))
#define PageError(page) \
        (test_bit(PG_error, &(page)->flags))
#define PageReferenced(page) \
        (test_bit(PG_referenced, &(page)->flags))
#define PageUptodate(page) \
        (test_bit(PG_uptodate, &(page)->flags))
#define PageFreeAfter(page) \
        (test_bit(PG_free_after, &(page)->flags))
#define PageDecrAfter(page) \
        (test_bit(PG_decr_after, &(page)->flags))
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsubsection{Exemple de mauvaise utilisation}
%..............................................
Dans une invocation de macro, chaque paramètre effectif peut
être une suite quelconque d'unités lexicales, mais après expansion,
le texte obtenu doit être un fragment valide de langage C.
Voici un exemple des horreurs que l'on peut écrire en utilisant les macros~:
\begin{verbatim}
#define macro(a,b) a [ b

f()
{
int i, t[10];

macro(t,i]) = 1;   /*   équivalent à t[i] = 1;   */
}
\end{verbatim}
%   [ pour donner a matcher au bracket fermant
Le second paramètre passé à la macro ({\tt i]}) ne correspond syntaxiquement à
rien, mais le résultat de l'expansion de la macro est correct.

\subsection{Les pièges des macros}
%---------------------------------
Par le fait que le traitement des macros consiste à faire de la substitution
de texte, l'écriture de macros recèle de nombreux pièges.

\subsubsection{Pièges des priorités d'opérateurs}
%................................................
Supposons que l'on écrive~:
\begin{verbatim}
#define CARRE(a) a * a
\end{verbatim}
une occurrence de \verb|CARRE(a+b)| aura comme expansion \verb|a+b * a+b|
ce qui est différent du \verb|(a+b) * (a+b)| qui était désiré.
De la même manière \verb|!CARRE(x)| aura comme expansion \verb|!x * x|
ce qui est différent du \verb|!(x * x)| qui était désiré.

On recommande donc de toujours respecter deux règles dans la définition
d'une macro devant être utilisée dans des expressions~:
\begin{enumerate}
\item parenthéser les occurrences des paramètres formels~;
\item parenthéser le corps complet de la macro.
\end{enumerate}
Une définition de {\tt CARRE} respectant ces règles est~:
\begin{verbatim}
#define CARRE(a) ((a) * (a))
\end{verbatim}

\subsubsection{Pièges des effets de bord}
%........................................
L'utilisation d'effet de bord sur les paramètres effectifs d'une macro peut
avoir des effets complètement inattendus.
Après la définition~:
\begin{verbatim}
#define CARRE(a) ((a) * (a))
\end{verbatim}
l'utilisation de \verb|CARRE(x++)| aura comme expansion \verb|((x++) * (x++))|,
l'opérateur {\tt ++} sera donc appliqué deux fois.

\subsection{Macros générant des instructions}
%---------------------------------------------
Tous les exemples donnés jusqu'ici sont des exemples de macros générant
des expressions.
Les macros peuvent aussi générer des instructions et là aussi il
y a des pièges à éviter.
Supposons qu'ayant à écrire un grand nombre de fois un appel de fonction
avec test d'erreur, on définisse la macro suivante~:
\begin{verbatim}
#define F(x) if (!f(x)) { printf("erreur\n"); exit(1); }
\end{verbatim}
La macro pourra s'appeler comme une fonction (avec un {\tt ;} à la fin)
dans un contexte de liste d'instructions~:
\begin{verbatim}
{
...
F(i);
...
}
\end{verbatim}
Par contre, dans un contexte d'instruction (et non de liste d'instructions),
il ne faudra pas mettre de {\tt ;} à la fin~:
\begin{verbatim}
do  F(a)  while ( ... );
\end{verbatim}
alors qu'il le faudrait si il s'agissait d'une fonction~:
\begin{verbatim}
do  f(a);  while ( ... );
\end{verbatim}
Mais le pire reste à venir~:
voyons ce qui se passe si on utilise la macro {\tt F}
dans un {\tt if} avec {\tt else}~:
\begin{verbatim}
if ( ... )
   F(i)
else
   ...
\end{verbatim}
Il suffit d'imaginer l'expansion de la macro~:
\begin{verbatim}
if ( ... )
   if (!f(x)) { printf("erreur\n"); exit(1); }
else
   ...
\end{verbatim}
pour comprendre le problème~: le {\tt else} va être raccroché au {\tt if}
de la macro, ce qui n'est pas ce qu'a voulu le programmeur.

\subsubsection{Recommandation}
%.............................
\index{instruction!do@{\tt do}}
Pour toute macro générant des instructions, on recommande d'englober
les instructions générées dans la partie instruction d'un
{\tt do ... while (0)}.
Notre exemple s'écrit ainsi~:
\begin{verbatim}
#define F(x) do { \
        if (!f(x)) { printf("erreur\n"); exit(1); } \
} while (0)
\end{verbatim}
et tous les problèmes précédents s'évanouissent.

\section{Compilation conditionnelle}
%===================================
\index{commande!du preprocesseur@{du préprocesseur}}
\index{compilation conditionnelle}
Les mécanismes de compilation conditionnelles ont pour but de compiler
ou d'ignorer des ensembles de lignes, le choix étant basé sur un test
exécuté à la compilation.

\subsection{Commande {\tt \#if}}
%-------------------------------
\index{\#if@{\tt \#if}}
La commande permettant de réaliser la compilation conditionnelle est la 
commande {\tt \#if} qui peut prendre plusieurs formes.

\subsubsection{Commande {\tt \#if} simple}
%.........................................
Quand le préprocesseur rencontre~:
\\
\verb|#if|
\hspace{3mm}{\it expression}\\
{\it ensemble-de-lignes}\\
\verb|#endif|
\\
il évalue {\it expression}.
Si {\it expression} délivre une valeur non nulle, {\it ensemble-de-lignes}
est compilé, sinon {\it ensemble-de-lignes} est ignoré.
L'évaluation de {\it expression} a lieu au moment de la compilation,
elle ne doit donc comporter que des constantes.
L'{\it ensemble-de-lignes} est une suite de lignes quelconques.

\subsubsection{Commande {\tt \#if} avec {\tt \#else}}
%....................................................
\index{\#else@{\tt \#else}}
Sur rencontre de~:
\\
\verb|#if|
\hspace{3mm}{\it expression}\\
{\it ensemble-de-lignes\/$_1$}\\
\verb|#else|\\
{\it ensemble-de-lignes\/$_2$}\\
\verb|#endif|
\\
le préprocesseur évalue {\it expression}.
Si {\it expression} délivre une valeur non nulle,
{\it ensemble-de-lignes\/$_1$} est compilé
et {\it ensemble-de-lignes\/$_2$} est ignoré, sinon
{\it ensemble-de-lignes\/$_1$} est ignoré
et {\it ensemble-de-lignes\/$_2$} est compilé.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsubsection{Commande {\tt \#if} avec {\tt \#elif}}
%....................................................
\index{\#elif@{\tt \#elif}}
De manière à imbriquer aisément des \verb|#if| dans des \verb|#if|, il existe
une commande \verb|#elif| dont la sémantique est else if.
Elle s'utilise de la manière suivante~:
\\
\verb|#if|\hspace{3mm}{\it expression\/$_1$}\\
{\it ensemble-de-lignes\/$_1$}\\
\verb|#elif|\hspace{3mm}{\it expression\/$_2$}\\
{\it ensemble-de-lignes\/$_2$}\\
...\\
\verb|#elif|\hspace{3mm}{\it expression\/$_n$}\\
{\it ensemble-de-lignes\/$_n$}\\
\verb|#else|\\
{\it ensemble-de-lignes\/$_{else}$}\\
\verb|#endif|
\\
Un seul {\it ensemble-de-lignes} sera compilé~: celui correspondant à la
première {\it expression\/$_i$} qui s'évaluera à une valeur non nulle si
elle existe, ou bien {\it ensemble-de-lignes\/$_{else}$}
si toutes les {\it expression\/$_i$} s'évaluent à 0.

\subsection{Commandes {\tt \#ifdef} et {\tt \#ifndef}}
%-----------------------------------------------------
\index{\#ifdef@{\tt \#ifdef}}
\index{\#ifndef@{\tt \#ifndef}}
Dans ce qui précède il est possible de remplacer les commandes \verb|#if|
\hspace{3mm}{\it expression} par~:
\verb|#ifdef|\hspace{3mm}{\it nom} ou\\
\verb|#ifndef|\hspace{3mm}{\it nom}.\\
Dans ce cas, le test ne porte plus sur la nullité ou non d'une expression, mais
sur la définition ou non d'une macro.
La commande \verb|#ifdef|\hspace{3mm}{\it nom} a pour sémantique~:
<<~si {\it nom} est défini~>>, et \verb|#ifndef|\hspace{3mm}{\it nom}
a pour sémantique~: <<~si {\it nom} n'est pas défini~>>.

\subsection{L'opérateur {\tt defined}}
%-------------------------------------
\index{defined@{\tt defined}}
L'opérateur {\tt defined} est un opérateur spécial~: il ne peut être
utilisé que dans le contexte d'une commande \verb|#if| ou \verb|#elif|.
Il peut être utilisé sous l'une des deux formes suivantes~: {\tt defined}
{\it nom} ou bien~: {\tt defined (} {\it nom} {\tt )}.
Il délivre la valeur 1 si {\it nom} est une macro définie, et la valeur 0 
sinon.
L'intérêt de cet opérateur est de permettre d'écrire des tests portant
sur la définition de plusieurs macros, alors que {\tt \#ifdef} ne peut en
tester qu'une.
\begin{verbatim}
#if defined(SOLARIS) || defined(SYSV)
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{La commande {\tt \#error}}
%-------------------------------------
\index{\#error@{\tt \#error}}
La commande {\tt \#error} a la syntaxe suivante~:
\verb|#error|\hspace{3mm}{\it suite-d-unités-lexicales}\\
La rencontre de cette commande provoquera l'émission d'un message d'erreur
comprenant la {\it suite-d-unités-lexicales}.
Cette commande a pour utilité de capturer à la compilation des conditions
qui font que le programme ne peut pas s'exécuter sur cette plate-forme.
Voici un exemple où on teste que la taille des entiers est suffisante~:
\begin{verbatim}
#include <limits.h>
#if INT_MAX < 1000000
#error "Entiers trop petits sur cette machine"
#endif
\end{verbatim}

\subsection{Usage}
%-----------------
\index{compilateur!mise en oeuvre du}
La compilation conditionnelle a pour but essentiel d'adapter le programme
à son environnement d'exécution~: soit il s'agit d'un programme système
devant s'adapter au matériel sur lequel il s'exécute, soit il s'agit d'un
programme d'application qui doit s'adapter au système sur lequel il s'exécute.
Prenons par exemple le système {\sc unix} qui existe en deux grandes
variantes~: la variante {\sc bsd} et la variante {\sc system v}.
La routine de recherche d'un caractère déterminé dans une chaîne s'appelle
{\tt index} en {\sc bsd} et {\tt strchr} en {\sc system v}, mais l'interface
est le même.
Voici comment on peut écrire un programme se compilant et s'exécutant sur les
deux plate-formes~: le programmeur peut décider d'utiliser un nom à lui,
par exemple RechercheCar, et de le définir comme ci-dessous.
\begin{verbatim}
#if defined(HAS_INDEX)
#define RechercheCar index
#elif defined(HAS_STRCHR)
#define RechercheCar strchr
#else
#error "Impossible de réaliser RechercheCar"
#endif
\end{verbatim}
Selon le système, la compilation se fera par~:
\begin{verbatim}
cc -c -DHAS_INDEX fichier.c
\end{verbatim}
ou par~:
\begin{verbatim}
cc -c -DHAS_STRCHR fichier.c
\end{verbatim}

\section{Récréation}
%===================
Quel est le plus petit programme possible en C ?

Mark Biggar a été un vainqueur de la compétition du code C le plus obscur
({\sc ioccc}) avec un programme ne comportant qu'une seule lettre~: {\tt P} !
Pour arriver à ce résultat, il avait compliqué un petit peu la ligne de
commande de compilation~:
\begin{verbatim}
cc -DC="R>0" -DI="if(T)O" -DO="c=write(1,&c,1);" \
   -DP="main(){X}" -DR="read(0,&c,1)" -DT="c!=015" \
   -DW="while(C)I" -DX="char c;W" markb.c
\end{verbatim}
Le fichier {\tt markb.c} contenant la lettre {\tt P}, qui 
du fait de l'expansion des macros, va être transformée en~:
\begin{latexonly}
%   newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}
\begin{verbatim}
main()
{
char c;
while(read(0,&c,1) >0)
   if (c!=015) c=write(1,&c,1);
} 
\end{verbatim}
