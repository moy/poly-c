% \setcounter{chapter}{2}
% \setcounter{page}{41}

\chapter{Les pointeurs}
%**********************

\section{Notion de pointeur}
%===========================
\index{pointeur!concept de}

Une valeur de type pointeur repère une variable. En pratique, cela signifie
qu'une valeur de type pointeur est l'adresse d'une variable.

\begin{center}
\setlength{\unitlength}{1mm}
\begin{picture}(110,40)
\put(0,10){\framebox(30,8)}
\put(80,10){\framebox(30,8){372}}
\put(15,14){\vector(1,0){64}}
\put(0,28){variable de type}
\put(0,23){pointeur vers un int}
\put(90,23){int}
\end{picture}
\end{center}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Déclarations de variables de type pointeur vers les types de base}
%==========================================================================
\index{declaration@{déclaration}!de pointeur}

Pour déclarer une variable pointeur vers un type de base~:
\begin{itemize}
\item[\ptiret]
   partir de la déclaration d'une variable ayant un type de base~;
\item[\ptiret]
   ajouter le signe \verb+*+ devant le nom de la variable.
\end{itemize}
Exemple~:
%
\begin{verbatim}
int *pi;        /* pi est un pointeur vers un int                */
short int *psi; /* psi est un pointeur vers un short int         */
double *pd;     /* pd pointeur vers un flottant double précision */
char *pc;       /* pc pointeur vers un char                      */
\end{verbatim}

\section{Type de pointeur générique}
%===================================
\index{pointeur!generique@générique}
\index{void@{\tt void}}
Le type \verb+void *+ est le type pointeur générique, c'est à dire capable
de pointer vers n'importe quel type d'objet.
Sans un tel type, il ne serait pas possible par exemple d'indiquer le type
d'objet rendu par les fonctions
d'allocation de mémoire qui rendent un pointeur vers l'objet alloué, puisque
ce type varie d'une invocation à l'autre de la fonction.
\index{allocation de mémoire}

Par exemple, la fonction \verb+malloc+ de la bibliothèque standard est
définie de la manière suivante~:
{\tt void *malloc(size\_t size);}\footnote{{\tt size\_t} est un type défini dans
{\tt stddef.h}}
\index{malloc@{\tt malloc}}

\subsubsection*{Note}
%....................
La solution quand on utilisait les compilateurs K\&R était d'utiliser
le type \verb+char *+ qui jouait de manière conventionnelle ce rôle de
pointeur générique.
\index{k&r@{\sc k\&r}}
Le lecteur ne s'étonnera donc pas de trouver dans les vieux (?) manuels, la
fonction \verb+malloc+ définie de la manière suivante~:
\begin{verbatim}
char *malloc(size);
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Opérateur adresse de}
%=============================
\index{operateur@{opérateur}!\&@{\tt \&}!adresse de}

L'opérateur \verb+&+ appliqué à une variable délivre l'adresse de celle-ci~;
cette adresse pourra être affectée à une variable de type pointeur.
On peut écrire par exemple~:
\begin{verbatim}
int i;
int *pi;   /*   pi pointeur vers un int   */

pi = &i;   /*   le pointeur pi repère la variable i   */
\end{verbatim}

\section{Opérateur d'indirection}
%================================
\index{operateur@{opérateur}!*@{\tt *}!indirection}

Lorsque l'opérateur \verb+*+ est utilisé en opérateur préfixé, il agit
de l'opérateur indirection qui, appliqué à
une valeur de type pointeur, délivre la valeur pointée.
On peut écrire par exemple~:
\begin{verbatim}
int i;
int *pi;

pi = &i;       /*   initialisation du pointeur pi                 */
*pi = 2;       /*   initialisation de la valeur pointée par pi    */
j = *pi + 1;   /*   une utilisation de la valeur pointée par pi   */
\end{verbatim}

\subsubsection*{Remarque}
%........................
\index{surcharge!de {\tt *}}
L'opérateur {\tt *} est surchargé~: il peut être opérateur
de multiplication ou opérateur d'indirection. La grammaire lève
l'ambiguïté car l'opérateur d'indirection est préfixé, alors que l'opérateur
de multiplication est infixé. Surcharger les opérateurs gêne la lisibilité
des programmes. Exemple~: si {\tt i} et {\tt j} sont des pointeurs vers des
entier, la multiplication des deux valeurs pointées s'écrit~: {\tt *i**j}

\subsubsection*{Devinette}
%........................
Si {\tt i} et {\tt j} sont des pointeurs vers des entiers, {\tt *i**j} est
le produit des valeurs pointées, mais {\tt *i/*j} est-il le quotient des
valeurs pointées~?
Réponse à la fin de ce chapitre.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Exercice}
%=================

\begin{enumerate}
\item Déclarer un entier {\tt i} et un pointeur {\tt p} vers un entier~;
\item Initialiser l'entier à une valeur arbitraire et faire pointer {\tt p} vers {\tt i}~;
\item Imprimer la valeur de {\tt i}~;
\item Modifier l'entier pointé par {\tt p} (en utilisant {\tt p,} pas {\tt i)}~;
\item Imprimer la valeur de {\tt i}.
\end{enumerate}

Une solution possible est donnée page suivante.
\newpage
\include{exer41}

\section{Pointeurs et opérateurs additifs}
%=========================================

\subsection{Opérateurs {\tt +} et {\tt -}}
%-----------------------------------------
\index{pointeur!et opérateurs {\tt +} et {\tt -}}
\index{pointeur!arithmétique sur}
L'opérateur {\tt +} permet de réaliser la somme de deux valeurs arithmétiques,
mais il permet également de réaliser la somme d'un pointeur et d'un entier.
Une telle opération n'a de sens cependant, que si le pointeur repère un
élément d'un tableau.

Soient p une valeur pointeur vers des objets de type T et un tableau dont
les éléments sont du même type T~;
si p repère l'élément d'indice i du tableau,
p + j {\bf est une valeur de type pointeur vers T},
qui repère l'élément d'indice i + j du tableau (en supposant qu'il existe).

Il en va de même avec l'opérateur soustraction~: si p repère
l'élément d'indice i d'un tableau, p - j repère l'élément d'indice i - j
du tableau (toujours en supposant qu'il existe).
Exemple~:
%
\begin{verbatim}
#define N 10
int t[N];
int *p,*q,*r,*s;

p = &t[0];       /*   p repère le premier élément de t   */
q = p + (N-1);   /*   q repère le dernier élément de t   */

r = &t[N-1];     /*   r repère le dernier élément de t   */
s = r - (N-1);   /*   s repère le premier élément de t   */
\end{verbatim}

La norme précise que pour réaliser la somme ou la différence d'un pointeur
et d'un entier, il faut qu'à la fois le pointeur et le résultat repèrent les
éléments d'un même tableau ou l'élément (fictif) après le dernier élément du
tableau.
En d'autre termes, si on a~:
\begin{verbatim}
#define N 100
int t[N];
int * p = &t[0];
\end{verbatim}
L'expression \verb|p + N| est valide, mais \verb|p - 1| ou \verb|p + N + 1|
ne sont pas valides.
La possibilité de référencer l'élément (fictif) après le dernier élément
d'un tableau a été introduite pour les problèmes de fin de boucle de
parcours de tableau (mais on aurait pu s'en passer).

\begin{latexonly}
\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}
\end{latexonly}

\subsection{Opérateurs {\tt ++} et {\tt --}}
%-------------------------------------------
\index{pointeur!et opérateurs {\tt ++} et {\tt --}}
On peut appliquer les opérateurs {\tt ++} et {\tt --} à des pointeurs et il est
classique de les utiliser pour réaliser des parcours de tableaux.
Exemple (on rappelle que toute chaîne est terminée par un {\it null}, c'est
à dire le caractère \verb+'\0'+)~:
\index{caractere@{caractère}!null@{\it null\/}}
\begin{verbatim}
char mess[] = "Hello world!!";
char *p;

for (p = &mess[0]; *p != '\0'; p++)
   {
   /*   ici p repère l'élément courant de mess   */
   }

\end{verbatim}

Autre classique, en reprenant les variables \verb+mess+ et \verb+p+ de
l'exemple précédent~:
\begin{verbatim}
p = &mess[0];
while (*p != '\0')
   {
   /*   ici utilisation de *p++   */
   }
\end{verbatim}

\section{Différence de deux pointeurs}
%=====================================
\index{ptrdiff@{\tt ptrdiff\_t}}
Il est possible d'utiliser l'opérateur de soustraction pour calculer la
différence de deux pointeurs. Cela n'a de sens que si les deux pointeurs
repèrent des éléments d'un même tableau.

Soient p1 et p2 deux pointeurs du même type tels que p1 repère le i$^{eme}$
élément d'un tableau, et p2 repère le j$^{eme}$ élément du même tableau,
p2 - p1 {\bf est une valeur de type \verb+ptrdiff_t+} qui est égale à j - i.
Le type \verb+ptrdiff_t+ est défini dans le fichier d'include \verb+stddef.h+
En pratique, une variable de type \verb+ptrdiff_t+ pourra être utilisée comme
une variable de type \verb+int+.
La norme précise qu'il est valide de calculer la différence de deux pointeurs
à condition que tous deux repèrent des éléments d'un même tableau, ou 
l'élément (fictif) après le dernier élément du tableau.

\section{Exercice}
%=================

Déclarer et initialiser statiquement un tableau d'entiers {\tt t} avec des 
valeurs dont certaines seront nulles.
Écrire une procédure qui parcoure le tableau {\tt t} et qui imprime les index
des éléments nuls du tableau, {\bf sans utiliser aucune variable de type
entier}.
Une solution possible est donnée page suivante.
\newpage
\include{exer42}

\section{Passage de paramètres}
%==============================
\index{parametre@{paramètre}!passage de}

\subsection{Les besoins du programmeur}
%--------------------------------------
En ce qui concerne le passage de paramètres à une procédure, le programmeur
a deux besoins fondamentaux~:
\begin{itemize}
\item[\ptiret]
soit il désire passer une valeur qui sera
exploitée par l'algorithme de la procédure (c'est ce dont on a besoin quand
on écrit par exemple sin(x)).
Une telle façon de passer un paramètre s'appelle du {\it passage par valeur}~;
\index{parametre@{paramètre}!passage de!par valeur}

\item[\ptiret]
soit il désire passer une référence à une variable, de
manière à permettre à la procédure de modifier la valeur de cette
variable.
C'est ce dont on a besoin quand on écrit une procédure réalisant le produit
de deux matrices {\tt prodmat(a,b,c)} où l'on veut qu'en fin d'exécution
de {\tt prodmat}, la matrice {\tt c} soit égale au produit matriciel des
matrices {\tt a} et {\tt b}.
{\tt prodmat} a besoin des valeurs des matrices {\tt a} et
{\tt b}, et d'une référence vers la matrice {\tt c}.
Une telle façon de passer un paramètre s'appelle du {\it passage par adresse}.
\index{parametre@{paramètre}!passage de!par adresse}
\end{itemize}

\subsection{Comment les langages de programmation satisfont ces besoins}
%-----------------------------------------------------------------------

Face à ces besoins, les concepteurs de langages de programmation ont
imaginé différentes manières de les satisfaire, et quasiment chaque
langage de programmation dispose de sa stratégie propre de passage de
paramètres.

\begin{itemize}
\item[\ptiret]
Une première possibilité consiste à arguer du fait que le passage de
paramètre par adresse est plus puissant que le passage par valeur, et à
réaliser tout passage de paramètre par
adresse (c'est la stratégie de {\sc fortran} et {\sc pl/}{\small 1})~;

\item[\ptiret]
Une seconde possibilité consiste à permettre au programmeur de déclarer
explicitement quels paramètres il désire passer par valeur et quels
paramètres il désire passer par adresse (c'est la stratégie de {\sc pascal})~;

\item[\ptiret]
La dernière possibilité consistant à réaliser tout passage de
paramètre par valeur semble irréaliste puisqu'elle ne permet pas de
satisfaire le besoin de modification d'un paramètre.
C'est cependant la stratégie choisie par les concepteurs du langage C.
\end{itemize}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{La stratégie du langage C}
%------------------------------------

En C, tout paramètre est passé par valeur, et cette règle ne souffre
aucune exception.
Cela pose le problème de réaliser un passage de paramètre par adresse lorsque
le programmeur en a besoin. La solution à ce problème consiste dans ce cas,
à déclarer le paramètre comme étant un pointeur. Cette solution n'est
rendue possible que par l'existence de l'opérateur {\it adresse de} qui permet
de délivrer l'adresse d'une {\it lvalue}.
\index{operateur@{opérateur}!\&@{\tt \&}!adresse de}

Voyons sur un exemple. Supposons que nous désirions écrire une procédure
{\tt add}, admettant trois paramètres {\tt a}, {\tt b} et {\tt c}.
Nous désirons que le résultat de
l'exécution de {\tt add} soit d'affecter au paramètre {\tt c} la somme
des valeurs des deux premiers paramètres.
Le paramètre {\tt c} ne peut évidemment pas être passé par valeur,
puisqu'on désire modifier la valeur du paramètre effectif correspondant.
Il faut donc programmer {\tt add} de la manière suivante~:
\begin{latexonly}
%   newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}
\begin{verbatim}
void add(int a, int b, int *c)
/*   c repère l'entier où on veut mettre le résultat   */

{
*c = a + b;
}

int main(void)
{
int i,j,k;

/*   on passe les valeurs de i et j comme premiers paramètres  */
/*   on passe l'adresse de k comme troisième paramètre         */
add(i,j,&k);
}
\end{verbatim}

\section{Discussion}
%===================
\begin{enumerate}
\item
Nous estimons qu'il est intéressant pour le programmeur de raisonner en
terme de {\it passage par valeur} et de {\it passage par adresse} et
qu'il est préférable 
d'affirmer, lorsque l'on écrit \verb+f(&i);+ << {\tt i} est passé par
adresse à {\tt f} >>, plutôt que
d'affirmer <<~l'adresse de {\tt i} est passée par valeur à {\tt f}~>>, tout en
sachant que c'est la deuxième affirmation qui colle le mieux à la stricte
réalité du langage.
Que l'on ne s'étonne donc pas dans la suite de ce manuel de nous entendre
parler de passage par adresse.

\item
Nous retiendrons qu'en C, le passage de paramètre par adresse est entièrement
géré par le programmeur. C'est à la charge du programmeur de déclarer le
paramètre concerné comme étant de type pointeur vers ... et de bien songer,
lors de l'appel de la fonction, à passer l'adresse du paramètre effectif.
\end{enumerate}

\section{Une dernière précision}
%===============================
Quand un langage offre le passage de paramètre par valeur, il y a deux
possibilités~:
\begin{enumerate}
\item soit le paramètre est une constante (donc non modifiable)
\item soit le paramètre est une variable locale à la procédure. Cette variable
est initialisée lors de l'appel de la procédure avec la valeur du paramètre
effectif.
\end{enumerate}
\index{variable!locale}
C'est la seconde solution qui a été retenue par les concepteurs du langage C.
Voyons sur un exemple. Supposons que l'on désire écrire une fonction
{\tt sum} admettant comme paramètre {\tt n} et qui rende la somme des {\tt n}
premiers entiers. On peut programmer de la manière suivante~:

\begin{verbatim}
int sum(int n)
{
int r = 0;

for ( ; n > 0; n--) r = r + n;
return(r);
}
\end{verbatim}
On voit que le paramètre {\tt n} est utilisé comme variable locale, et que
dans l'instruction {\tt for}, la partie initialisation est vide puisque {\tt n}
est initialisée par l'appel de {\tt sum}.

\section{Exercice}
%=================

On va coder un algorithme de cryptage très simple~: on choisit un décalage
(par exemple 5), et un a sera remplacé par un f, un b par un g, un c par un h,
etc.
On ne cryptera que les lettres majuscules et minuscules sans toucher ni à la
ponctuation ni à la mise en page (caractères blancs et {\it line feed}).
On supposera que les codes des lettres se suivent de a à z et de A à Z.
On demande de~:

\begin{enumerate}
\item déclarer un tableau de caractères {\tt mess} initialisé avec le 
message en clair~;

\item écrire une procédure {\tt crypt} de cryptage d'un caractère qui
sera passé par adresse~;

\item écrire le {\tt main} qui activera {\tt crypt} sur l'ensemble du
message et imprimera le résultat.

\end{enumerate}

\newpage
\include{exer43}

\section{Lecture formattée}
%==========================
\index{bibliothèque standard}
\index{scanf@{\tt scanf}}
\index{sequence d'echappement@{séquence d'échappement}}

Il existe dans la bibliothèque standard une fonction de lecture formattée qui
fonctionne selon le même principe que la procédure {\tt printf}.
Sa syntaxe d'utilisation est la suivante~:
\newline
{\tt scanf} \hspace{3mm}{\tt (} \hspace{3mm}{\it format} \hspace{3mm}{\tt ,} 
   \hspace{3mm}{\it liste-d'expressions} \hspace{3mm}{\tt )} \hspace{3mm}{\tt ;}
\newline
{\it format} est une chaîne de caractères indiquant sous forme de séquences
d'échappement les entités que {\tt scanf} lit sur l'{\it entrée standard}~:
\begin{itemize}
\item[\ptiret] \verb|%d| pour un nombre décimal~;
\item[\ptiret] \verb|%x| pour un nombre écrit en hexadécimal~;
\item[\ptiret] \verb|%c| pour un caractère.
\end{itemize}
%
Tous les éléments de la {\it liste-d'expressions} doivent délivrer après 
évaluation l'adresse de la variable dans laquelle on veut mettre la valeur lue.
Si le flot de caractères lu sur l'{\it entrée standard} ne correspond pas à 
{\it format}, {\tt scanf} cesse d'affecter les variables de
{\it liste-d'expressions} à la première disparité.
\index{entree standard@{entrée standard}}
Exemple~:

\begin{verbatim}
scanf("%d %x",&i,&j);
\end{verbatim}
cette instruction va lire un nombre écrit en décimal et mettre sa valeur dans la
variable {\tt i,} puis lire un nombre écrit en hexadécimal et mettre sa valeur
dans la variable {\tt j}.
On aura remarqué que les paramètres {\tt i} et {\tt j} ont étés
passés par adresse à {\tt scanf}.
Si l'{\it entrée standard} commence par un nombre décimal non suivi d'un
nombre hexadécimal, seul \verb+i+ recevra une valeur.
Si l'{\it entrée standard} ne commence pas par un nombre décimal, ni {\tt i}
ni {\tt j} ne recevront de valeur.

\subsubsection*{Valeur rendue}
%.............................
Sur rencontre de fin de fichier, la fonction \verb+scanf+ rend \verb+EOF+, sinon
elle rend le nombre de variables qu'elle a pu affecter.
\index{eof@{\tt EOF}}

\subsubsection*{Attention}
%.........................
Une erreur facile à commettre est d'omettre les opérateurs \verb+&+ devant
les paramètres de {\tt scanf}.
\index{operateur@{opérateur}!\&@{\tt \&}!adresse de}
C'est une erreur difficile à détecter car le compilateur ne donnera aucun
message d'erreur et à l'exécution, ce sont les valeurs de {\tt i} et
{\tt j} qui seront interprétées comme des adresses par {\tt scanf}.
Avec un peu de chance ces valeurs seront des adresses invalides, et le 
programme s'avortera\footnote{Le terme avorter est à prendre au sens technique
de {\it abort}} sur l'exécution du {\tt scanf}, ce qui donnera une
idée du problème.
Avec un peu de malchance, ces valeurs donneront
des adresses parfaitement acceptables, et le {\tt scanf} s'exécutera
en allant écraser les valeurs d'autres variables qui ne demandaient rien
à personne.
Le programme pourra s'avorter beaucoup plus tard, rendant très difficile la
détection de l'erreur.

\section{Les dernières instructions}
%===================================
Le langage C comporte 3 instructions que nous n'avons pas encore vu~: 
un {\it if} généralisé, un {\it goto} et une instruction nulle.

\subsection{Instruction {\tt switch}}
%------------------------------------
\index{instruction!switch@{\tt switch}}
\index{instruction!break@{\tt break}}
\index{case@{\tt case}}
\index{default@{\tt default}}
Le langage C offre une instruction {\tt switch} qui est un {\tt if}
généralisé.
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it instruction}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt switch} {\tt (} {\it expression} {\tt )}\\
\hspace*{18mm}\verb+{+\\
\hspace*{18mm}{\tt case} \hspace{3mm}{\it expression\/$_1$} \hspace{3mm}{\tt :} \hspace{3mm}{\it liste-d'instructions$_{1 \hspace{1mm}option}$} \hspace{3mm}{\tt break;$_{option}$}\\
\hspace*{18mm}{\tt case} \hspace{3mm}{\it expression\/$_2$} \hspace{3mm}{\tt :} \hspace{3mm}{\it liste-d'instructions$_{2 \hspace{1mm}option}$} \hspace{3mm}{\tt break;$_{option}$}\\

\hspace*{18mm}{\tt ....}\\

\hspace*{18mm}{\tt case} \hspace{3mm}{\it expression\/$_n$} \hspace{3mm}{\tt :} \hspace{3mm}{\it liste-d'instructions$_{n \hspace{1mm}option}$} \hspace{3mm}{\tt break;$_{option}$}\\
\hspace*{18mm}{\tt default} \hspace{3mm}{\tt :} \hspace{3mm}{\it liste-d'instructions}\\
\verb+}+
\index{instruction!break@{\tt break}}

De plus~:
\begin{itemize}
\item[\ptiret] toutes les {\it expression\/$_i$} doivent délivrer une valeur
connue à la compilation~;
\item[\ptiret] il ne doit pas y avoir deux {\it expression\/$_i$} délivrant
la même valeur~;
\item[\ptiret] l'alternative {\tt default} est optionnelle.
\end{itemize}
%
\item[$\bullet$] Sémantique~:
\begin{enumerate}
\item {\it expression} est évaluée, puis le résultat est comparé avec
{\it expression\/$_1$}, {\it expression\/$_2$,} etc.
\item à la première {\it expression\/$_i$} dont la valeur est égale à
celle de {\it expression}, on exécute la (ou les)
\footnote{la ou les, car dans chaque {\tt case}, le {\tt break} est optionnel}
{\it liste-d'instructions}
correspondante(s) jusqu'à la rencontre de la première instruction {\tt break;}.
La rencontre d'une instruction {\tt break} termine l'exécution de
l'instruction {\tt switch}.
\item si il n'existe aucune {\it expression\/$_i$} dont la valeur soit
égale à celle de {\it expression,} on exécute la
{\it liste-d'instructions} de l'alternative {\tt default} si celle-ci existe,
sinon on ne fait rien.
\end{enumerate}
\end{itemize}

\subsubsection*{Discussion}
%..........................
Vu le nombre de parties optionnelles dans la syntaxe, il y a 3 types
d'utilisations possibles pour le {\tt switch}.
Première possibilité, on peut avoir dans chaque alternative une
{\it liste-d'instructions} et un {\tt break;} comme dans l'exemple suivant~:

\begin{latexonly}
%   un newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}

\begin{verbatim}
enum {BLEU=1, BLANC, ROUGE};

void print_color(int color)
{
switch(color)
   {
   case BLEU : printf("bleu"); break;
   case BLANC : printf("blanc"); break;
   case ROUGE : printf("rouge"); break;
   default : printf("erreur interne du logiciel numéro xx\n");
   }
}
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

Deuxième possibilité, on peut avoir une ou plusieurs alternatives ne
possédant ni {\it liste-d'instructions}, ni {\tt break;}.
Supposons que l'on désire compter dans une suite de caractères, le nombre
de caractères qui sont des chiffres, et le nombre de caractères qui ne le
sont pas. On peut utiliser le {\tt switch} suivant~:
\begin{verbatim}
switch(c)
   {
   case '0':
   case '1':
   case '2':
   case '3':
   case '4':
   case '5':
   case '6':
   case '7':
   case '8':
   case '9': nb_chiffres++; break;
   default: nb_non_chiffres++;
   }
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

Troisième possibilité, une alternative peut ne pas avoir de {\tt break}
comme dans l'exemple suivant~:
\begin{verbatim}
enum {POSSIBLE, IMPOSSIBLE};

void print_cas(int cas)
{
switch (cas)
   {
   case IMPOSSIBLE: printf("im");
   case POSSIBLE:   printf("possible\n"); break;
   case default:    printf("erreur interne du logiciel numéro xx\n");
   }
}
\end{verbatim}
Une telle utilisation du {\tt switch} pose un problème de lisibilité, car
l'expérience montre que l'absence du {\tt break;} est très difficile à voir.
Il est donc recommandé de mettre un commentaire, par exemple de la façon
suivante~:
\begin{verbatim}
   case IMPOSSIBLE: printf("im");   /*   ATTENTION: pas de break;   */
\end{verbatim}

\subsubsection*{Remarque}
%..........................
\index{surcharge!de {\tt break}}
Le mot-clé {\tt break} est surchargé~: nous avons vu au chapitre 
\ref{instruction-break} que l'instruction {\tt break} permettait de stopper
l'exécution d'une instruction itérative {\tt for}, {\tt while}, {\tt do}.
Il est utilisé ici de manière complètement différente.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Instruction {\tt goto}}
%----------------------------------
\index{instruction!goto@{\tt goto}}
\index{definition@{définition}!d'etiquette de branchement@{d'étiquette de branchement}}
\label{instruction-goto}

\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it instruction}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}{\tt goto} \hspace{3mm}{\it identificateur} \hspace{3mm}{\tt ;}

\item[$\bullet$] Sémantique~:

Toute instruction peut être précédée d'un identificateur suivi du signe
{\tt :}. 
Cet identificateur est appelé {\it étiquette}.
\index{etiquette@{étiquette}}
Une instruction {\tt goto} {\it identificateur} a pour effet de transférer
le contrôle d'exécution à l'instruction étiquetée par
{\it identificateur}.
L'instruction {\tt goto} et l'instruction cible du {\tt goto} doivent se
trouver dans la même procédure~: le langage C est un langage à branchement
locaux.
\item[$\bullet$] Exemple~:
\begin{verbatim}
{
etiq2:
...            /*   des instructions                      */
goto etiq1;    /*   goto avant définition de l'étiquette  */
...            /*   des instructions                      */
etiq1:
...            /*   des instructions                      */
goto etiq2;    /*   goto après définition de l'étiquette  */
}
\end{verbatim}
\end{itemize}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Instruction nulle}
%-----------------------------
\index{instruction!nulle}
\begin{itemize}
\item[$\bullet$] Syntaxe~:

{\it instruction}~:\\
\hspace*{10mm} $\Rightarrow$ \hspace{3mm}\hspace{3mm}{\tt ;}

\item[$\bullet$] Sémantique~:

ne rien faire~!

\end{itemize}

Cette instruction ne rend que des services syntaxiques.
Elle peut être utile quand on désire mettre une étiquette à la fin d'une
instruction composée.
Par exemple~:
\begin{verbatim}
{
...
fin: ;
}
\end{verbatim}

Elle est également utile pour mettre un corps nul à certaines instructions
itératives.
En effet, à cause des effets de bord dans les expressions,
on peut arriver parfois à faire tout le travail dans les expressions
de contrôle des instructions itératives.
Par exemple, on peut initialiser à zéro un tableau de la manière suivante~:
\begin{verbatim}
for (i = 0; i < N; t[i++]= 0) 
   ;    /*   instruction nulle   */
\end{verbatim}
\index{instruction!for@{\tt for}}

\subsubsection{Attention}
%........................
Cette instruction nulle peut parfois avoir des effets désastreux.
Supposons que l'on veuille écrire la boucle~:
\begin{verbatim}
for (i = 0; i < N; i++)
   t[i] = i;
\end{verbatim}
si par mégarde on met un {\tt ;} à la fin de ligne du {\tt for}, on obtient
un programme parfaitement correct, qui s'exécute sans broncher, mais ne fait
absolument pas ce qui était prévu.
En effet~:
\begin{verbatim}
for (i = 0; i < N; i++) ;
   t[i] = i;
\end{verbatim}
exécute le {\tt for} avec le seul effet d'amener la variable {\tt i} à
la valeur N, et ensuite exécute une fois {\tt t[i] = i} ce qui a 
probablement pour effet d'écraser la variable déclarée juste après le
tableau {\tt t}.

\section{Exercice}
%=================

Écrire une procédure {\tt main} se comportant comme une calculette
c'est à dire exécutant une boucle sur~:
\begin{enumerate}
   \item lecture d'une ligne supposée contenir un entier, un opérateur et un
   entier (ex~: 12 + 34), les opérateurs seront \verb|+ - * / %|~;
   \item calculer la valeur de l'expression~;
   \item imprimer le résultat.
\end{enumerate}

\subsection*{Commentaire de la solution}
%---------------------------------------
Nous faisons ici un commentaire sur la solution proposée qui se trouve à la
page suivante.
Dans le cas où la ligne lue n'a pas une syntaxe correcte (elle ne contient pas
un nombre, un signe, un nombre), le programme émet un message d'erreur et
exécute \verb+exit(1)+.
Ceci ne réalise pas un interface utilisateur bien agréable, car
il serait plus intéressant de continuer la boucle au lieu de terminer le
programme.
Cela n'a pas été implémenté car ce n'est pas réalisable à l'aide des seules
possibilités de base de \verb+scanf+ qui ont été présentées.
Dans le chapitre <<~Les entrées-sorties~>>, \verb+scanf+ sera expliqué de 
manière exhaustive et une meilleure version de ce programme sera présentée.

\newpage
\input{exer44}

\newpage
\section{Récréation}
%===================
Pour les amateurs de palindromes, voici la contribution de Brian Westley a la
compétition du code C le plus obscur ({\sc ioccc}) de 1987.
{
\small
\input{westley-1987}
}
L'exécution de ce programme imprime le palindrome~: <<~Able was I ere I saw
elbA~>>.

\subsection*{Réponse de la devinette}
%------------------------------------
Non, {\tt *i/*j} n'est pas un quotient, car {\tt /*} est un début de
commentaire. Pour obtenir le quotient de {\tt *i} par {\tt *j} il faut utiliser
du blanc~: {\tt *i / *j}.
