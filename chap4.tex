% \setcounter{chapter}{3}
% \setcounter{page}{59}

\chapter{Relations entre tableaux et pointeurs}
%**********************************************
\index{tableau!generalite@{généralité}}

\section{Conversion des tableaux}
%================================
\index{conversion!de tableau}
\index{pointeur!et tableau}
\index{tableau!et pointeur}
\label{conversion-tableau}
Nous avons jusqu'à présent utilisé les tableaux de manière intuitive,
en nous contentant de savoir qu'on peut déclarer un tableau par une
déclaration du genre~:
%
\begin{verbatim}
int t[10];
\end{verbatim}
%
et qu'on dispose d'un opérateur d'indexation noté \verb+[]+,
permettant d'obtenir un élément du tableau~:
l'élément d'index {\tt i} du tableau {\tt t} se désigne par {\tt t[i]}.
Il est temps maintenant d'exposer une caractéristique originale des 
références à un tableau dans le langage C~: elles subissent une conversion
automatique.

\subsubsection*{Règle~:}
%........................
Tout identificateur de type <<~tableau de {\sc x}~>> apparaissant dans
une expression est converti en une valeur constante dont~:
\begin{itemize}
\item[\ptiret] le type est <<~pointeur vers {\sc x}~>>~;
\item[\ptiret] la valeur est l'adresse du premier élément du tableau.
\end{itemize}

Cette conversion n'a lieu que pour un identificateur de type
<<~tableau de {\sc x}~>> {\bf apparaissant dans une expression}.
En particulier, elle n'a pas lieu lors de la déclaration.
Quand on déclare \verb+int T[10]+, le compilateur mémorise que {\tt T}
est de type <<~tableau de 10 int~>> et réserve de la place en mémoire pour
10 entiers.
C'est lors de toute utilisation ultérieure de l'identificateur {\tt T},
que cette occurrence de {\tt T} sera convertie en type \verb+int *+, de
valeur adresse de \verb+T[0]+.

\subsubsection*{Remarques}
%.........................
\begin{enumerate}
\item La conversion automatique d'un identificateur ayant le type tableau
empêche de désigner un tableau en entier, c'est pour cette
raison que l'opérateur d'affectation ne peut affecter un tableau à un autre
tableau~:
\begin{verbatim}
int t1[10];
int t2[10];
t1 = t2;     /*   le compilateur rejettera cette instruction   */
\end{verbatim}
Un telle affectation ne peut se réaliser qu'à l'aide d'une procédure qui 
réalisera l'affectation élément par élément.

\item  Un identificateur ayant le type tableau est converti en une valeur
constante, on ne peut donc rien lui affecter~:
\begin{verbatim}
int *p;
int t[10];
t = p;      /*   interdit   */
p = t;      /*   valide     */
\end{verbatim}

\end{enumerate}
\vspace*{5mm}
L'existence des conversions sur les références aux tableaux va avoir deux
conséquences importantes~: la
première concerne l'opérateur d'indexation et la seconde le passage de
tableaux en paramètre.

%%%%%%%%%%%%%%%%%%%%%%    ABANDONNE   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Ceci est suffisant pour les utilisations les plus simples des tableaux, mais
%dès que l'on veut faire des choses plus complexes, (par exemple passer des
%tableaux en paramètre), il est nécessaire d'aller plus à fond dans le
%concept de tableau tel que l'envisage le langage C.
%
%De manière à bien mettre en évidence les particularités des tableaux
%dans le langage C, nous allons faire un rappel sur la notion de tableau
%telle qu'elle est généralement envisagée dans les langages de
%programmation.
%
%\section{La notion de tableau en programmation}
%%==============================================
%
%La plupart des langages de programmation disposent du concept de tableau.
%Une telle notion offre au programmeur deux services principaux~:
%\begin{itemize}
%\item déclarer des tableaux
%\item d'utiliser un opérateur d'indexation qui, selon les langages,
%peut être noté {\tt ()}
%ou {\tt []}, admettant en opérandes un tableau {\tt t} et un entier {\tt i},
%et délivrant l'élément d'index {\tt i} de {\tt t}.
%\end{itemize}
%
%Derrière les particulariés de chaque langage (parfois
%les bornes des tableaux peuvent être connue dynamiquement,
%parfois les bornes doivent être connues statiquement, etc.)
%le programmeur peut cependant se raccrocher à un certain nombre de points
%communs.
%
%Généralement en effet, quand on déclare un tableau de nom {\tt t}, 
%\begin{enumerate}
%\item {\tt t} est une variable,
%\item {\tt t} est de type tableau de {\it quelque chose},
%\item {\tt t} désigne le tableau en entier.
%\end{enumerate}
%
%Voyons ce qu'il en est en ce qui concerne la façon dont C envisage les
%tableaux.
%
%\section{La notion de tableau dans le langage C}
%%===============================================
%Sous une similitude de surface, le langage C cache de
%profondes différences avec ce que nous venons de voir.
%En effet, lorsqu'en C, on déclare un tableau de nom {\tt t},
%\begin{enumerate}
%\item {\tt t} {\bf n'est pas} une variable,
%\item {\tt t} {\bf n'est pas} de type tableau de {\it quelque chose},
%\item {\tt t} {\bf ne désigne pas} le tableau en entier.
%\end{enumerate}
%
%Que se passe-t-il alors~?
%\newline
%En C, quand on écrit~:
%\begin{verbatim}
%int t[10];
%\end{verbatim}
%
%on a bien déclaré une variable de type tableau, mais cette variable
%{\bf n'a pas de nom}.
%
%Le nom {\tt t} est le nom d'une {\bf constante} qui est de type pointeur
%vers {\tt int}, et dont la valeur est l'adresse du premier élément du
%tableau~: la valeur de {\tt t} est \verb+&t[0]+.
%
%Le fait de déclarer une variable qui n'a pas de nom est un mécanisme qui
%est bien connu dans les langages de programmation.
%La situation est comparable à ce que l'on obtient lorsqu'on fait l'allocation
%dynamique d'une variable (via le new de {\sc pascal}, l'allocate de 
%{\sc PL/I} etc.).
%La variable allouée n'a pas de nom, elle ne peut être désignée que via
%un pointeur.
%Ce qui est original ici, c'est de retrouver ce mécanisme sur une allocation
%de variable qui est une allocation statique.
%
%Une telle façon d'envisager les tableaux dans le langage C
%va avoir deux conséquences importantes~: la
%première concerne l'opérateur d'indexation et la seconde la passage de
%tableaux en paramètre.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{L'opérateur d'indexation}
%------------------------------------
\index{operateur@{opérateur}!indexation}
\label{operateur-indexation}

La sémantique de l'opérateur d'indexation consiste à dire qu'après les
déclarations~:
\begin{verbatim}
int t[N];
int i;
\end{verbatim}
{\tt t[i]} est équivalent à {\tt *(t + i)}.
Vérifions que cela est bien conforme à la façon dont nous l'avons utilisé
jusqu'à présent.
Nous avons vu que {\tt t} à pour valeur l'adresse du premier élément du
tableau.
D'après ce que nous savons sur l'addition entre un pointeur et un entier, nous
pouvons conclure que {\tt t + i} est l'adresse de l'élément de rang {\tt i}
du tableau.
Si nous appliquons l'opérateur d'indirection à {\tt (t+i)} nous obtenons
l'élément de rang {\tt i} du tableau, ce que nous notions jusqu'à présent
{\tt t[i]}.

\subsubsection{conséquence numéro 1}
%...................................

L'opérateur d'indexation noté {\tt []} est donc inutile, et n'a été
offert que pour des raisons de lisibilité des programmes, et pour ne pas
rompre avec les habitudes de programmation.

\subsubsection{conséquence numéro 2}
%...................................

Puisque l'opérateur d'indexation s'applique à des valeurs de type pointeur,
on va pouvoir l'appliquer à n'importe quelle valeur de type pointeur, et pas
seulement aux constantes repérant des tableaux.
En effet, après les déclarations~:
%
\begin{verbatim}
int t[10];
int * p;
\end{verbatim}
on peut écrire~:
\begin{verbatim}
p = &t[4];
\end{verbatim}
%
et utiliser l'opérateur d'indexation sur {\tt p}, {\tt p[0]} étant
{\tt t[4]}, {\tt p[1]} étant {\tt t[5]}, etc. {\tt p} peut donc être
utilisé comme un sous-tableau de {\tt t}.

%
%   ATTENTION
%   Dans la version precedente j'avais ici un topo sur la facon de faire
%   des index de 1 a N mais je l'ai supprime puisque c'est explicitement
%   interdit par la norme
%

\subsubsection{conséquence numéro 3}
%...................................

L'opérateur d'indexation est commutatif ! En effet, {\tt t[i]} étant
équivalent à {\tt *(t + i)} et l'addition étant commutative, {\tt t[i]}
est équivalent à {\tt *(i + t)} donc à {\tt i[t]}.
Lorsqu'on utilise l'opérateur d'indexation, on peut noter indifféremment
l'élément de rang i d'un tableau, {\tt t[i]} ou {\tt i[t]}.
Il est bien évident que pour des raisons de lisibilité, une telle notation
doit être prohibée, et doit être considérée comme une conséquence
pathologique de la définition de l'opérateur d'indexation.

\section{Passage de tableau en paramètre}
%-------------------------------------------
\index{tableau!passage en paramètre}

Du fait de la conversion d'un identificateur de type tableau en l'adresse
du premier élément, lorsqu'un tableau est passé en paramètre effectif,
c'est cette adresse qui est passée en paramètre.
Le paramètre formel correspondant devra
donc être déclaré comme étant de type pointeur.

Voyons sur un exemple.
Soit à écrire une procédure \verb+imp_tab+ qui est chargée
d'imprimer un tableau d'entiers qui lui est passé en paramètre.
On peut procéder de la manière suivante~:
\begin{verbatim}
void imp_tab(int *t, int nb_elem)   /*   définition de imp_tab       */
{
int i;

for (i = 0; i < nb_elem; i++) printf("%d ",*(t + i));
}
\end{verbatim}
Cependant, cette méthode a un gros inconvénient.
En effet, lorsqu'on lit l'en-tête de cette procédure, c'est à dire
la ligne~:
\begin{verbatim}
void imp_tab(int *t, int nb_elem)
\end{verbatim}
il n'est pas possible de savoir si le programmeur a voulu passer en paramètre un
pointeur vers un {\tt int} (c'est à dire un pointeur vers {\bf un seul}
{\tt int)}, ou au contraire si il a voulu passer un tableau, c'est à
dire un pointeur vers une zone de n {\tt int}.
De façon à ce que le programmeur puisse exprimer cette différence dans
l'en-tête de la procédure, le langage C admet que l'on puisse déclarer un
paramètre formel de la manière suivante~:
\begin{verbatim}
void proc(int t[])
{
...   /*   corps de la procédure proc   */
}
\end{verbatim}
car le langage assure que lorsqu'un paramètre formel de procédure ou
de fonction est déclaré comme étant de type tableau de {\sc x},
il est considéré comme étant de type pointeur vers {\sc x}.

Si d'autre part, on se souvient que la notation {\tt *(t + i)} est
équivalente à la
notation {\tt t[i]}, la définition de \verb+imp_tab+ peut s'écrire~:
\begin{verbatim}
void imp_tab(int t[], int nb_elem)   /*   définition de imp_tab       */
{
int i;

for (i = 0; i < nb_elem; i++) printf("%d ",t[i]);
}
\end{verbatim}
%
Cette façon d'exprimer les choses est beaucoup plus claire, et sera donc
préférée.
L'appel se fera de la manière suivante~:
\begin{latexonly}
%   newpage dans la verszion papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}
\begin{verbatim}
#define NB_ELEM 10
int tab[NB_ELEM];

int main(void)
{
imp_tab(tab,NB_ELEM);
}
\end{verbatim}

\subsubsection*{Remarque}
%........................
Quand une fonction admet un paramètre de type tableau, il y a deux cas
possibles~:
\begin{itemize}
\item[\ptiret] soit les différents tableaux qui lui sont passés en paramètre
effectif ont des tailles différentes, et dans ce cas la taille doit être
un paramètre supplémentaire de la fonction, comme dans l'exemple précédent~;

\item[\ptiret] soit les différents tableaux qui lui sont passés en paramètre
effectif ont tous la même taille, et dans ce cas la taille peut
apparaître dans le type du paramètre effectif~:
\begin{verbatim}
#define NB_ELEM 10
void imp_tab(int t[NB_ELEM])
{
...
}
\end{verbatim}
\end{itemize}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Modification des éléments d'un tableau passé en paramètre}
%==================================================================
\index{tableau!passage en paramètre}
Lorsqu'on passe un paramètre effectif à une procédure ou une fonction, on
a vu que l'on passait une valeur.
Il est donc impossible à une procédure de
modifier la valeur d'une variable passée en paramètre.

En ce qui concerne les tableaux par contre, on passe à la procédure l'adresse
du premier élément du tableau.
La procédure pourra donc modifier si elle le
désire les éléments du tableau.

Il semble donc que le passage de tableau en paramètre se fasse par adresse
et non par valeur et qu'il s'agisse d'une exception à la règle qui affirme
qu'en C, tout passage de paramètre se fait par valeur.
Mais il n'en est rien~: c'est la conversion automatique
des identificateurs de type tableau qui provoque ce phénomène.

Du point de vue pratique, on retiendra que l'on peut modifier les éléments
d'un tableau passé en paramètre.
On peut écrire par exemple~:

\begin{verbatim}
/*   incr_tab fait + 1 sur tous les éléments du tableau t  */
void incr_tab(int t[], int nb_elem)  
{
int i;

for (i = 0; i < nb_elem; i++) t[i]++;
}
\end{verbatim}

\section{Interdiction de modification des éléments d'un tableau passé en paramètre}
%===============================================================================
\index{const@{\tt const}}
Lors de la normalisation du langage C, le comité en charge du travail a pensé
qu'il était important d'introduire dans le langage un mécanisme permettant
au programmeur d'exprimer l'idée~: << cette procédure qui admet en paramètre
ce tableau, ne doit pas en modifier les éléments >>.
Pour réaliser cela, un nouveau mot-clé a été introduit,
le mot \verb+const+, qui permet de déclarer des variables de la manière 
suivante~:
\begin{verbatim}
const int i = 10;
\end{verbatim}
%
qui déclare une variable de nom \verb+i+ dont il est interdit de modifier
la valeur. 
L'intérêt de \verb+const+ se manifeste pour les paramètres de fonction.
Reprenons l'exemple de la procédure \verb+imp_tab+, pour exprimer le fait
que cette procédure ne doit pas modifier les éléments du tableau \verb+t+,
on peut (et il est recommandé de) l'écrire de la façon suivante~:
%
\begin{verbatim}
void imp_tab(const int t[], int nb_elem)   /*   définition de imp_tab       */
{
int i;

for (i = 0; i < nb_elem; i++) printf("%d ",t[i]);
}
\end{verbatim}

\section{Conversion des chaînes littérales}
%==========================================
\index{chaine litterale@{chaîne littérale}}
\index{conversion!de chaine litterale@{de chaîne littérale}}
On rappelle que les chaînes littérales  peuvent être
utilisées lors de la déclaration avec initialisation d'un tableau
de caractères, comme ceci~:
%
\begin{verbatim}
char message[] = "Bonjour !!";
\end{verbatim}
%
ou être utilisées dans une expression et être passées en
paramètre de fonction par exemple, comme cela~:
%
\begin{verbatim}
printf("Bonjour");
\end{verbatim}
%

\subsubsection*{Règle}
%.....................
Lorsque les chaînes littérales apparaissent dans un autre contexte qu'une
déclaration avec initialisation de tableau de caractères, elles subissent une
conversion en pointeur vers {\tt char}.
Si une fonction a comme paramètre formel un tableau de caractères, on pourra
lui passer en paramètre effectif aussi bien le nom d'un tableau de caractères
qu'une chaîne littérale.
Exemple~:
\begin{verbatim}
char mess[] = "Bonjour";
void f( char t[])
{
...   /*   corps de la fonction f  */
}

f(mess);       /*   un appel possible de f         */
f("Hello");    /*   un autre appel possible de f   */
\end{verbatim}

\section{Retour sur {\tt printf}}
%================================
\index{printf@{\tt printf}}
Nous avons vu au paragraphe \ref{printf} que la fonction {\tt printf} admet
comme premier paramètre une chaîne à imprimer comportant
éventuellement des séquence d'échappement~: \verb|%d|, \verb|%o| et
\verb|%x| pour imprimer un nombre respectivement en
décimal, octal et hexadécimal et \verb|%c| pour imprimer un caractère.
Il existe aussi une séquence d'échappement pour imprimer les chaînes
de caractères~:\verb|%s|.
Exemple~:
\begin{verbatim}
char mess1[] = "Hello";
char mess2[] = "Bonjour";
char *p;
if (...) p = mess1; else p = mess2;
printf("Message = %s\n",p);
\end{verbatim}

\section{Exercice}
%=================

\begin{enumerate}
\item Déclarer et initialiser deux tableaux de caractères (ch1 et ch2).

\item Écrire une fonction (\verb+lg_chaine1+) qui admette en paramètre un
tableau de caractères se terminant par un {\it null}, et qui rende le
nombre de caractères du tableau ({\it null} exclu).

\item Écrire une fonction (\verb+lg_chaine2+) qui implémente le même
interface que \verb+lg_chaine1+, mais en donnant à son paramètre le type
pointeur vers {\tt char}.

\item La procédure {\tt main} imprimera le nombre d'éléments de {\tt ch1}
et {\tt ch2} par un appel à \verb+lg_chaine1+ et \verb+lg_chaine2+.
\end{enumerate}

\newpage
\include{exer51}

\section{Tableaux multidimensionnels}
%=====================================
\index{tableau!multidimensionnel}

\subsection{Déclarations}
%------------------------
\index{declaration@{déclaration}!de tableau}

En C, un tableau multidimensionnel est considéré comme étant un tableau dont
les éléments sont eux mêmes des tableaux.
Un tableau à deux dimensions se déclare donc de la manière suivante~:
\begin{verbatim}
int t[10][20];
\end{verbatim}
Les mêmes considérations que celles que nous avons
développées sur les tableaux à une dimension s'appliquent, à savoir~:

\begin{enumerate}
\item à la déclaration, le compilateur allouera une zone
mémoire permettant de stocker de manière contiguë 10 tableaux de 20 entiers,
soit 200 entiers~;
\item toute référence ultérieure à {\tt t} sera convertie en l'adresse de 
sa première ligne, avec le type pointeur vers tableau de 20 {\tt int}.
\end{enumerate}

\subsection{Accès aux éléments}
%------------------------------
\index{tableau!element@{élément de}}
L'accès à un élément du tableau se fera de préférence par l'expression
{\tt t[i][j]}.

\subsection{Passage en paramètre}
%--------------------------------
\index{tableau!passage en paramètre}

Lorsqu'on désire qu'un paramètre formel soit un tableau à deux dimensions,
il faut le déclarer comme dans l'exemple suivant~:
\begin{verbatim}
#define N 10

p(int t[][N])
{
...   /*  corps de p   */
}
\end{verbatim}
On peut en effet omettre la taille de la première dimension, mais il est
nécessaire d'indiquer la taille de la seconde dimension, car le compilateur en
a besoin pour générer le code permettant d'accéder à un élément.
En effet, si T est la taille des éléments de {\tt t}, l'adresse de
{\tt t[i][j]} est~: {\it adresse de} $t + (i \times N + j) \times T$.
Le compilateur à besoin de connaître N, ce sera donc une constante.
Par contre, la taille de la première dimension pourra être passée en paramètre,
comme nous l'avons fait pour les tableaux à une seule dimension.
Exemple~:

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\begin{verbatim}
#define P 10

void raz_mat(int t[][P], int n)
{
int i,j;

for (i = 0; i < n; i++)
   for (j = 0; j < P; j++)
      t[i][j] = 0;
}
\end{verbatim}
%
\verb+raz_mat+ ne sera appliquable qu'à des tableaux dont la première
dimension à une
taille quelconque, mais dont la seconde dimension doit impérativement avoir
P éléments.

\section{Initialisation}
%-----------------------
\index{initialisation!de tableau}

Un tableau multidimensionnel peut être initialisé à l'aide d'une liste
de listes d'expressions constantes.
Exemple~:
\begin{verbatim}
int t[4][5] = {
   { 0,1,2,3,4},
   { 10,11,12,13,14},
   { 20,21,22,23,24},
   { 30,31,32,33,34},
};
\end{verbatim}
Un telle initialisation doit se faire avec des expressions constantes, c'est
à dire délivrant une valeur connue à la compilation.

\section{Exercice}
%=================

\begin{enumerate}
\item Déclarer et initialiser statiquement une matrice [5,5] d'entiers
({\tt tab}).

\item Écrire une fonction (\verb+print_mat+) qui admette en paramètre
une matrice [5,5]
et qui imprime ses éléments sous forme de tableau.

\item La procédure {\tt main} fera un appel à \verb+print_mat+ pour le
tableau  {\tt tab}.
\end{enumerate}

\newpage
\include{exer52}

\section{Tableau de pointeurs}
%=============================
\index{tableau!de pointeurs}

\subsection{Cas général}
%-----------------------
Pour des raisons de gain de place mémoire, on est parfois amené à créer
des tableaux à deux dimensions dont toutes les lignes n'ont pas la même taille.
Ceci peut se réaliser à l'aide d'un tableau de pointeurs vers des tableaux
de tailles différentes, associé à un autre tableau qui donnera la taille
de chaque ligne.
On obtient alors la structure de données suivante~:\\
\vspace*{0.5cm}
\hspace{3cm} \mbox{\includegraphics{fig3}}

Si nous supposons que le type des objets terminaux est {\tt int},
pour traduire cette structure de données en langage C, les programmeurs
ont l'habitude de <<~tricher~>>, et de ne pas utiliser le type
{\it tableau de pointeurs vers des tableaux de} {\tt int}, considéré comme
étant trop compliqué (un tel type s'écrit~: \verb+int (*tab[NB_ELEM])[]+).
La solution habituellement retenue, consiste à utiliser le type
{\it tableau de pointeurs vers des} {\tt int}, soit \verb+int *tab[NB_ELEM]+.
Voici un exemple d'une telle déclaration avec initialisation statique du
tableau~:
%
\begin{verbatim}
#define NB_ELEM 3
int taille[NB_ELEM] = {1, 2, 3};
int ligne1[] = {10};
int ligne2[] = {20,21};
int ligne3[] = {30,31,32};

int *tab[] = {ligne1, ligne2, ligne3};
\end{verbatim}
%
Pour une référence à l'élément $j$ de la ligne $i$, on n'a que l'embarras du
choix.
Nous donnons ci-après trois méthodes différentes d'imprimer les éléments du
tableau ligne par ligne.

\subsubsection*{Première méthode}
%................................
On utilise un pointeur vers un entier que l'on fait progresser d'élément en
élément dans une ligne~:

\begin{latexonly}
%   newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}

\begin{verbatim}
int i, *p;
for (i = 0; i < NB_ELEM; i++)
   {
   for (p = tab[i]; p < tab[i] + taille[i]; p++)
      printf("%d ",*p); /*   accès à l'élément courant par *p   */
   printf("\n");
   }

\end{verbatim}

\subsubsection*{Deuxième méthode}
%................................
On utilise un pointeur vers le premier élément d'une ligne~; ce pointeur reste
fixe.

\begin{verbatim}
int i, j, *p;
for (i = 0; i < NB_ELEM; i++)
   {
   for (p = tab[i], j = 0; j < taille[i]; j++)
      printf("%d ",p[j]); /*   accès à l'élément courant par p[j]   */
   printf("\n");
   }
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsubsection*{Troisième méthode}
%.................................
La dernière méthode est surprenante~: pour la comprendre, il suffit de
remarquer que la variable {\tt p} dans la seconde solution est inutile,
on peut accéder à l'élément courant par la notation \verb+tab[i][j]+.
%
\begin{verbatim}
int i, j, *p;
for (i = 0; i < NB_ELEM; i++)
   {
   for (j = 0; j < taille[i]; j++)
      /* accès à l'élément courant par tab[i][j] */
      printf("%d ", tab[i][j]);
   printf("\n");
   }
\end{verbatim}
%
On remarquera que c'est la même notation que celle qui est utilisée quand on
a un vrai tableau à deux dimensions, c'est à dire une structure de données
physiquement complètement différente.
Que l'accès à deux structures de données différentes puissent se réaliser de
la même manière, doit sans doute être considéré comme une faiblesse du langage.

\subsection{Tableaux de pointeurs vers des chaînes}
%--------------------------------------------------
\index{chaine de caracteres@{chaîne de caractères}}
\index{chaine litterale@{chaîne littérale}}
C'est pour les tableaux de caractères à deux dimensions, 
que se manifeste le plus souvent l'intérêt de disposer d'un tableau
de lignes de longueurs différentes~: les longueurs des chaînes sont
extrêmement variables.
La aussi, les habitudes sont les mêmes, les programmeurs utilisent le type
{\it tableau de pointeurs vers des} {\it char}
\footnote{Alors qu'en toute rigueur il faudrait utiliser le type
tableau de pointeurs vers des tableaux de char}, comme ceci~:
\begin{verbatim}
char * t[NB_ELEM];
\end{verbatim}
On peut initialiser un tableau de ce type avec des chaînes littérales~:
%
\begin{verbatim}
char * mois[] = {"janvier", "février", "mars", "avril",
           "mai", "juin", "juillet", "août", "septembre",
           "octobre", "novembre", "décembre"};
\end{verbatim}
%
On remarquera que ceci est impossible avec tout autre type que les {\tt char}~:
il est impossible d'écrire~:
%
\begin{verbatim}
int * tab[] = {{1}, {2,3}, {4,5,6}};
\end{verbatim}
%
Une boucle d'impression des valeurs du tableau {\tt mois} pourra être~:
\begin{verbatim}
#define NBMOIS 12
int i;

for (i = 0; i < NBMOIS ; i++)
   printf("%s\n",mois[i]);
\end{verbatim}

\subsection{Paramètres d'un programme}
%------------------------------------
\index{parametre@{paramètre}!de programme}
\index{argc@{\tt argc}}
\index{argv@{\tt argv}}
\index{chaine de caracteres@{chaîne de caractères}}
Les tableaux de pointeurs vers des chaînes de caractères sont une structure
de données importante, car c'est sur celle-ci que s'appuie la transmission 
de paramètres lors de l'exécution d'un programme.
Lorsqu'un utilisateur lance l'exécution du programme {\tt prog} avec les
paramètres {\tt param1}, {\tt param2}, ... {\tt paramn}, l'interpréteur de
commandes collecte tous ces mots sous forme de chaînes de caractères,
crée un tableau de pointeurs vers ces chaînes, et lance la procédure {\tt main}
en lui passant deux paramètres~:
\begin{itemize}
\item[\ptiret] un entier contenant la taille du tableau~;
\item[\ptiret] le tableau de pointeurs vers les chaînes.
\end{itemize}

Pour que le programme puisse exploiter les paramètres passés par l'utilisateur,
la fonction {\tt main} doit être déclarée de la manière suivante~:
\begin{verbatim}
int main(int argc, char *argv[])
{
...
}
\end{verbatim}
Les noms {\tt argc} (pour {\it argument count}),
ainsi que {\tt argv} (pour {\it argument values}), sont des noms
traditionnels, mais peuvent être remplacés par n'importe quels autres noms~;~
seuls les types doivent être respectés.

Comme exemple d'utilisation des paramètres, nous donnons le source d'un
programme qui imprime son nom et ses paramètres~:
\begin{verbatim}
int main(int argc, char *argv[])
{
int i;

printf("Nom du programme : %s\n", argv[0]);
for (i = 1; i < argc; i++)
   printf("Paramètre %d : %s\n",i,argv[i]);
}
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Tableau et pointeur, c'est la même chose~?}
%==================================================
Il y a un moment dans l'étude du langage C, où on a l'impression que les
tableaux et les pointeurs sont plus ou moins interchangeables, en un mot
que c'est pratiquement la même chose.
il faut donc être très clair~:
un tableau et un pointeur {\bf ce n'est pas} la même chose.
Quand on déclare, ailleurs qu'en paramètre formel de fonction,
\verb|int t[10];|
on déclare un tableau, et le compilateur réserve une zone de 10 entiers
consécutifs.
Quand on déclare \verb|int *p|, il s'agit toujours d'un pointeur, et le
compilateur réserve simplement un élément de mémoire pouvant contenir un
pointeur.

Les caractéristiques du langage C qui créent la confusion dans l'esprit des
utilisateurs, sont les trois règles suivantes~:
\begin{enumerate}
\item tout identificateur de type tableau de {\sc x} apparaissant dans
une expression est converti en une valeur constante de type pointeur vers 
{\sc x}, et ayant comme valeur l'adresse du premier élément du tableau~;

\item un paramètre formel de fonction, de type tableau de {\sc x} est considéré
comme étant de type pointeur vers {\sc x}~;

\item la sémantique de l'opérateur d'indexation  est la suivante~: \verb|T[i]|
est équivalent à \verb|*(T + i)|.
\end{enumerate}

\subsection{Commentaires}
%------------------------
Bien noter les points suivants~:
\begin{itemize}

\item[$\bullet$]
Le point important à comprendre dans la règle 2 est que tableau de
{\sc x} est la même chose que pointeur vers {\sc x} {\bf uniquement dans le
cas de paramètre formel de fonction}.
Donc \verb|void fonc (int t[]) { ... }| est équivalent à \\
\verb|void fonc (int * t) { ... }|.
Les types des objets déclarés de type tableau ou de type pointeur
sont différents dans tous les autres
contextes, que ce soit déclaration de variables globales ou locales à une
fonction.

\item[$\bullet$]
Différence entre règle 1 et règle 2~: une déclaration \verb|int t[10]|
qui n'est pas un paramètre formel, déclare un tableau de 10 entiers.
Ce sont les utilisations ultérieures de {\tt t} qui subissent une conversion
en type pointeur vers entier.
Par contre, à la déclaration d'un paramètre formel \verb|int t[]|,
c'est la déclaration elle-même qui est transformée en \verb|int *t|.

\end{itemize}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Cas particulier des chaînes littérales}
%..................................................
\index{chaine litterale@{chaîne littérale}}
Les chaînes littérales viennent ajouter à la confusion, car on peut
déclarer \\
\verb+char t[] = "Hello";+ et \verb+char *p = "Hello";+.
Dans le premier cas, le compilateur alloue un tableau de 6 caractères qu'il
initialise avec les caractères H, e, l, l, o et \verb+\0+.
Toute occurrence de {\tt t} dans une expression sera convertie en type
pointeur vers {\tt char}.
Dans le second cas, le compilateur alloue un tableau de 6 caractères qu'il
initialise de la même manière que dans le premier cas, mais de surcroît,
il alloue une variable de type pointeur vers {\tt char} qu'il initialise
avec l'adresse du premier caractère de la chaîne.

%   En sortant en EPS les deux figures fig1.fig et fig2.fig, il faut les
%   mettre a la magnification 80%
\begin{center}
\begin{figure}[h]
\begin{tabular}{c|c}
\setlength{\extrarowheight}{10pt}
\begin{minipage}{4cm}
\mbox{\tt char t[] = "Hello";}\vspace*{.7cm}
\end{minipage} & \mbox{\includegraphics{fig1}} \\ \hline
\begin{minipage}{4cm}
\mbox{\tt char *p = "Hello";} 
\end{minipage} 
& 
\begin{minipage}{13cm}
\vspace*{0.5cm}
\mbox{\includegraphics{fig2}} \\
\end{minipage}
\end{tabular}
\end{figure}
\end{center}

\begin{latexonly}
%   Si je ne fais pas un newpage, ca fait flotter la figure
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}

Ceci est un cas particulier des tableaux de caractères qui ne se reproduit pas
avec les autres types.
On peut déclarer un tableau d'entiers par \verb| int t[] = {1, 2, 3, 4, 5};|,
mais on ne peut pas déclarer un pointeur vers un tableau d'entiers par~:\\
\verb| int *p = {1, 2, 3, 4, 5};|.

\section{Récréation}
%===================
\index{chaine de caracteres@{chaîne de caractères}}
\index{shell@{\it shell}}
En illustration sur les bizarreries des tableaux dans le langage C,
voici la contribution de David Korn (le créateur du {\it korn shell}) à la
compétition du code C le plus obscur ({\sc ioccc}) de 1987~:
\begin{verbatim}
main() { printf(&unix["\021%six\012\0"],(unix)["have"]+"fun"-0x60);}
\end{verbatim}

Non, ce programme n'imprime pas {\it have fun with unix} ou quelque chose de
ce genre~!
Le lecteur est invité à essayer d'élucider ce programme (oui, il imprime 
quelque chose, mais quoi~?)
la solution est donnée à la page suivante.
\newpage

Voici les clés de la compréhension~:
\begin{enumerate}
\item Tout compilateur C possède un certain nombre de constantes prédéfinies
dépendant de l'architecture de la machine sur laquelle il s'exécute.
En particulier, tout compilateur pour machine {\sc unix} prédéfinit la
constante {\tt unix} avec comme valeur 1.
Donc le programme est équivalent à~:
\begin{verbatim}
main() { printf(&1["\021%six\012\0"],(1)["have"]+"fun"-0x60);}
\end{verbatim}

\item Si on se souvient qu'on a vu en \ref{operateur-indexation}
que pour un tableau {\tt t}, \verb+t[i]+
est équivalent à \verb+i[t]+, on voit que \verb|1["\021%six\012\0"]| est
équivalent à \verb|"\021%six\012\0"[1]| et \verb|(1)["have"]| à 
\verb|"have"[1]|.
Donc \verb|&1["\021%six\012\0"]| est l'adresse du caractère \verb+%+ dans la
chaîne \verb|"\021%six\012\0"| et \verb|"have"[1]| est le caractère \verb|'a'|.
On peut donc réécrire le programme~:
\begin{verbatim}
main() { printf("%six\012\0", 'a' + "fun" -0x60);}
\end{verbatim}

\item La fin d'une chaîne est signalée par un caractère {\it null} (\verb+\0+)
et le compilateur en met un à la fin de chaque chaîne littérale. Celui qui
est ici est donc inutile.
D'autre part, il existe une notation plus parlante pour le caractère de code
\verb+\012+ (c'est à dire {\it new line}), il s'agit de la notation \verb+\n+.
Le programme peut donc se réécrire~:
\begin{verbatim}
main() { printf("%six\n", 'a' + "fun" -0x60);}
\end{verbatim}

\item Le caractère \verb+'a'+ a pour code {\sc ascii} 0x61, donc 
\verb+'a' -0x60+ est égal à 1.
Réécrivons le programme~:
\begin{verbatim}
main() { printf("%six\n","fun" + 1); }
\end{verbatim}

\item \verb|"fun" + 1| est l'adresse du caractère u dans la chaîne \verb|"fun"|,
 le programme devient donc~:
\begin{verbatim}
main() { printf("%six\n","un"); }
\end{verbatim}
il imprime donc \verb|unix|.
\end{enumerate}

