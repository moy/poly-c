% \setcounter{chapter}{5}
% \setcounter{page}{91}

\cleardoublepage

\chapter{Structures, unions et énumérations}
%*******************************************
\section{Notion de structure}
%============================
Il est habituel en programmation d'avoir besoin d'un mécanisme permettant
de grouper un certain nombre de variables de types différents au sein d'une
même entité.
On travaille par exemple sur un fichier de personnes et on voudrait
regrouper une variable de type chaîne de caractères pour le nom, une
variable de type entier pour le numéro d'employé, etc.
La réponse à ce besoin est le concept d'{\it enregistrement}~: un enregistrement
est un ensemble d'éléments de types différents repérés par un nom.
Les éléments d'un enregistrement sont appelés des {\it champs}.
Le langage C possède le concept d'enregistrement avec cependant un problème de
vocabulaire~:
\begin{center}
\begin{tabular}{|c|c|}
\hline
ce que tout le monde appelle & le langage C l'appelle \\ \hline
enregistrement & structure \\
champ d'un enregistrement & membre d'une structure \\
\hline
\end{tabular}
\end{center}

\section{Déclaration de structure}
%=================================
\index{declaration@{déclaration}!de structure}
\index{identificateur}
\index{definition@{définition}!d'etiquette de structure@{d'étiquette de structure}}
Il y a plusieurs méthodes possibles pour déclarer des structures.

\subsubsection*{Première méthode}
%................................
La déclaration~:
%
\begin{verbatim}
struct personne
   {
   char nom[20];
   char prenom[20];
   int no_employe;
   };
\end{verbatim}
%
déclare 
l'identificateur {\tt personne} comme étant le nom d'un type de structure
composée de trois membres, dont le premier est un tableau de 20 caractères
nommé {\tt nom,} le second un tableau de 20 caractères nommé {\tt prenom},
et le dernier un entier nommé \verb+no_employe+.
\index{struct@{\tt struct}}
\index{membre!de structure}
\index{etiquette@{étiquette}!de structure}
Dans le jargon du langage C, l'identificateur {\tt personne} est une
{\it étiquette de structure}.
On peut ensuite utiliser ce type structure pour déclarer des variables,
de la manière suivante~:
\begin{verbatim}
struct personne p1,p2;
\end{verbatim}
qui déclare deux variables de type {\tt struct personne} de noms {\tt p1}
et {\tt p2};

\subsubsection*{Deuxième méthode}
%................................
On peut déclarer des variables de type structure sans utiliser d'étiquette de
structure, par exemple~:
\begin{verbatim}
struct
   {
   char nom[20];
   char prenom[20];
   int no_employe;
   } p1,p2;
\end{verbatim}
déclare deux variables de noms {\tt p1} et {\tt p2} comme étant deux
structures de trois membres, mais elle ne donne pas de nom au type de
la structure.
L'inconvénient de cette méthode est qu'il sera par la suite impossible de
déclarer une autre variable du même type.
En effet, si plus loin on écrit~:
\begin{verbatim}
struct
   {
   char nom[20];
   char prenom[20];
   int no_employe;
   } p3;
\end{verbatim}
les deux structures ont beau avoir le même nombre de champs, avec les mêmes
noms et les mêmes types, elles seront considérées de types différents.
Il sera impossible en particulier d'écrire \verb|p3 = p1;|.

\subsubsection*{Troisième méthode}
%.................................
On peut combiner déclaration d'étiquette de structure et déclaration de
variables, comme ceci~:
\begin{verbatim}
struct personne
   {
   char nom[20];
   char prenom[20];
   int no_employe;
   } p1,p2;
\end{verbatim}
déclare les deux variables {\tt p1} et {\tt p2} et donne le nom
{\tt personne} à la structure.
%
Là aussi, on pourra utiliser ultérieurement le nom {\tt struct personne} pour
déclarer d'autres variables~:
\begin{verbatim}
struct personne pers1, pers2, pers3;
\end{verbatim}
qui seront du même type que {\tt p1} et {\tt p2}.

De ces trois méthodes c'est la première qui est recommandée,
car elle permet de bien séparer la
définition du type structure de ses utilisations.

\subsection*{Initialisation d'une structure}
%-------------------------------------------
\index{initialisation!de structure}
Une structure peut être initialisée par une liste d'expressions constantes
à la manière des initialisations de tableau. Exemple~:
\begin{verbatim}
struct personne p = {"Jean", "Dupond", 7845};
\end{verbatim}

\section{Opérateurs sur les structures}
%======================================
\subsection{Accès aux membres des structures}
%-------------------------------------------
\index{operateur@{opérateur}!sur les structures}
\index{operateur@{opérateur}!.@{\tt .}(point)}

Pour désigner un membre d'une structure, il faut utiliser l'opérateur de
sélection de membre qui se note {\tt .} (point).
Par exemple, si {\tt p1} et {\tt p2} sont deux variables de type
{\tt struct personne}, on
désignera le membre {\tt nom} de {\tt p1} par {\tt p1.nom} et on désignera
le membre \verb+no_employe+ de {\tt p2} par \verb+p2.no_employe+.
Les membres ainsi désignés se comportent comme n'importe quelle variable et
par exemple, pour accéder au premier caractère du nom de {\tt p2}, on écrira~:
{\tt p2.nom[0]}.

\subsection{Affectation de structures}
%-------------------------------------
\index{affectation!de structure}
On peut affecter une structure à une variable structure de même type, grâce à
l'opérateur d'affectation~:
\begin{verbatim}
struct personne p1,p2
...
p1 = p2;
\end{verbatim}

\subsection{Comparaison de structures}
%-------------------------------------
Aucune comparaison n'est possible sur les structures, même pas les opérateurs
{\tt ==} et {\tt !=}.

\section{Tableaux de structures}
%===============================
\index{tableau!de structure}

Une déclaration de tableau de structures se fait selon le même modèle que la
déclaration d'un tableau dont les éléments sont de type simple.
Supposons que l'on ait déjà déclaré la {\tt struct personne}, si on veut
déclarer un tableau de 100 structures de ce type, on écrira~:
\begin{verbatim}
struct personne t[100];
\end{verbatim}
Pour référencer le nom de la personne qui a l'index {\tt i} dans {\tt t}
on écrira~: {\tt t[i].nom}.

\section{Exercice}
%=================

Soit un fichier de données identiques à celui de l'exercice précédent.

Écrire une procédure {\tt main} qui~:
\begin{enumerate}
\item lise le fichier en mémorisant son contenu dans un tableau de structures,
chaque structure permettant de mémoriser le contenu d'une ligne (nom,
article, nombre et prix).
\item parcoure ensuite ce tableau en imprimant le contenu de chaque structure.
\end{enumerate}
\newpage
\include{exer71}

\section{Pointeurs vers une structure}
%=====================================
\index{pointeur!vers une structure}
Supposons que l'on ait défini la {\tt struct personne} à l'aide de la
déclaration~:
\begin{verbatim}
struct personne
   {
   ...
   };
\end{verbatim}
on déclarera une variable de type pointeur vers une telle structure de la
manière suivante~:
\begin{verbatim}
struct personne *p;
\end{verbatim}
on pourra alors affecter à {\tt p} des adresses de {\tt struct personne}.
Exemple~:
\begin{verbatim}
struct personne
   {
   ...
   };

int main(void)
{
struct personne pers;   /*   pers est une variable de type struct personne   */
struct personne *p;     /*   p est un pointeur vers une struct personne      */

p =  &pers;
}
\end{verbatim}

\section{Structures dont un des membres pointe vers une structure du même type}
%=============================================================================
Une des utilisations fréquentes des structures, est de créer des listes de
structures chaînées.
Pour cela, il faut que chaque structure contienne un membre
qui soit de type pointeur vers une structure du même type.
Cela se fait de la façon suivante~:
\begin{verbatim}
struct personne
   {
   ...  /*   les différents membres   */
   struct personne *suivant;
   };
\end{verbatim}
le membre de nom {\tt suivant} est déclaré comme étant du type pointeur
vers une {\tt struct personne}.
\index{structure!recursive@{récursive}}
La dernière structure de la liste devra avoir un membre {\tt suivant} dont
la valeur sera le pointeur {\tt NULL} que nous avons vu en \ref{pointer-null}

\section{Accès aux éléments d'une structure pointée}
%===================================================
\index{membre!de structure!acces aux@{accès aux}}
\index{operateur@{opérateur}!sur les structures}
\index{operateur@{opérateur}!->@{\tt ->}}

Supposons que nous ayons déclaré {\tt p} comme étant de type pointeur
vers une
{\tt struct personne}, comment écrire une référence à un membre de
la structure pointée par {\tt p} ?
Étant donné que {\tt *p} désigne la structure, on serait tenté d'écrire
{\tt *p.nom} pour référencer le membre {\tt nom}.
Mais il faut savoir que les opérateurs d'indirection (*) et de sélection (.),
tout comme les opérateurs arithmétiques, ont une priorité.
Et il se trouve que l'indirection a une priorité inférieure à celle de la
sélection.
Ce qui fait que {\tt *p.nom} sera interprété comme signifiant {\tt *(p.nom)}.
(Cela aurait un sens si {\tt p} était une structure dont un des membres
s'appelait {\tt nom} et était un pointeur).
Dans notre cas, il faut écrire {\tt (*p).nom} pour forcer 
l'indirection à se faire avant la sélection.

Cette écriture étant assez lourde, le langage C a prévu un
nouvel opérateur noté \verb|->| qui réalise à la fois l'indirection et
la sélection~: \verb|p->nom| est identique à {\tt (*p).nom}.
Exemple~:
si {\tt p} est de type pointeur vers la {\tt struct personne} définie
précédemment, pour
affecter une valeur au membre \verb+no_employe+ de la structure pointée par
{\tt p,} on peut écrire~:
\begin{verbatim}
p -> no_employe = 13456;
\end{verbatim}

\section{Passage de structures en paramètre}
%===========================================
\index{structure!passage en paramètre}
Supposons que l'on ait fait la déclaration suivante~:
\begin{verbatim}
struct date
   {
   int jour,mois,annee;
   };
\end{verbatim}
une fonction de comparaison de deux dates pourra s'écrire~:
\begin{verbatim}
enum {AVANT, EGAL, APRES};

int cmp_date( struct date d1, struct date d2)
{
if (d1.annee > d2.annee)
   return(APRES);
if (d1.annee < d2.annee)
   return(AVANT);
...   /*   comparaison portant sur mois et jour   */
}
\end{verbatim}
%
et une utilisation de cette fonction pourra être~:
\begin{verbatim}
struct date d1,d2;

if (cmp_date(d1,d2) == AVANT)
   ...
\end{verbatim}

\subsection*{Attention}
%----------------------
En langage C {\sc k\&r}, il n'est pas possible de passer en paramètre une
structure, mais on peut passer un pointeur vers une structure.

\section{Détermination de la taille allouée à un type}
%=====================================================
\index{operateur@{opérateur}!sizeof@{\tt sizeof}}

Pour connaître la taille en octets de l'espace mémoire nécessaire pour
une variable, on dispose de l'opérateur {\tt sizeof}.
Cet opérateur est un opérateur unaire préfixé que l'on peut employer de deux
manières différentes~: soit {\tt sizeof}~{\it expression} soit
{\tt sizeof}~{\tt (}~{\it nom-de-type}~{\tt )}.
Exemple~:
\begin{latexonly}
%   newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}
\begin{verbatim}
int i,taille;

taille = sizeof i;
taille = sizeof (short int);
taille = sizeof (struct personne);
\end{verbatim}

\subsection{Retour sur la conversion des tableaux}
%-------------------------------------------------
\index{conversion!de tableau}
L'opérande de l'opérateur {\tt sizeof} est la seule exception à la
conversion d'un identificateur de type tableau de {\sc x} en pointeur
vers {\sc x}.
Ne pas réaliser cette conversion est en effet nécessaire pour que l'opérateur
{\tt sizeof} ait l'effet attendu par le programmeur lorsqu'il l'applique
à un tableau.
Exemple~:
\begin{verbatim}
int t[10];

if (sizeof(t) / sizeof(int) != 10)
   printf("sizeof mal implémenté\n");
else printf("sizeof ok\n");
\end{verbatim}

\section{Allocation et libération d'espace pour les structures}
%==============================================================
\index{bibliothèque standard}
\label{gestion-memoire}
Nous allons voir dans ce paragraphe trois fonctions de la bibliothèque standard
permettant d'allouer et de libérer de l'espace.

\subsection{Allocation d'espace~: fonctions {\tt malloc} et {\tt calloc}}
%------------------------------------------------------------------------
\index{malloc@{\tt malloc}}
\index{calloc@{\tt calloc}}
\index{allocation de mémoire}
\index{liberation de memoire@{libération de mémoire}}

Quand on crée une liste chaînée, c'est parce qu'on ne sait pas à la
compilation combien elle comportera d'éléments à l'exécution (sinon on
utiliserait un tableau).
Pour pouvoir créer des listes, il est donc nécessaire de pouvoir allouer de
l'espace dynamiquement.
On dispose pour cela de deux fonctions {\tt malloc} et {\tt calloc}.

\subsubsection{Allocation d'un élément~: fonction {\tt malloc}}
%..............................................................
La fonction {\tt malloc} admet un paramètre qui est la taille en octets
de l'élément désiré et elle rend un pointeur vers l'espace alloué.
Utilisation typique~:
\begin{verbatim}
#include <stdlib.h>
struct personne *p;

p = malloc(sizeof(struct personne));
\end{verbatim}

\subsubsection{Allocation d'un tableau d'éléments~: fonction {\tt calloc}}
%.........................................................................
Elle admet deux paramètres~:
\begin{itemize}
\item[\ptiret] le premier est le nombre d'éléments désirés~;
\item[\ptiret] le second est la taille en octets d'un élément.
\end{itemize}
%
son but est d'allouer un espace suffisant pour contenir les éléments demandés
et de rendre un pointeur vers cet espace.
\begin{latexonly}
%   Newpage dans la version papier
\ifthenelse{\boolean{Viewable}}
   {}
   {\newpage}
\end{latexonly}
Utilisation typique~:
\begin{verbatim}
#include <stdlib.h>
struct personne *p;
int nb_elem;

...     /*   init  de nb_elem   */
p = calloc(nb_elem,sizeof(struct personne));
\end{verbatim}
%
On peut alors utiliser les éléments {\tt p[0]}, {\tt p[1]}, ... \verb+p[nb_elem-1]+.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\subsection{Libération d'espace~: procédure free}
%------------------------------------------------
\index{free@{\tt free}}

On libère l'espace alloué par {\tt malloc} ou {\tt calloc} au moyen de
la procédure {\tt free} qui admet
un seul paramètre~: un pointeur précédemment rendu par un appel à
{\tt malloc} ou {\tt calloc}.
Utilisation typique~:
\begin{verbatim}
#include <stdlib.h>
struct personne *p;

p = malloc(sizeof(struct personne));
...   /*   utilisation de la structure allouée   */
free(p);
\end{verbatim}

%-------------------------------------------------------------------------------
%   ABANDONNE CAR REJETE DANS LE CHAPITRE 1
%-------------------------------------------------------------------------------
%\section{Déclaration de procédures / fonctions externes}
%%=======================================================
%Lorsque on  écrit des choses du genre~:
%\begin{verbatim}
%struct date *p;
%
%p = malloc(sizeof(struct date));
%\end{verbatim}
%on s'apercoit, lors de la compilation, qu'il y a émission d'un
%avertissement concernant l'instruction \verb+p = malloc(...)+.
%Cet avertissement précise que le type de la valeur affectée n'ést pas
%compatible avec celui de la variable à laquelle on l'affecte.
%
%Pour comprendre la raison de ce message il faut savoir 2 choses~:
%\begin{enumerate}
%\item Les procédures et fonctions {\tt printf}, {\tt scanf}, {\tt malloc},
%{\tt free} etc. ne sont
%pas en C, contrairement à ce qu'elles sont dans la majorité des autres langages,
%des procédures connues du langage. Ce sont des procédures et fonctions externes,
%indiscernables du point de vue du langage, des autres procédures et fonctions
%écrites par l'utilisateur.
%
%\item Lorsqu'on rencontre dans un source C une occurence d'une utilisation d'une
%fonction avant sa définition, le langage considère par défaut que cette 
%fonction retourne un {\tt int}.
%Si tel est bien le cas, il n'y a pas de problème,
%sinon, il sera nécessaire de déclarer cette fonction de manière à informer
%le compilateur du type des valeurs retournées par la fonction.
%\end{enumerate}
%
%Pour résoudre ce problème il faut, avant d'utiliser  {\tt malloc}, la
%déclarer en tant que fonction externe.
%Cette déclaration pourra se faire de deux façons différentes,
%selon le programme~:
%\begin{itemize}
%\item si la fonction {\tt malloc} n'est utilisée que pour allouer un seul
%type d'objet, (par exemple des {\tt struct date}), on la déclarera comme suit~:
%\begin{verbatim}
%extern struct date *malloc();
%\end{verbatim}
%
%\item si la fonction {\tt malloc} sert à allouer plusieurs type d'objets,
%il faudra utiliser le type pointeur générique (donc compatible avec tout
%%pointeur) et la déclarer de cette façon~:
%
%\begin{verbatim}
%extern void *malloc();
%\end{verbatim}
%%\end{itemize}

\section{Exercice}
%=================

Modifier le programme précédent~:
\begin{enumerate}
\item en écrivant une procédure d'impression d'une {\tt struct commande}
passée en paramètre.

\item en écrivant une fonction de recherche de commande maximum (celle pour
laquelle le produit nombre $ \times $ prix est maximum).
Cette fonction admettra en paramètre un pointeur vers la {\tt struct commande}
qui est tête de la liste complète,
et rendra un pointeur vers la structure recherchée.

\item le {\tt main} sera modifié de manière à faire appel à la fonction de
 recherche de la commande maximum et à imprimer cette commande.
\end{enumerate}
\newpage
\include{exer73}

\section{Les champs de bits}
%===========================
\subsection{Généralités}
%-----------------------
\index{champs de bits}
\index{unsigned@{\tt unsigned}}
Il est parfois nécessaire pour un programmeur de décrire en termes de bits 
la structure d'une ressource matérielle de la machine.
Un exemple typique est la programmation système qui nécessite de manipuler
des registres particuliers de la machine.
Par exemple, dans le manuel du {\sc mc 68030} de Motorola, le registre
d'état est ainsi décrit~:
\begin{itemize}
\item[\ptiret] bit 0 : carry~;
\item[\ptiret] bit 1 : overflow~;
\item[\ptiret] bit 2 : zéro~;
\item[\ptiret] bit 3 : négatif~;
\item[\ptiret] bit 4 : extension~;
\item[\ptiret] bits 5-7 : inutilisés~;
\item[\ptiret] bits 8-10 : masque des interruptions~;
\item[\ptiret] bit 11 : inutilisé~;
\item[\ptiret] bits 12-13 : niveau de privilège~;
\item[\ptiret] bits 14-15 : état des traces.
\end{itemize}

Il existe dans le langage C un moyen de réaliser de telles descriptions,
à l'aide du concept de structure.
En effet, dans une déclaration de structure, il est possible de
faire suivre la définition d'un membre par une indication du nombre de bits que
doit avoir ce membre. Dans ce cas, le langage C appelle ça un {\it champ de
bits}.

Le registre d'état du {\sc mc 68030} peut se décrire ainsi~:
\begin{verbatim}
struct sr
   {
   unsigned int trace : 2;
   unsigned int priv : 2;
   unsigned int : 1;            /*   inutilisé   */
   unsigned int masque : 3;
   unsigned int : 3;            /*   inutilisé   */
   unsigned int extend : 1;
   unsigned int negative : 1;
   unsigned int zero : 1;
   unsigned int overflow : 1;
   unsigned int carry : 1;
   };
\end{verbatim}
On voit que le langage C accepte que l'on ne donne pas de nom aux champs de
bits qui ne sont pas utilisés.

\subsection{Contraintes}
%-----------------------
\index{big endian@{\it big endian}}
\index{little endian@{\it little endian}}
\index{signed@{\tt signed}}
\begin{enumerate}
\item Les seuls types acceptés pour les champs de bits sont {\tt int},
{\tt unsigned int} et {\tt signed int}.
\item L'ordre dans lequel sont mis les champs de bits à l'intérieur
d'un mot dépend de l'implémentation, mais généralement, dans une machine
{\it little endian} les premiers champs décrivent les bits de poids faibles et
les derniers champs les bits de poids forts, alors que c'est généralement
l'inverse dans une machine {\it big endian}.
\item Un champ de bit déclaré comme étant de type {\tt int,} peut en fait
se comporter comme un {\tt signed int} ou comme un {\tt unsigned int}
(cela dépend de l'implémentation).
Il est donc recommandé d'une manière générale de déclarer les champs
de bits comme étant de type {\tt unsigned int}.
\item Un champ de bits n'a pas d'adresse, on ne peut donc pas lui appliquer
l'opérateur adresse de (\verb+&+).
\end{enumerate}
\index{dependance de l'implementation@{dépendance de l'implémentation}}

\section{Les énumérations}
%=========================
\index{enumeration@{énumération}}
\index{enum@{\tt enum}}
\index{definition@{définition}!d'etiquette d'enumeration@{d'étiquette d'énumération}}
Nous avons vu au paragraphe \ref{enumeration} que l'on pouvait déclarer
des constantes nommées de la manière suivante~:
\begin{verbatim}
enum {LUNDI, MARDI, MERCREDI, JEUDI, VENDREDI, SAMEDI, DIMANCHE};
\end{verbatim}
qui déclare les identificateurs {\tt LUNDI}, {\tt MARDI}, etc. comme
étant des constantes entières de valeur 0, 1, etc.
Ce qui n'avait pas été dit à ce moment là, c'est que les énumérations 
fonctionnent syntaxiquement comme les structures~: après le mot-clé 
{\tt enum} il peut y avoir un identificateur appelé {\it étiquette
d'énumération} qui permettra plus loin dans le programme de déclarer des
variables de type énumération. Exemple~:
\begin{verbatim}
enum jour {LUNDI, MARDI, MERCREDI, JEUDI,
           VENDREDI, SAMEDI, DIMANCHE};

enum jour j1, j2;
j1 = LUNDI;
j2 = MARDI;
\end{verbatim}

L'exemple ci-dessus est conforme à ce qui nous semble être de bonnes règles
de programmation~: déclarer d'abord le type énumération en lui donnant un
nom grâce à une étiquette d'énumération et ensuite utiliser ce nom pour
déclarer des variables. Cependant, comme pour les structures, le langage C
permet de déclarer des variables dans la déclaration du type énumération,
éventuellement en omettant l'étiquette d'énumération. Exemples~:
\begin{verbatim}
enum jour {LUNDI, MARDI, MERCREDI, JEUDI,
           VENDREDI, SAMEDI, DIMANCHE} d1, d2;
\end{verbatim}
déclare {\tt d1} et {\tt d2} comme étant des variable de type {\tt enum jour},
\begin{verbatim}
enum {FAUX, VRAI} b1, b2;
\end{verbatim}
déclare {\tt b1} et {\tt b2} comme étant des variables de type énumération sans 
nom, dont les valeurs peuvent être {\tt FAUX} ou {\tt VRAI}.
Nous avons expliqué plus haut pourquoi un tel style de programmation nous
semblait mauvais.

\section{Les unions}
%===================
\index{union}
\index{declaration@{déclaration}!d'union}
\index{definition@{définition}!d'etiquette d'union@{d'étiquette d'union}}

Il est parfois nécessaire de manipuler des variables auxquelles on désire
affecter des valeurs de type différents.
Supposons que l'on désire écrire un
package mathématique qui manipulera des nombres qui seront implémentés
par des {\tt int}, tant que la précision des entiers de la machine sera
suffisante et qui passera automatiquement à une représentation sous
forme de flottants dès que ce ne sera plus le cas.
Il sera nécessaire de disposer de variables pouvant prendre
soit des valeurs entières, soit des valeurs flottantes. 

Ceci peut se réaliser en C, grâce au mécanisme des unions.
Une définition d'union a la même syntaxe qu'une définition de structure,
le mot clé {\tt struct} étant simplement remplacé par le mot clé
{\tt union}. Exemple~:

\begin{verbatim}
union nombre
   {
   int i;
   float f;
   }
\end{verbatim}
L'identificateur {\tt nombre} est appelé {\it étiquette d'union}.
\index{etiquette@{étiquette}!d'union}
La différence sémantique entre les {\tt struct} et les {\tt unions}
est la suivante~:
alors que pour une variable de type structure tous les membres peuvent avoir en
même temps une valeur, une variable de type union ne peut avoir à un instant
donné qu'un seul membre ayant une valeur.
En reprenant l'exemple précédent, on déclarera une variable {\tt n}
de type {\tt union nombre} par~:
\begin{verbatim}
union nombre n;
\end{verbatim}
cette variable pourra posséder soit une valeur entière, soit une valeur
flottante, mais pas les deux à la fois.

\section{Accès aux membres de l'union}
%=====================================

Cet accès se fait avec le même opérateur sélection (noté {\tt .}) que
celui qui sert à accéder aux membres des structures.
Dans l'exemple précédent, si on désire faire posséder à la variable
{\tt n} une valeur entière, on écrira~:
\begin{verbatim}
n.i = 10;
\end{verbatim}
si on désire lui faire posséder une valeur flottante, on écrira~:
\begin{verbatim}
n.f = 3.14159;
\end{verbatim}

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Utilisation pratique des unions}
%========================================

Lorsqu'il manipule des variables de type union, le programmeur n'a
malheureusement aucun moyen de savoir à un instant donné, quel est le
membre de l'union qui possède une valeur.
Pour être utilisable, une union doit donc toujours être associée à une
variable dont le but sera d'indiquer le membre de l'union qui est valide.
En pratique, une union et son indicateur sont généralement englobés à
l'intérieur d'une structure.
Dans l'exemple précédent, on procédera de la manière suivante~:
\begin{verbatim}
enum type {ENTIER, FLOTTANT};

struct arith
   {
   enum type typ_val;   /*   indique ce qui est dans u   */
   union
      {
      int i;
      float f;
      } u;
   };
\end{verbatim}
la {\tt struct arith} a deux membres \verb+typ_val+ de type {\tt int},
et {\tt u} de type union d'{\tt int} et de {\tt float}.
On déclarera des variables par~:
%
\begin{verbatim}
struct arith a1,a2;
\end{verbatim}
%
puis on pourra les utiliser de la manière suivante~:
%
\begin{verbatim}
a1.typ_val = ENTIER;
a1.u.i = 10;

a2.typ_val = FLOTTANT;
a2.u.f = 3.14159;
\end{verbatim}
Si on passe en paramètre à une procédure un pointeur vers une
{\tt struct arith},
la procédure testera la valeur du membre \verb+typ_val+ pour savoir
si l'union reçue possède un entier ou un flottant.

\begin{latexonly}
\ifthenelse{\boolean{Viewable}}
   {\newpage}
   {}
\end{latexonly}

\section{Une méthode pour alléger l'accès aux membres}
%====================================================
\index{membre!de structure!acces aux@{accès aux}}
\index{preprocesseur@{préprocesseur}}
\index{\#define@{\tt \#define}}

Quand une union est dans une structure, il faut donner un nom au membre de la
structure qui est de type union, ce qui à pour conséquence de rendre assez 
lourd l'accès aux membres de l'union.
Dans l'exemple précédent, il faut écrire {\tt a1.u.f} pour accéder
au membre {\tt f}.
On peut alléger l'écriture en utilisant les facilités du préprocesseur.
On peut écrire par exemple~:
\begin{verbatim}
#define I u.i
#define F u.f
\end{verbatim}
Pour initialiser {\tt a1} avec l'entier 10, on écrira alors~:
\begin{verbatim}
a1.typ_val = ENTIER;
a1.I = 10;
\end{verbatim}
