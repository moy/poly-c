\chapter{Les extensions C-99 et C-11}
\label{chap=c99-c11}
%***********************************************
Le but de ce chapitre est de présenter quelques extensions du langage
C qui sont présentes dans les révisions du standard de 1999 et 2011.
Les extensions sont pour la plupart originaire et commune avec C++.
Nous ne présentons que quelques unes des nouveautés. Les lecteurs
curieux pourront se référer aux pages
\url{http://en.wikipedia.org/wiki/C99} et
\url{http://en.wikipedia.org/wiki/C11_%28C_standard_revision%29} pour
plus de détails.

L'implantation de ces extensions varie d'un compilateur à
l'autre. Par exemple, GCC a un support quasi-complet de C99, mais il
est désactivé par défaut, il faut compiler avec \verb|--std=c99| pour
en bénéficier (on peut aussi utiliser \verb|--std=gnu99| pour avoir
les ajouts de C99 en conservant les extensions GNU). D'autres
compilateurs comme Visual C++ ont choisi de se concentrer sur C90 et
C++, et n'ont pas de support pour C99 ni C11. En pratique, la norme
C90 reste donc la plus utilisée car la plus portable.


\section{Extension de la syntaxe}

Deux nouveautés de C99 permettent aux développeurs C++ et Java de
retrouver leurs marques en C :

\subsection{Les commentaires mono-lignes}

  Depuis C99, on peut utiliser des commentaires mono-lignes, avec la syntaxe
  \verb|// commentaire|. En C90, les seuls commentaires autorisés sont
  ceux utilisant la syntaxe \verb|/* commentaire */|, même si
  beaucoup de compilateurs acceptent les deux en pratique (GCC accepte
  les commentaires mono-lignes sauf si on compile avec \verb|-ansi|
  pour lui demander de se conformer plus strictement à la norme).

\subsection{Les déclarations de variables en cours de bloc}

C99 autorise des programmes comme :
\begin{verbatim}
{
    int x;
    x = 42;
    int y; // déclaration en milieu de bloc
    y = 42;
}
\end{verbatim}
  Une utilisation très pratique de cette fonctionnalité est de
  déclarer une variable locale à une boucle \texttt{for} :
\begin{verbatim}
for (int i = 0; i < N; ++i) f(i);
// i n'existe plus en dehors de la boucle
\end{verbatim}

\section{Les types de base}
%===============================
\AuthorNote{
  On ne parle pas des wide char, ni de Unicode, difficile à présenter
  dans une initiation.
}

Pour une présentation détaillées des différents types de C dans les
différentes normes, voir par exemple
\url{http://en.wikipedia.org/wiki/C_data_types}.

\subsection{Les booléens}
\label{sec=c99-bool}
%--------------------------

\index{type!booleen@{booléen}}
\index{bool@{\tt bool}}

En C90, la notion de booléen est totalement confondue avec celle de
nombre entier. En C99, on peut toujours utiliser le type \texttt{int}
pour désigner une valeur booléenne, mais on peut aussi utiliser le
type \texttt{bool} et les constantes \texttt{true} et \texttt{false},
contenues dans \texttt{stdbool.h}. Par exemple :

\begin{verbatim}
#include <stdbool.h>
#include <stdio.h>

int main() {
    bool x = true;
    if (x) {
        printf("Oui\n");
    }
}
\end{verbatim}

\subsection{Les grands nombres}

On avait déjà le type \texttt{long} pour désigner un grand entier
(selon les architectures, il est plus grand ou identique à
\texttt{int}), et \texttt{double} pour désigner les nombres flottants
avec une précision plus fine que \texttt{float}.

C99 apporte les types \texttt{long long} pour des entiers 64 bits (ou
plus) et \texttt{long double}, qui peut selon les architectures être
identique à \texttt{double}, ou être codé sur 80 ou 128 bits.

\subsection{Les entiers de taille fixée}
%----------------------------------------
\index{type!entiersdetaillefixee@{entier de taille fixée}}
\index{int8t@{\tt int8\_t}}
\index{int16t@{\tt int16\_t}}
\index{int32t@{\tt int32\_t}}
\index{int64t@{\tt int64\_t}}
\index{INT8MAX@{\tt INT8\_MAX}}
\index{INT16MAX@{\tt INT16\_MAX}}
\index{INT32MAX@{\tt INT32\_MAX}}
\index{INT64MAX@{\tt INT64\_MAX}}
\index{INT8MIN@{\tt INT8\_MIN}}
\index{INT16MIN@{\tt INT16\_MIN}}
\index{INT32MIN@{\tt INT32\_MIN}}
\index{INT64MIN@{\tt INT64\_MIN}}

Les types de base comme \texttt{int} ou \texttt{long} peuvent être
pratiques car leur taille s'adapte à l'architecture sur laquelle on
travaille. Par exemple, \texttt{int} sera un entier 16 bits sur une
architecture 16 bits, mais 32 bits sur une architecture 32 ou 64 bits.
Cette «~adaptation~» peut aussi être un inconvénient quand on souhaite
maîtriser exactement la taille (ou les valeurs minimales/maximales)
des données. C99 introduit des types entiers de taille fixe, déclarés
dans le fichier d'en-tête \texttt{stdint.h} (\texttt{man stdint.h} pour
avoir les détails). Par exemple, le type \texttt{int16\_t} désigne un
entier signé sur 16 bits, le type \texttt{uint32\_t} un entier non
signé («~u~» comme «~unsigned~») sur 32 bits. Plus généralement, les
types \texttt{int$N$\_t} et \texttt{uint$N$\_t}
désignent respectivement des entiers signés et non-signés sur $N$
bits. La norme impose de définir ces types pour $N$ valant 8, 16 et
32 ; et si l'implémentation fournit de tels entiers, 64. Pour chaque
valeur de $N$, les constantes \texttt{INT$N$\_MIN} et
\texttt{INT$N$\_MAX} désignent les valeur minimales et maximales
possibles pour le type \texttt{int$N$\_t}.

\section{Tableaux de taille variable}

En C90, la taille d'un tableau doit être constante. Même une
déclaration aussi simple que la suivante est refusée par la norme
(même si GCC l'accepte par défaut) :

\begin{verbatim}
const int S = 42;

int main() {
    int T[S];
    // ...
}
\end{verbatim}

(Le programme redevient correct si on utilise \verb|#define S 42| à la
place du \texttt{const int}).

En C99, la taille des tableaux n'est plus nécessairement connue à la
compilation, même si elle doit bien sûr être connue à l'instanciation
du tableau. Le programme ci-dessus est autorisé, de même que :

\begin{verbatim}
void f(int m) {
    int T[m]; // instanciation d'un tableau de taille m
    // ...
    int T2[T[42]];
}
\end{verbatim}

\section{Multithreading}

La programmation multi-thread (ou programmation parallèle), qui était
possible via des bibliothèques comme les \texttt{pthread} sur les
systèmes Unix, intègre la norme du langage en C11. La sémantique des
accès mémoires sur des programmes parallèles est définie, en ajoutant
la notion de type atomique (\texttt{\_Atomic} et le fichier
\texttt{stdatomic.h}). On peut déclarer des variables locales à un
thread (\texttt{\_Thread\_local}), et les primitives classiques comme
les création/destruction de thread, mutex et conditions sont définies
dans le nouveau fichier d'en-tête \texttt{threads.h}.

% complex
% init des tableaux
% init des structures
% notation binaire
% variable anonyme
% le %a %A et %p 
% inline
% restrict const
% snprintf
% a verifier si presnt dans C11: fonctions imbriquees; case ranges;


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "doc-book"
%%% End: 
