all: doc-book.pdf doc-book-mobile.pdf

html: document-html/index.html

FIGS=fig1.pdf fig2.pdf fig3.pdf iso-latin-1.pdf
WEB=$(HOME)/WWW/cours/poly-c

# convert fait n'importe quoi depuis du fig vers du bitmap.
# En passant par le PDF, ça marche par contre.
%.png: %.pdf
	convert $< $@

document-html/index.html: document-html.tex ${wildcard *.tex} $(FIGS:.pdf=.png)
	latex2html -local_icons $<
	echo 'AddCharset UTF-8 .html' > document-html/.htaccess

doc-book.pdf: $(FIGS)

install: doc-book.pdf doc-book-mobile.pdf document-html/index.html index.html
	mkdir -p $(WEB)
	rsync -av --delete document-html/ $(WEB)/poly-c-html/
	cd $(WEB)/ && $(RM) poly-c-html.zip && zip -r poly-c-html.zip poly-c-html/
	cp -f doc-book.pdf $(WEB)/poly-c.pdf
	cp -f doc-book-mobile.pdf $(WEB)/poly-c-mobile.pdf
	cp -f index.html $(WEB)/index.html
	cp -f ensimag.png $(WEB)/ensimag.png

PDFLATEX_OPTIONS=-file-line-error
LU_MASTERS=doc-book doc-book-mobile
include LaTeX.mk


# List of source files
SOURCES=index.txt

HTMLFILES=$(SOURCES:.txt=.html)

%.html: %.txt Makefile template.txt
	rst2html --stylesheet-path=style.css --embed-stylesheet \
		--initial-header-level=2 \
		--template=template.txt \
		$< > $@
